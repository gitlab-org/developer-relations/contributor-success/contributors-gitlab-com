# Contributing

We welcome contributions!

Everything you need to get a local development environment up and running can be
found in [README.md](README.md).

Issues to work on can be found in the [contributors-gitlab-com issue tracker](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/issues).
Items with the ~"quick win" label and/or the ~"workflow::ready for development"
label are probably the best candidates, but feel free to ping `@gitlab-org/developer-relations/contributor-success`
on any issue if you're interested.

## Developer Certificate of Origin and License

By contributing to GitLab B.V., you accept and agree to the following terms and
conditions for your present and future contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., you reserve all right, title, and interest in and to
your Contributions.

All contributions are subject to the
[Developer Certificate of Origin and License](https://docs.gitlab.com/ee/legal/developer_certificate_of_origin).

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Code of conduct

Contributions to this project should abide by the [GitLab code of conduct](https://about.gitlab.com/community/contribute/code-of-conduct/).
