# frozen_string_literal: true

module Api
  module V1
    class ApiApplicationController < ApplicationController
      skip_before_action :verify_authenticity_token

      private

      def ensure_logged_in
        render json: { error: 'Unauthorized' }, status: :unauthorized unless logged_in?
      end

      def ensure_admin
        render json: { error: 'Forbidden' }, status: :forbidden unless admin?
      end
    end
  end
end
