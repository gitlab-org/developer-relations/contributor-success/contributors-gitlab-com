# frozen_string_literal: true

module Api
  module V1
    class EventsController < ApiApplicationController
      include SessionUserConcern

      before_action :ensure_logged_in

      def create
        issue = ::Events::CreateService.new(session_user, event_params).execute

        render json: issue
      end

      private

      def event_params
        params.permit(:name, :date, :link, :role, :virtual, :size, :note)
      end
    end
  end
end
