# frozen_string_literal: true

module Api
  module V1
    class LeaderboardController < ApiApplicationController
      include DateParams
      include PaginationConcern

      def index
        records = LeaderboardQuery.new(
          search:, from_date:, to_date:, community_only:, page_size:, page:
        ).execute
        render_paginated_json(
          ::ContributorsSerializer.new(records, { current_user_id: session[:user_id] })
        )
      end

      private

      def community_only
        params[:community_only] != 'false'
      end

      def search
        params[:search].presence
      end
    end
  end
end
