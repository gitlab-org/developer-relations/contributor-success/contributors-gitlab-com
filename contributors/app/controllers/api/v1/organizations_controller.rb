# frozen_string_literal: true

module Api
  module V1
    class OrganizationsController < ApiApplicationController
      include DateParams
      include PaginationConcern

      def index
        results = ::OrganizationsQuery.new(filter_params).execute

        render_paginated_json(results)
      end

      def show
        organization = OrganizationQuery.new(params[:id], filter_params).execute

        render json: organization
      end

      private

      def filter_params
        {
          search: params[:search],
          from_date:,
          to_date:,
          page_size:,
          page:
        }
      end
    end
  end
end
