# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :track_user_activity!

  private

  def ensure_logged_in
    redirect_to login_path(return: request.fullpath) unless logged_in?
  end

  def gitlab_access_token(force_refresh: false)
    return session[:gitlab_access_token] unless force_refresh || Time.zone.now.to_i > session[:gitlab_expires_at]

    refresh = GitlabOauthService.refresh(session[:gitlab_refresh_token])
    session.merge!(refresh)
    session[:gitlab_access_token]
  end

  def logged_in?
    session[:username].present?
  end

  def admin?
    !!session[:admin]
  end

  def current_user
    # This is used by audited
    # We only need the id, so there's no need to query the database to load the user
    User.new(id: session[:user_id])
  end

  def track_user_activity!
    Users::TrackActivityService.new(session, cookies).execute
  end
end
