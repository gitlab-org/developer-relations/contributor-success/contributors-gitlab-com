# frozen_string_literal: true

module DateParams
  private

  def from_date
    return 90.days.ago if params[:from_date].blank?

    Time.zone.parse(params[:from_date])
  end

  def to_date
    return nil if params[:to_date].blank?

    Time.zone.parse(params[:to_date])
  end
end
