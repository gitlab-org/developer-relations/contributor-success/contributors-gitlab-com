# frozen_string_literal: true

class FrontendController < ApplicationController
  def index
    render layout: 'vue'
  end
end
