# frozen_string_literal: true

class LeaderboardController < ApplicationController
  include DateParams
  include PaginationConcern

  before_action :ensure_logged_in

  def me
    leaderboard_query = LeaderboardQuery.new(from_date:, to_date:, community_only:, page_size:)
    page_number = leaderboard_query.find_user_page(session[:user_id])
    return redirect_to '/leaderboard' unless page_number

    params[:page] = page_number.to_i
    querystring = params.except(:controller, :action).to_unsafe_h.to_param
    redirect_to "/leaderboard?#{querystring}"
  end

  private

  def community_only
    params[:community_only] != 'false'
  end
end
