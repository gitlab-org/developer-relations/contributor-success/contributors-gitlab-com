# frozen_string_literal: true

class ManageMergeRequestController < ApplicationController
  include SessionUserConcern

  before_action :ensure_logged_in
  before_action :check_required_parameters

  def request_help
    bot_request('help')
  end

  def request_review
    redirect_to "/manage-merge-request?projectId=#{project_id}&mergeRequestIid=#{merge_request_iid}"
  end

  private

  def bot_request(what)
    response = gitlab_rest_service.bot_request(what, project_id, merge_request_iid)

    return render json: response unless response.code == 201

    redirect_to web_url, allow_other_host: true
  end

  def check_required_parameters
    return unless project_id.nil? || merge_request_iid.nil?

    render json: { error: 'Missing parameter(s)' }, status: :bad_request
  end

  def gitlab_rest_service
    @gitlab_rest_service ||= GitlabRestService.new(gitlab_access_token)
  end

  def web_url
    gitlab_rest_service.get_merge_request_web_url(project_id, merge_request_iid)
  end

  def project_id
    params[:project_id]
  end

  def merge_request_iid
    params[:merge_request_iid]
  end
end
