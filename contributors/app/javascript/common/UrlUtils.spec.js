import { buildUrl } from './UrlUtils';

describe('buildUrl', () => {
  it('should return a simple url', () => {
    global.window.location = { href: 'http://localhost' };

    expect(buildUrl('/test').toString()).toBe('http://localhost/test');
  });

  describe('with query', () => {
    it('should return a url with querystring', () => {
      global.window.location = { href: 'http://localhost' };

      expect(buildUrl('/test', { query: { key: 'value', andThis: 5 } }).toString()).toBe('http://localhost/test?key=value&andThis=5');
    });
  });
});
