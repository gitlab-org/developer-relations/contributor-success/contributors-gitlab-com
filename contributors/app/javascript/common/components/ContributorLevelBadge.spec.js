import { shallowMount } from '@vue/test-utils';

import contributorLevelOneBadge from '@images/contributor-level-1.png';
import contributorLevelTwoBadge from '@images/contributor-level-2.png';
import contributorLevelThreeBadge from '@images/contributor-level-3.png';
import contributorLevelFourBadge from '@images/contributor-level-4.png';

import ContributorLevelBadge from './ContributorLevelBadge.vue';

let wrapper;
const createWrapper = ({ badgeSize, contributorLevel } = {}) => {
  wrapper = shallowMount(ContributorLevelBadge, {
    propsData: {
      badgeSize,
      contributorLevel,
    },
  });
};

const img = () => wrapper.find('img');

describe('with default props', () => {
  it('does not render', () => {
    createWrapper();

    expect(img().exists()).toBe(false);
  });
});

describe('with an invalid contributorLevel', () => {
  it('does not render', () => {
    createWrapper({ contributorLevel: 5 });

    expect(img().exists()).toBe(false);
  });
});

describe.each([
  [1, contributorLevelOneBadge],
  [2, contributorLevelTwoBadge],
  [3, contributorLevelThreeBadge],
  [4, contributorLevelFourBadge],
])('when contributorLevel is %i', (level, badgeImage) => {
  it('renders the correct badge and title', () => {
    createWrapper({ contributorLevel: level });

    expect(img().exists()).toBe(true);
    expect(img().attributes('src')).toBe(badgeImage);
    expect(img().attributes('alt')).toBe(`Contributor level ${level} badge`);
    expect(img().attributes('title')).toBe(`Contributor level ${level} badge`);
  });
});

describe('with a custom badgeSize', () => {
  it('render the correct height and width', () => {
    createWrapper({ contributorLevel: 1, badgeSize: 32 });

    expect(img().exists()).toBe(true);
    expect(img().attributes('height')).toBe('32');
    expect(img().attributes('width')).toBe('32');
  });
});
