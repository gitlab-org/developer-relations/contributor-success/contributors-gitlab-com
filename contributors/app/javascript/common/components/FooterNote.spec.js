import { mount } from '@vue/test-utils';

import FooterNote from './FooterNote.vue';

let wrapper;
const createWrapper = (props = {}) => {
  wrapper = mount(FooterNote, { props });
};

it('renders default text', async () => {
  createWrapper();

  expect(wrapper.text()).toEqual('All data is imported from public APIs.');
});

describe('with learnMoreHref property set', () => {
  it('renders default text and learn more link', async () => {
    const learnMoreHref = 'example.com';
    createWrapper({ learnMoreHref });

    expect(wrapper.text()).toEqual('All data is imported from public APIs. Learn more.');
    expect(wrapper.find(`a[href='${learnMoreHref}']`).exists()).toBe(true);
  });

  describe('with customText property set', () => {
    it('renders custom text and learn more link', async () => {
      const learnMoreHref = 'example.com';
      const customText = 'Hello!';
      createWrapper({ customText, learnMoreHref });

      expect(wrapper.text()).toEqual(`${customText} Learn more.`);
      expect(wrapper.find(`a[href='${learnMoreHref}']`).exists()).toBe(true);
    });
  });
});
