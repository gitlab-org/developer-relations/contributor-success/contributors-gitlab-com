import { mount } from '@vue/test-utils';
import { BCard, BProgress, BProgressBar, createBootstrap } from 'bootstrap-vue-next';

import LevelProgress from './LevelProgress.vue';
import PointsDocumentationTooltip from './PointsDocumentationTooltip.vue';

const bootstrap = createBootstrap();
let wrapper;
const createWrapper = async (props = { minimized: false }, userOverrides = {}) => {
  const user = {
    username: 'lee',
    contributorLevel: 1,
    contributorPoints: 25,
    userAdmin: false,
    ...userOverrides,
  };

  wrapper = await mount(
    LevelProgress,
    {
      props,
      global: {
        plugins: [bootstrap],
        provide: { vueData: user },
      },
    },
  );
};

const findInWrapper = testId => wrapper.findByTestId(`progress-bar-${testId}`);

const findProgress = () => wrapper.findComponent(BProgress);
const findProgressBar = () => wrapper.findComponent(BProgressBar);

describe('when minimized', () => {
  it('renders BProgress without title, tooltip, and BCard', async () => {
    await createWrapper({ minimized: true });

    expect(findProgress().exists()).toBe(true);
    expect(findProgressBar().exists()).toBe(true);

    expect(wrapper.findComponent(BCard).exists()).toBe(false);
    expect(wrapper.findComponent(PointsDocumentationTooltip).exists()).toBe(false);
    expect(findInWrapper('subtitle').exists()).toBe(false);
  });
});

describe('when not minimized', () => {
  it('renders ProgressBar with tooltip', async () => {
    await createWrapper();

    expect(findProgress().exists()).toBe(true);
    expect(findProgressBar().exists()).toBe(true);
    expect(wrapper.findComponent(PointsDocumentationTooltip).exists()).toBe(true);
  });

  describe('with level 0 to level 1', () => {
    it('renders progress bar with subtitle', async () => {
      await createWrapper({ minimized: false }, { contributorLevel: 0, contributorPoints: 23 });

      expect(findInWrapper('subtitle').text()).toEqual('Level 1 in 2 points Points, scoring, and contributor level documentation');
      expect(findProgress().props('max')).toBe(25);
      expect(findProgressBar().props('value')).toBe(23);
    });
  });

  describe('with level 1 to level 2', () => {
    it('renders progress bar with subtitle', async () => {
      await createWrapper({ minimized: false }, { contributorLevel: 1, contributorPoints: 183 });

      expect(findInWrapper('subtitle').text()).toEqual('Level 2 in 317 points Points, scoring, and contributor level documentation');
      expect(findProgress().props('max')).toBe(475);
      expect(findProgressBar().props('value')).toBe(158);
    });
  });

  describe('with level 3 to level 4', () => {
    it('renders progress bar with subtitle', async () => {
      await createWrapper({ minimized: false }, { contributorLevel: 3, contributorPoints: 3400 });

      expect(findInWrapper('subtitle').text()).toEqual('Level 4 in 4,100 points Points, scoring, and contributor level documentation');
      expect(findProgress().props('max')).toBe(5000);
      expect(findProgressBar().props('value')).toBe(900);
    });
  });

  describe('with level 4+', () => {
    it('renders progress bar with subtitle', async () => {
      await createWrapper({ minimized: false }, { contributorLevel: 4, contributorPoints: 9999 });

      expect(findInWrapper('subtitle').text()).toEqual('You\'re a top contributor! 9,999 points. The next level-up is coming soon! Points, scoring, and contributor level documentation');
      expect(findProgress().props('max')).toBe(0);
      expect(findProgressBar().props('value')).toBe(2499);
    });
  });

  describe('with contributorData prop', () => {
    it('renders progress bar with subtitle', async () => {
      await createWrapper({ contributorData: { contributorLevel: 1, contributorPoints: 100 } });

      expect(findInWrapper('subtitle').text()).toEqual('Level 2 in 400 points Points, scoring, and contributor level documentation');
      expect(findProgress().props('max')).toBe(475);
      expect(findProgressBar().props('value')).toBe(75);
    });

    describe('with level 4+', () => {
      it('renders progress bar with subtitle', async () => {
        await createWrapper({ contributorData: { contributorLevel: 4, contributorPoints: 100000 } });

        expect(findInWrapper('subtitle').text()).toEqual('Top contributor! 100,000 points Points, scoring, and contributor level documentation');
        expect(findProgress().props('max')).toBe(0);
        expect(findProgressBar().props('value')).toBe(92500);
      });
    });
  });

  describe('when points do not match level', () => {
    it('renders progress bar with subtitle', async () => {
      await createWrapper({ minimized: false }, { contributorLevel: 2, contributorPoints: 200 });

      expect(findInWrapper('subtitle').text()).toEqual('Level 3 in 2,300 points Points, scoring, and contributor level documentation');
      expect(findProgress().props('max')).toBe(2500);
      expect(findProgressBar().props('value')).toBe(200);
    });

    describe('with level 4+', () => {
      it('renders progress bar with subtitle', async () => {
        await createWrapper({ contributorData: { contributorLevel: 4, contributorPoints: 200 } });

        expect(findInWrapper('subtitle').text()).toEqual('Top contributor! 200 points Points, scoring, and contributor level documentation');
        expect(findProgress().props('max')).toBe(7500);
        expect(findProgressBar().props('value')).toBe(7500);
      });
    });

    describe('with contributorData prop', () => {
      it('renders progress bar with subtitle', async () => {
        await createWrapper({ contributorData: { contributorLevel: 3, contributorPoints: 200 } });

        expect(findInWrapper('subtitle').text()).toEqual('Level 4 in 7,300 points Points, scoring, and contributor level documentation');
        expect(findProgress().props('max')).toBe(7500);
        expect(findProgressBar().props('value')).toBe(200);
      });
    });
  });
});
