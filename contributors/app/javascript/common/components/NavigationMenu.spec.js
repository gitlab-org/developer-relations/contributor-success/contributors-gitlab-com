import { mount, RouterLinkStub } from '@vue/test-utils';
import {
  BDropdownItem,
  BDropdownHeader,
  BNavbarBrand,
  BNavItem,
  BNavItemDropdown,
} from 'bootstrap-vue-next';

import ContributorLevelBadge from './ContributorLevelBadge.vue';
import LevelProgress from './LevelProgress.vue';
import NavigationMenu from './NavigationMenu.vue';

global.ResizeObserver = class ResizeObserver { observe() {} };

let wrapper;
const createWrapper = async ({ username, contributorLevel = 0, userAdmin = false } = {}) => {
  wrapper = await mount(NavigationMenu, {
    global: {
      stubs: { RouterLink: RouterLinkStub, RouterView: '' },
      provide: { vueData: { username, contributorLevel, userAdmin } },
    },
  });
};

const dropdownItems = () => wrapper.findAllComponents(BNavItemDropdown).at(1)?.findAllComponents(BDropdownItem).map(item => item.text());
const navItems = () => wrapper.findAllComponents(BNavItem).map(item => item.text());
const contributorLevelBadge = () => wrapper.findComponent(ContributorLevelBadge);

describe('when not logged in', () => {
  beforeEach(() => {
    createWrapper();
  });

  it('renders app name', () => {
    expect(wrapper.findComponent(BNavbarBrand).text()).toBe('Contributors');
  });

  it('renders expected links', () => {
    expect(navItems()).toStrictEqual(['Leaderboard', 'Organizations', 'Rewards', 'User Guide', 'Login']);
  });

  it('renders expected dropdown items', () => {
    expect(dropdownItems()).toBe(undefined);
  });

  it('does not render a contributor level badge', () => {
    expect(contributorLevelBadge().exists()).toBe(false);
  });

  it('does not render level progress', () => {
    createWrapper();

    expect(wrapper.findComponent(LevelProgress).exists()).toBe(false);
  });
});

describe('when logged in', () => {
  describe('as a regular user', () => {
    beforeEach(() => {
      createWrapper({ username: 'lee', contributorLevel: 1 });
    });

    it('renders logged in username', () => {
      const dropdownText = wrapper.findAllComponents(BNavItemDropdown).at(1).text();

      expect(dropdownText).toContain('lee');
    });

    it('renders expected links', () => {
      expect(navItems()).toStrictEqual(['Leaderboard', 'Organizations', 'Rewards']);
    });

    it('renders expected dropdown items', () => {
      expect(wrapper.findComponent(BDropdownHeader).exists()).toBe(true);
      expect(dropdownItems()).toStrictEqual(['Profile', 'GitLab profile', 'User guide', 'Logout']);
    });

    it('renders a contributor level badge', () => {
      expect(contributorLevelBadge().exists()).toBe(true);
    });

    it('renders level progress', () => {
      createWrapper();

      const levelProgress = wrapper.findComponent(LevelProgress);
      expect(levelProgress.exists()).toBe(true);
      expect(levelProgress.props('minimized')).toBe(true);
    });
  });

  describe('as an admin', () => {
    beforeEach(() => {
      createWrapper({ username: 'lee', contributorLevel: 1, userAdmin: true });
    });

    it('renders expected links', () => {
      expect(navItems()).toStrictEqual(['Leaderboard', 'Organizations', 'Rewards']);
    });
  });
});
