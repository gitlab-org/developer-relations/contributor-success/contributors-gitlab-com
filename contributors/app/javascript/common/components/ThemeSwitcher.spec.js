import { mount } from '@vue/test-utils';
import {
  BDropdownItem,
  BNavItemDropdown,
} from 'bootstrap-vue-next';
import { nextTick } from 'vue';

import { store } from '~/store';

import ThemeSwitcher from './ThemeSwitcher.vue';

let wrapper;
const createWrapper = () => {
  wrapper = mount(ThemeSwitcher);
};

const themeDropdown = () => wrapper.findComponent(BNavItemDropdown);
const darkThemeToggle = () => themeDropdown().find('.dropdown-item');
const lightThemeToggle = () => themeDropdown().findAll('.dropdown-item').at(1);

it('renders theme selector', () => {
  createWrapper();
  const themes = themeDropdown().findAllComponents(BDropdownItem).map(item => item.text());

  expect(themes).toStrictEqual(['dark', 'light', 'auto']);
});

describe('when switching theme', () => {
  beforeEach(() => {
    createWrapper();
  });

  it('defaults to light', async () => {
    expect(wrapper.find('.dropdown-item.active').text()).toBe('light');
    expect(global.document.documentElement.classList.value).toBe('');
  });

  it('applies dark css class', async () => {
    themeDropdown().trigger('click');
    darkThemeToggle().trigger('click');
    await nextTick();

    expect(global.document.documentElement.classList.value).toBe('dark');
  });

  it('removes dark css class', async () => {
    themeDropdown().trigger('click');
    darkThemeToggle().trigger('click');
    themeDropdown().trigger('click');
    lightThemeToggle().trigger('click');
    await nextTick();

    expect(global.document.documentElement.classList.value).toBe('');
  });

  it('saves theme to store', async () => {
    const spy = vi.spyOn(store, 'setTheme');
    themeDropdown().trigger('click');
    darkThemeToggle().trigger('click');
    await nextTick();

    expect(spy).toHaveBeenCalled('dark');
  });
});
