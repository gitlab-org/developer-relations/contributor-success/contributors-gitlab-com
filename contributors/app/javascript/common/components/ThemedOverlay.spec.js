import { shallowMount } from '@vue/test-utils';
import { BOverlay } from 'bootstrap-vue-next';

import ThemedOverlay from './ThemedOverlay.vue';

let wrapper;
const createWrapper = (props = {}) => {
  wrapper = shallowMount(ThemedOverlay, { props });
};

const findBOverlay = () => wrapper.findComponent(BOverlay);

describe('on mount', () => {
  it('passes show prop to BOverlay', () => {
    createWrapper({ show: true });

    expect(findBOverlay().props('show')).toBe(true);
  });

  it('sets BOverlay variant to theme', () => {
    vi.mock('~/store.js', () => {
      return {
        store: { theme: 'dark' },
      };
    });
    createWrapper();

    expect(findBOverlay().props('variant')).toBe('dark');
  });
});
