import { mount } from '@vue/test-utils';
import { BButton, createBootstrap } from 'bootstrap-vue-next';
import { reactive } from 'vue';

import ContributorsTable from '~/common/components/ContributorsTable.vue';
import RoutePagination from '~/common/components/RoutePagination.vue';
import ThemedOverlay from '~/common/components/ThemedOverlay.vue';
import { daysAgo } from '~/common/DateUtils';

import LeaderboardPage from './LeaderboardPage.vue';

const bootstrap = createBootstrap();
let wrapper;
let reactiveRoute;
const createWrapper = () => {
  wrapper = mount(LeaderboardPage, {
    global: {
      plugins: [bootstrap],
    },
  });
};

const mockResponse = {
  metadata: {
    total_count: 2,
    current_page: 1,
    per_page: 50,
  },
  records: [
    {
      id: 5211906,
      username: 'csouthard',
      merged_merge_requests: 4,
      merged_with_issues: 1,
      opened_merge_requests: 4,
      opened_issues: 0,
      merged_commits: 4,
      added_notes: 0,
      score: 430,
      contributor_level: 1,
      bonus_points: 0,
    },
    {
      id: 8198564,
      username: 'rkadam3',
      merged_merge_requests: 2,
      merged_with_issues: 0,
      opened_merge_requests: 3,
      opened_issues: 0,
      merged_commits: 2,
      added_notes: 1,
      score: 221,
      contributor_level: 1,
      bonus_points: 0,
    },
  ],
};

beforeEach(() => {
  reactiveRoute = reactive({ query: {} });
  vi.mock('vue-router', async () => {
    return {
      useRoute: vi.fn(() => reactiveRoute),
      useRouter: vi.fn(() => ({
        push: vi.fn(({ query }) => {
          reactiveRoute.query = { ...reactiveRoute.query, ...query };
        }),
      })),
    };
  });
});

describe('while loading', () => {
  beforeEach(() => {
    global.fetch = vi.fn(() => new Promise(() => {}));

    createWrapper();
  });

  it('renders overlay with loading state', () => {
    expect(wrapper.findComponent(ThemedOverlay).props('show')).toBe(true);
  });

  it('sends the correct default api query', () => {
    const url = new URL('/api/v1/leaderboard', window.location.href);
    url.searchParams.append('from_date', daysAgo(90));
    url.searchParams.append('to_date', '');
    url.searchParams.append('community_only', true);
    url.searchParams.append('page', 1);

    expect(global.fetch).toHaveBeenCalledWith(url);
  });

  describe('with modified params', () => {
    it('sends the correct api query', async () => {
      wrapper.findByTestId('filter-card-from-date').setValue('2022-01-01');
      wrapper.findByTestId('filter-card-to-date').setValue('2023-01-01');
      wrapper.findByTestId('filter-card-community-only').setChecked(false);
      wrapper.findByTestId('filter-card-search-input').setValue('Bob');
      const filterButton = wrapper.findComponent(BButton);
      await filterButton.trigger('click');

      const url = new URL('/api/v1/leaderboard', window.location.href);
      url.searchParams.append('from_date', '2022-01-01');
      url.searchParams.append('to_date', '2023-01-01');
      url.searchParams.append('community_only', false);
      url.searchParams.append('page', 1);
      url.searchParams.append('search', 'Bob');

      expect(global.fetch).toHaveBeenCalledWith(url);
    });
  });
});

describe('once loaded', () => {
  beforeEach(async () => {
    global.fetch = vi.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockResponse),
      }),
    );

    reactiveRoute.query = { page: 2 };
    await createWrapper();
  });

  it('renders ContributorsTable with not busy state', () => {
    expect(wrapper.findComponent(ContributorsTable).props('busy')).toBe(false);
  });

  it('renders ContributorsTable with expected contributors', () => {
    const contributors = wrapper.findComponent(ContributorsTable).props('contributors');

    expect(contributors).toStrictEqual(mockResponse.records);
  });

  it('renders pagination', () => {
    const pagination = wrapper.findComponent(RoutePagination);

    expect(pagination.exists()).toBe(true);
    expect(pagination.props('totalRows')).toEqual(2);
    expect(pagination.props('perPage')).toEqual(50);
  });

  it('sends the correct api query with changed page', async () => {
    const url = new URL('/api/v1/leaderboard', window.location.href);
    url.searchParams.append('from_date', daysAgo(90));
    url.searchParams.append('to_date', '');
    url.searchParams.append('community_only', true);
    url.searchParams.append('page', 2);

    expect(global.fetch).toHaveBeenCalledWith(url);
  });
});
