import { mount } from '@vue/test-utils';
import { BButton, BForm, createBootstrap } from 'bootstrap-vue-next';

import AddEventModal from './AddEventModal.vue';

const bootstrap = createBootstrap();
let modalWrapper;
let formWrapper;
const createWrapper = () => {
  modalWrapper = mount(AddEventModal, {
    props: {
      modelValue: true,
    },
    global: { plugins: [bootstrap] },
  });
  formWrapper = modalWrapper.findComponent(BForm);
};

const findField = testId => formWrapper.findByTestId(`event-modal-${testId}`);

const fillForm = async (optional = true) => {
  await findField('input-name').setValue('Community event #999');
  await findField('input-date').setValue('2024-12-24');
  if (optional) await findField('input-link').setValue('example.com');
  await findField('radio-role').find('input[type="radio"][value="organizer"]').setChecked();
  await findField('radio-virtual').find('input[type="radio"][value="true"]').setChecked();
  await findField('select-size').setValue('medium');
  if (optional) await findField('input-note').setValue('test note about an event');
};
const submitForm = async () => {
  await formWrapper.findComponent(BButton).trigger('click');
};

beforeEach(() => {
  global.fetch = vi.fn(() => Promise.resolve({ json: () => ({ web_url: 'example.com' }) }));
  global.window.open = vi.fn();
  createWrapper();
});

it('renders the modal with correct content', () => {
  expect(formWrapper.text()).toBe(
    'NameDateLinkRoleSpeakerOrganizerTypeVirtualIn-personSizeSmall (< 20)Medium '
    + '(20 - 100)Large (100+)Note Add event',
  );
});

it('cannot be submitted without filling in the form', async () => {
  await submitForm();

  expect(formWrapper.emitted('submit')).toBeFalsy();
});

it('can be submitted with fields filled out', async () => {
  await fillForm();
  await submitForm();

  expect(formWrapper.emitted('submit')).toBeTruthy();
});

it('can be submitted without optional fields', async () => {
  await fillForm(false);
  await submitForm();

  expect(formWrapper.emitted('submit')).toBeTruthy();
});

describe('when form is submitted', () => {
  it('calls add event API with expected body', async () => {
    await fillForm();
    await submitForm();

    expect(global.fetch).toHaveBeenCalledWith('/api/v1/events', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: 'Community event #999',
        date: '2024-12-24',
        link: 'example.com',
        role: 'organizer',
        virtual: true,
        size: 'medium',
        note: 'test note about an event',
      }),
    });
  });

  it('emits eventAdded', async () => {
    await fillForm();
    await submitForm();

    expect(modalWrapper.emitted('eventAdded')).toBeTruthy();
  });

  it('opens event issue link in new tab', async () => {
    await fillForm();
    await submitForm();

    expect(global.window.open).toHaveBeenCalledWith('example.com', '_blank');
  });
});
