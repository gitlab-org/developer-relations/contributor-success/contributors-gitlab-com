import { mount } from '@vue/test-utils';
import { BButton, BForm, BFormSelect, BInput, createBootstrap } from 'bootstrap-vue-next';

import AddPointsModal from './AddPointsModal.vue';

const bootstrap = createBootstrap();
let modalWrapper;
let formWrapper;
const createWrapper = () => {
  modalWrapper = mount(AddPointsModal, {
    props: {
      userId: 123,
      modelValue: true,
    },
    global: { plugins: [bootstrap] },
  });
  formWrapper = modalWrapper.findComponent(BForm);
};

const fillForm = async () => {
  await formWrapper.findAllComponents(BInput).at(0).setValue('Test reason');
  await formWrapper.findAllComponents(BInput).at(1).setValue(10);
  await formWrapper.findComponent(BFormSelect).setValue('other');
};
const submitForm = async () => {
  await formWrapper.findComponent(BButton).trigger('click');
};

beforeEach(() => {
  global.fetch = vi.fn(() => Promise.resolve({ json: () => ({}) }));
  createWrapper();
});

it('renders the modal with correct content', () => {
  expect(formWrapper.text()).toBe('Reason:Points:Activity Type:Community supportContent creationEvent activitySocial engagementOther Add points');
});

it('cannot be submitted without filling in the form', async () => {
  await submitForm();

  expect(formWrapper.emitted('submit')).toBeFalsy();
});

it('can be submitted with both fields filled out', async () => {
  await fillForm();
  await submitForm();

  expect(formWrapper.emitted('submit')).toBeTruthy();
});

it('calls addPoints API when form is submitted', async () => {
  await fillForm();
  await submitForm();

  expect(global.fetch).toHaveBeenCalledWith('/api/v1/users/123/add_points', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ reason: 'Test reason', points: 10, activity_type: 'other' }),
  });
});

it('emits pointsAdded event', async () => {
  await fillForm();
  await submitForm();

  expect(modalWrapper.emitted('pointsAdded')).toBeTruthy();
});
