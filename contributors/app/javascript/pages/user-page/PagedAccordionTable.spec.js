import { mount } from '@vue/test-utils';
import { BAccordion, BPagination, BTable, createBootstrap } from 'bootstrap-vue-next';

import PagedAccordionTable from './PagedAccordionTable.vue';

const fields = ['title'];
const sortKey = 'created_at';
const item = [
  {
    title: 'Merge request title1',
    opened_date: '2022-01-01',
    merged_date: '2022-01-02',
    web_url: 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1',
  },
];

let wrapper;
const createWrapper = ({ items = [item] } = {}) => {
  wrapper = mount(PagedAccordionTable, {
    props: {
      title: 'Merge Requests',
      items,
      fields,
      busy: false,
      sortKey,
    },
    global: { plugins: [bootstrap] },
  });
};

const bootstrap = createBootstrap();

const findPaginationControl = () => wrapper.findComponent(BPagination);
const findTable = () => wrapper.findComponent(BTable);

it('renders header', () => {
  createWrapper();

  const accordion = wrapper.findComponent(BAccordion);
  expect(accordion.text()).contain('Merge Requests (1)');
});

it('passes props to table', () => {
  createWrapper();

  const table = findTable();
  const { key, order } = table.props('sortBy')[0];
  expect(table.props('fields')).toStrictEqual(fields);
  expect(table.props('items')).toStrictEqual([item]);
  expect(table.props('busy')).toBe(false);
  expect(key).toBe(sortKey);
  expect(order).toBe('desc');
});

describe('with only one page of data', () => {
  it('does not render pagination control', () => {
    createWrapper();

    expect(findPaginationControl().exists()).toBe(false);
  });
});

describe('with more than one page of data', () => {
  it('renders pagination control', () => {
    createWrapper({ items: Array(11).fill(item) });

    expect(findPaginationControl().exists()).toBe(true);
    expect(findPaginationControl().props('totalRows')).toBe(11);
  });

  it('sets the currentPage table prop when paging', async () => {
    createWrapper({ items: Array(11).fill(item) });

    await findPaginationControl().vm.$emit('update:modelValue', 2);

    expect(findTable().props('currentPage')).toBe(2);
  });
});
