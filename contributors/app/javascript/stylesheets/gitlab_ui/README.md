# Why do we have this folder?

[GitLab UI](https://gitlab.com/gitlab-org/gitlab-ui/-/tree/main) has dependency on legacy unsupported Vue.js 2.
This project uses V3. That is the reason why we can't import files from package and use this folder instead.
