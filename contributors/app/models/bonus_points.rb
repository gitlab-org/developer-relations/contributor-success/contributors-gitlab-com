# frozen_string_literal: true

class BonusPoints < ApplicationRecord
  self.table_name = 'bonus_points'

  belongs_to :awarded_by_user, class_name: 'User'
  belongs_to :user

  REQUIRED_FIELDS = %i[
    awarded_by_user_id
    points
    reason
    user_id
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)

  scope :awarded_between, ->(from_date, to_date) { where(created_at: from_date..to_date) }
  scope :user_totals, -> { group(:user_id).sum(:points) }

  # NOTE: Update `app/app/javascript/common/Constants.js` when updating this list
  enum :activity_type, {
    other: 0,
    social: 1,
    event: 2,
    support: 3,
    content: 4
  }, prefix: true
end
