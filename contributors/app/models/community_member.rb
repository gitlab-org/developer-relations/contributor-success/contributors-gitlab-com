# frozen_string_literal: true

class CommunityMember < ApplicationRecord
  belongs_to :user

  REQUIRED_FIELDS = %i[
    user_id
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)
end
