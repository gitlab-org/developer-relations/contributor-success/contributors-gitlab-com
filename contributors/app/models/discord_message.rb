# frozen_string_literal: true

class DiscordMessage < ApplicationRecord
  belongs_to :user

  REQUIRED_FIELDS = %i[
    added_date
    channel_id
    id
    user_id
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)
  validates :reply, inclusion: [true, false]

  CHANNELS = {
    '997442331202564176' => '#contribute',
    '782473368876220426' => '#general',
    '1036634937438253137' => '#showcase',
    '1004018661176135750' => '#heroes'
  }.freeze

  scope :added_between, ->(from_date, to_date) { where(added_date: from_date..to_date) }
  scope :user_counts, -> { group(:user_id).count }

  def channel
    CHANNELS[channel_id]
  end

  def web_url
    "https://discord.com/channels/778180511088640070/#{channel_id}/#{id}"
  end
end
