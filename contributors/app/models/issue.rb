# frozen_string_literal: true

class Issue < ApplicationRecord
  belongs_to :project, optional: true
  belongs_to :user

  has_many :notes, dependent: :delete_all

  REQUIRED_FIELDS = %i[
    id
    title
    opened_date
    web_url
    upvotes
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)

  enum :state_id, {
    opened: 1,
    closed: 2
  }

  scope :opened_between, ->(from_date, to_date) { where(opened_date: from_date..to_date) }
  scope :user_counts, -> { group(:user_id).count }
end
