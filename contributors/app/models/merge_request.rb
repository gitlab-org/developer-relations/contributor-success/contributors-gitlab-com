# frozen_string_literal: true

class MergeRequest < ApplicationRecord
  belongs_to :project, optional: true
  belongs_to :user

  has_many :notes, dependent: :delete_all
  has_many :commits, dependent: :delete_all

  REQUIRED_FIELDS = %i[
    id
    title
    state_id
    opened_date
    web_url
    upvotes
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)

  enum :state_id, {
    opened: 1,
    closed: 2,
    merged: 3,
    locked: 4
  }

  scope :merged_between, ->(from_date, to_date) { where(merged_date: from_date..to_date) }
  scope :opened_between, ->(from_date, to_date) { where(opened_date: from_date..to_date) }
  scope :user_counts, -> { group(:user_id).count }
  scope :with_commits, -> { joins(:commits) }
  scope :with_linked_issues, -> { where('labels @> ?', '["linked-issue"]') }
end
