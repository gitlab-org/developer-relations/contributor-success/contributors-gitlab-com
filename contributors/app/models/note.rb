# frozen_string_literal: true

class Note < ApplicationRecord
  belongs_to :user
  belongs_to :issue, optional: true
  belongs_to :merge_request, optional: true

  validate :noteable_present

  REQUIRED_FIELDS = %i[
    id
    user_id
    added_date
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)

  scope :added_between, ->(from_date, to_date) { where(added_date: from_date..to_date) }
  scope :user_counts, -> { group(:user_id).count }

  def noteable
    issue || merge_request
  end

  def web_url
    "#{noteable.web_url}#note_#{id}"
  end

  def noteable_present
    return if issue.present? ^ merge_request.present?

    errors.add(:base, 'Issue or merge request must be present')
  end
end
