# frozen_string_literal: true

class TeamMember < ApplicationRecord
  REQUIRED_FIELDS = %i[
    username
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)
  validates :mr_coach, inclusion: [true, false]
  validates :available, inclusion: [true, false]

  scope :mr_coach, -> { where(mr_coach: true) }
  scope :available, -> { where(available: true) }

  def self.random_available_coach
    mr_coach.available.order('RANDOM()').first.username
  end
end
