# frozen_string_literal: true

class UserProfile < ApplicationRecord
  # Audit on platform_first_login_at and platform_last_activity_at produces:
  # Tried to dump unspecified class: ActiveSupport::TimeWithZone
  audited(except: %i[platform_first_login_at platform_last_activity_at])

  belongs_to :user

  REQUIRED_FIELDS = %i[
    user_id
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)
end
