# frozen_string_literal: true

class LeaderboardQuery < ApplicationQuery
  def initialize(options = { page: 1, page_size: 20 })
    super()
    @options = options
  end

  def execute
    query
      .page(options[:page])
      .per(page_size)
  end

  def find_user_page(user_id)
    exec_query(find_user_page_query(user_id)).first&.[]('page_number')
  end

  private

  attr_reader :options

  def query
    points_service = PointsService.new(**points_service_options)
    relation = points_service.fetch_data
    relation = relation.username_search(options[:search]) if options[:search]
    relation.order(score: :desc)
  end

  def find_user_page_query(user_id)
    <<-SQL.squish.freeze
      SELECT
        page_number
      FROM (
        SELECT
          user_id,
          CEIL((ROW_NUMBER() OVER (ORDER BY score DESC)) / #{page_size.to_f}) AS page_number
        FROM
          (#{query.to_sql}) AS leaderboard
      ) AS pages
      WHERE
        user_id = #{user_id}
    SQL
  end

  def points_service_options
    options.slice(:from_date, :to_date, :community_only)
  end

  def page_size
    options[:page_size]
  end
end
