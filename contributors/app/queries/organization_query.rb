# frozen_string_literal: true

class OrganizationQuery
  def initialize(name, options)
    @name = name
    @options = options
  end

  def execute
    contributors = fetch_contributors
    {
      name:,
      contributors:
    }
  end

  private

  attr_reader :name, :options

  def fetch_contributors
    service = ::PointsService.new(
      community_only: true,
      from_date: options[:from_date],
      to_date: options[:to_date],
      users_scope: User.where(organization: name)
    )
    service.fetch_data.order(score: :desc)
  end
end
