# frozen_string_literal: true

class ContributorsSerializer < CollectionSerializer
  def as_json
    return [] if records.blank?
    return records unless current_user_id

    current_user_index = records.index { |contributor| contributor[:user_id] == current_user_id }
    return records unless current_user_index

    contributors = records.as_json
    contributors[current_user_index][:current_user] = true
    contributors
  end
end
