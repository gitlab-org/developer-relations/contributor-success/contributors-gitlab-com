# frozen_string_literal: true

class RewardsSerializer < CollectionSerializer
  def as_json
    records.map { |reward| serialize_record(reward) }
  end

  private

  def serialize_record(reward)
    common = {
      username: reward.user.username,
      user_id: reward.user_id,
      reason: reward.reason,
      credits: reward.credits,
      awarded_by: reward.awarded_by_user.username,
      awarded_by_web_url: reward.awarded_by_user.web_url,
      created_at: reward.created_at
    }
    return common unless options[:admin]

    common.merge(gift_code: reward.gift_code, issue_web_url: reward.issue_web_url)
  end
end
