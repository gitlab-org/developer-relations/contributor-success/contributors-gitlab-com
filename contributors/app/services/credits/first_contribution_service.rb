# frozen_string_literal: true

module Credits
  class FirstContributionService
    def execute
      Views::RecentActivitySummary.refresh
      users = fetch_eligible_users
      users.each { |user| Rails.logger.info("Awarding credits to user: #{user.username} with user id #{user.id}") }
      award_credits(users)
    end

    def fetch_eligible_users
      five_days_ago = 5.days.ago.to_date
      users = users_who_merged_since(five_days_ago)
      users = users.where.not(user_id: user_who_merged_before(five_days_ago, users))
      users = users.where.not(user_id: users_with_rewards(users))
      User.where(id: users)
    end

    def award_credits(users)
      gitlab_access_token = ENV.fetch('GITLAB_ACCESS_TOKEN')
      awarded_by_user_id = ENV.fetch('GITLAB_USER_ID')
      credits = ENV.fetch('FIRST_CONTRIBUTION_CREDITS', 5).to_i
      reason = Reward.reasons[:first_contribution]
      note = 'Congratulations on having your first MR merged!'

      rewards_service = RewardsService.new(gitlab_access_token)
      users.each do |user|
        rewards_service.issue(
          user_id: user['id'],
          username: user['username'],
          credits:,
          reason:,
          note:,
          awarded_by_user_id:
        )
      end
    end

    private

    def users_who_merged_since(date)
      Views::RecentActivitySummary
        .where(activity_date: date..)
        .where(merged_mrs: 1..)
        .where(community_member: true)
        .select(:user_id)
        .distinct
    end

    def user_who_merged_before(date, users)
      MergeRequest
        .where(merged_date: ...date)
        .where(user_id: users)
        .select(:user_id)
        .distinct
    end

    def users_with_rewards(users)
      Reward
        .where(user_id: users)
        .select(:user_id)
        .distinct
    end
  end
end
