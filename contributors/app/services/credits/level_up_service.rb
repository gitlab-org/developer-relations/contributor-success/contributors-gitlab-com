# frozen_string_literal: true

require 'pry-byebug'

module Credits
  class LevelUpService
    LEVEL_ACHIEVEMENTS = {
      1 => 59,
      2 => 60,
      3 => 61,
      4 => 62
    }.freeze
    LEVEL_CREDITS = {
      1 => 5,
      2 => 50,
      3 => 150,
      4 => 300
    }.freeze
    LEVEL_UP_REWARD_REASON = Reward.reasons[:level_up]

    def initialize
      @awarded_by_user_id = ENV.fetch('GITLAB_USER_ID')
      @lookback_date = 5.days.ago.to_date
      gitlab_api_token = ENV.fetch('GITLAB_API_TOKEN')
      @rewards_service = RewardsService.new(gitlab_api_token)
    end

    def execute
      Views::RecentActivitySummary.refresh
      users = fetch_eligible_users
      users.each do |user|
        update_level(user)
        award_credits(user)
        revoke_and_award_achievement(user)
      end
    end

    private

    attr_reader :awarded_by_user_id, :lookback_date, :rewards_service

    def fetch_eligible_users
      recent_active_users = users_with_activity_since(lookback_date)
      users_to_level_up(recent_active_users)
    end

    def update_level(user)
      User.where(id: user[:id]).update_all(contributor_level: user[:calculated_level])
    end

    def revoke_and_award_achievement(user)
      new_achievement_gid = achievement_gid(LEVEL_ACHIEVEMENTS[user[:calculated_level]])
      existing_level_achievement = current_achievement(user[:id])

      already_has_achievement = existing_level_achievement&.[](:achievement_gid) == new_achievement_gid
      return if already_has_achievement

      Gitlab::AchievementsService.revoke(existing_level_achievement[:gid]) if existing_level_achievement
      Gitlab::AchievementsService.award(user[:id], new_achievement_gid)
    end

    def current_achievement(user_id)
      user_achievements = Gitlab::AchievementsService.current(user_id)

      level_achievement_gids = LEVEL_ACHIEVEMENTS.values.map { achievement_gid(it) }
      existing_level_achievement = user_achievements.find do |user_achievement|
        level_achievement_gids.include?(user_achievement.dig('achievement', 'id'))
      end
      return unless existing_level_achievement

      {
        gid: existing_level_achievement['id'],
        achievement_gid: existing_level_achievement.dig('achievement', 'id')
      }
    end

    def award_credits(user)
      new_level = user[:calculated_level]
      user_id = user[:id]
      username = user[:username]
      credits = LEVEL_CREDITS[new_level]
      note = "Congratulations on becoming a level #{new_level} contributor!"

      Rails.logger.info("Awarding #{credits} credits to user: #{username} with user id #{user_id}")

      rewards_service.issue(
        user_id:,
        username:,
        credits:,
        reason: LEVEL_UP_REWARD_REASON,
        note:,
        awarded_by_user_id:
      )
    end

    def users_to_level_up(recent_active_users)
      users = user_points_and_levels(recent_active_users)
      users.map do |user|
        calculated_level = PointsService.contributor_level(user.points)

        next if user.contributor_level >= calculated_level

        {
          id: user.id,
          username: user.username,
          calculated_level:
        }
      end.compact
    end

    def users_with_activity_since(date)
      Views::RecentActivitySummary
        .where(activity_date: date..)
        .where(community_member: true)
        .group(:user_id, :username)
        .select(:user_id)
    end

    def user_points_and_levels(users)
      User
        .where(id: users)
        .select(:id, :username, :points, :contributor_level)
    end

    def achievement_gid(achievement_id)
      "gid://gitlab/Achievements::Achievement/#{achievement_id}"
    end
  end
end
