# frozen_string_literal: true

class FetchContributorSuccessUsersService
  class << self
    include GraphqlHelper

    def execute
      response = GitlabGraphqlService.execute(query)
      contributor_success_users_ids(response)
    end

    private

    def query
      <<~GRAPHQL
        query {
          group(fullPath: "gitlab-org/developer-relations/contributor-success") {
            groupMembers(relations: DIRECT) {
              nodes {
                user {
                  id
                }
              }
            }
          }
        }
      GRAPHQL
    end

    def contributor_success_users_ids(data)
      data.dig('data', 'group', 'groupMembers', 'nodes').map { |user| id_from_graphql_id(user.dig('user', 'id')) }
    end
  end
end
