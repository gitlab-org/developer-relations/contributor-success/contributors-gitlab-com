# frozen_string_literal: true

require 'httparty'

class GitlabGraphqlService
  CI_API_GRAPHQL_URL = 'https://gitlab.com/api/graphql'
  MAX_TRIES = 5
  RETRY_SLEEP = 10

  class << self
    def execute(query, with_retries: true)
      response = with_retries ? post_query_with_retries(query) : post_query(query)

      Rails.logger.info { "Received a non-default error code #{response.code}" } if response.code != 200
      response.parsed_response
    end

    private

    def post_query_with_retries(query)
      MAX_TRIES.times do |attempt|
        return post_query(query)
      rescue => e # rubocop:disable Style/RescueStandardError
        Rails.logger.warn { "Fetch retry ##{attempt + 1} due to error: #{e.message}" }
        raise e if attempt == MAX_TRIES - 1

        sleep(RETRY_SLEEP)
      end
    end

    def post_query(query)
      HTTParty.post(
        graphql_api_url,
        body: { query: }.to_json,
        headers: {
          'Content-Type' => 'application/json',
          'Cookie' => cookies,
          'User-Agent' => 'contributorsDot'
        }.merge(authorization_header)
      )
    end

    def graphql_api_url
      ENV.fetch('CI_API_GRAPHQL_URL', CI_API_GRAPHQL_URL)
    end

    def authorization_header
      gitlab_api_token = ENV.fetch('GITLAB_API_TOKEN', nil)
      return {} if gitlab_api_token.nil?

      { 'Authorization' => "Bearer #{gitlab_api_token}" }
    end

    def cookies
      cookie_hash = HTTParty::CookieHash.new
      cookie_hash.add_cookies('gitlab_canary=true')
      cookie_hash.to_cookie_string
    end
  end
end
