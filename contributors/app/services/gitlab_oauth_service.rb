# frozen_string_literal: true

require 'httparty'

class GitlabOauthService
  GITLAB_OAUTH_APP_ID = '66cca785e9fa40526f3736e182698741b4ccbb04910ae105c0f86c972fc53db8'
  REDIRECT_URI = 'http://localhost:3030/oauth_callback'
  BASE_HOST = 'gitlab.com'

  class << self
    def authorize(code)
      parameters = uri_parameters(client_secret:, code:, grant_type: 'authorization_code')

      response = HTTParty.post(
        oauth_token_url,
        body: parameters
      )
      json_response = JSON.parse(response.body)
      gitlab_access_token = json_response['access_token']

      user = user(gitlab_access_token)
      {
        user_id: user['id'],
        username: user['username'],
        gitlab_access_token:,
        gitlab_refresh_token: json_response['refresh_token'],
        gitlab_expires_at: json_response['created_at'] + json_response['expires_in']
      }
    end

    def oauth_authorize_url
      response_type = 'code'
      scope = 'api'
      state = SecureRandom.hex(16)

      URI::HTTPS.build(
        host: BASE_HOST,
        path: '/oauth/authorize',
        query: uri_parameters(scope:, response_type:, state:).to_query
      )
    end

    def refresh(refresh_token)
      parameters = uri_parameters(client_secret:, refresh_token:, grant_type: 'refresh_token')

      response = HTTParty.post(
        oauth_token_url,
        body: parameters
      )

      json_response = JSON.parse(response.body)
      {
        gitlab_access_token: json_response['access_token'],
        gitlab_refresh_token: json_response['refresh_token'],
        gitlab_expires_at: json_response['created_at'] + json_response['expires_in']
      }
    end

    def user(token)
      response = HTTParty.get(
        user_api_url,
        headers: { 'Authorization' => "Bearer #{token}" }
      )

      JSON.parse(response.body)
    end

    private

    def client_id
      ENV.fetch('APP_ID', GITLAB_OAUTH_APP_ID)
    end

    def client_secret
      ENV.fetch('APP_SECRET')
    end

    def oauth_token_url
      URI::HTTPS.build(
        host: BASE_HOST,
        path: '/oauth/token'
      )
    end

    def redirect_uri
      ENV.fetch('OAUTH_CALLBACK_URL', REDIRECT_URI)
    end

    def uri_parameters(**kwargs)
      {
        client_id:,
        redirect_uri:
      }.merge(kwargs)
    end

    def user_api_url
      URI::HTTPS.build(
        host: BASE_HOST,
        path: '/api/v4/user'
      )
    end
  end
end
