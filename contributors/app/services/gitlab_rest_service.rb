# frozen_string_literal: true

require 'httparty'

class GitlabRestService
  REWARDS_PROJECT_ID = 48_412_664
  EVENTS_PROJECT_ID = 65_972_950

  class GitlabResponseFailure < StandardError; end

  def initialize(token)
    @rest_api_base_url = ENV.fetch('CI_API_V4_URL', 'https://gitlab.com/api/v4')
    @gitlab_api_token = token
  end

  def create_event_issue(user_id, title, description)
    return {} if Rails.env.development?

    endpoint = "projects/#{EVENTS_PROJECT_ID}/issues"
    body = { assignee_id: user_id, title:, description: }

    response = post(endpoint, body)
    return response.parsed_response.symbolize_keys if response.success?

    Rails.logger.error(response.body)
    raise GitlabResponseFailure
  end

  def create_reward_issue(user_id, _points, title, description)
    return 0 if Rails.env.development?

    endpoint = "projects/#{REWARDS_PROJECT_ID}/issues"
    body = {
      assignee_id: user_id,
      title:,
      description:
    }

    response = post(endpoint, body)
    return response.parsed_response['iid'] if response.success?

    Rails.logger.error(response.body)
    raise GitlabResponseFailure
  end

  def start_thread(project_id, iid, type, message)
    endpoint = "projects/#{project_id}/#{type}/#{iid}/discussions"

    post(
      endpoint,
      {
        body: "#{message}\n\nPosted via [contributors.gitlab.com](https://contributors.gitlab.com)."
      }
    )
  end

  def bot_request(what, project_id, merge_request_iid)
    endpoint = "projects/#{project_id}/merge_requests/#{merge_request_iid}/notes"

    post(
      endpoint,
      {
        body: "@gitlab-bot #{what}\n\nPosted via [contributors.gitlab.com](https://contributors.gitlab.com)."
      }
    )
  end

  def get_merge_request_web_url(project_id, merge_request_iid)
    endpoint = "projects/#{project_id}/merge_requests/#{merge_request_iid}"

    response = get(endpoint)
    JSON.parse(response.body)['web_url']
  end

  def get(endpoint)
    response = HTTParty.get(
      "#{@rest_api_base_url}/#{endpoint}",
      headers: {
        'Content-Type' => 'application/json',
        'User-Agent' => 'contributorsDot',
        'Authorization' => "Bearer #{@gitlab_api_token}"
      }
    )

    log_if_error(response)

    response
  end

  def post(endpoint, body)
    response = HTTParty.post(
      "#{@rest_api_base_url}/#{endpoint}",
      body: body.to_json,
      headers: {
        'Content-Type' => 'application/json',
        'User-Agent' => 'contributorsDot',
        'Authorization' => "Bearer #{@gitlab_api_token}"
      }
    )

    log_if_error(response)

    response
  end

  private

  def log_if_error(response)
    return if response.success?

    Rails.logger.warn("Received an unsuccessful response #{response.code}: #{response.body}")
  end
end
