# frozen_string_literal: true

class PointsService
  # If you are update this hash then you need to update the same name const in
  # app/javascript/Constants.js
  LEVEL_POINTS = {
    4 => 7_500,
    3 => 2_500,
    2 => 500,
    1 => 25
  }.freeze

  def self.contributor_level(points)
    return 4 if points >= LEVEL_POINTS[4]
    return 3 if points >= LEVEL_POINTS[3]
    return 2 if points >= LEVEL_POINTS[2]
    return 1 if points >= LEVEL_POINTS[1]

    0
  end

  def initialize(from_date:, to_date:, community_only:, users_scope: nil)
    @from_date = from_date
    @to_date = to_date
    @community_only = community_only
    @users_scope = users_scope
  end

  def fetch_data(save: false)
    Views::ActivitySummary.refresh if save

    activities = activity_view.all
    activities = activities.wider_community if @community_only
    activities = activities.where(user_id: @users_scope) if @users_scope
    activities = activities.between(@from_date, @to_date)
    activities = activities.user_counts

    update_users(activities) if save
    activities
  end

  def activity_view
    if @from_date&.>= 90.days.ago.to_date
      Views::RecentActivitySummary
    elsif @to_date&.< Time.zone.today
      Views::ActivitySummary
    else
      Views::CombinedActivitySummary
    end
  end

  def update_users(data)
    records = data.map do |user|
      points = user[:score]
      {
        id: user[:user_id],
        username: user[:username],
        points:
      }
    end
    User.upsert_all(
      records,
      update_only: :points
    )
  end
end
