# frozen_string_literal: true

class UserUpdateService
  def initialize(page_size: 10)
    @graphql_service = GitlabGraphqlService
    @page_size = page_size
  end

  def execute
    all_users = User.where(updated_at: ...24.hours.ago).order(:id)
    all_users.each_slice(1_500).with_index do |users, batch_index|
      users_to_update = []

      users.each_slice(@page_size).each do |batch|
        query = users_query(batch.pluck(:id))
        result = @graphql_service.execute(query)
        raise(StandardError, result['errors'].inspect) if result['errors']

        process_user_data(result['data'], users, users_to_update)
      end

      update_users(users_to_update, batch_index)
    end
  end

  def process_user_data(data, users, users_to_update)
    data.each do |key, gitlab_user|
      id = key.split('user').last.to_i
      db_user = users.find { |u| u.id == id }

      if should_delete_user?(gitlab_user)
        delete_user(db_user)
      else
        conditionally_update_user(users_to_update, db_user, gitlab_user)
      end
    end
  end

  def conditionally_update_user(users_to_update, db_user, gitlab_user)
    gitlab_user['contributor_level'] = calculate_contributor_level(db_user, gitlab_user)
    gitlab_user['discord_id'] = gitlab_user.delete('discord')
    check = %w[username discord_id contributor_level organization]
    return unless user_needs_updating?(db_user, gitlab_user, check:)

    users_to_update << {
      id: db_user.id,
      username: gitlab_user['username'],
      contributor_level: gitlab_user['contributor_level'],
      community_member: db_user.user_community_member,
      discord_id: gitlab_user['discord_id'],
      organization: gitlab_user['organization']
    }
  end

  def user_needs_updating?(db_user, gitlab_user, check:)
    check.any? { |attr_name| gitlab_user[attr_name].presence != db_user.attributes[attr_name].presence }
  end

  def calculate_contributor_level(user, value)
    return if value.nil?

    user_achievements = value&.dig('userAchievements', 'nodes')
    contributor_level = achievement_contributor_level(user_achievements)

    [user.contributor_level, contributor_level].max
  end

  def should_delete_user?(value)
    value.nil? || value['state'] == 'blocked'
  end

  def delete_user(user)
    Rails.logger.info { "Deleting user #{user.id}: #{user.username}" }
    user.destroy
  end

  def update_users(users, batch_index)
    Rails.logger.info { "Processed #{(batch_index + 1) * 1_500}" }
    User.upsert_all(users, update_only: %i[username organization contributor_level discord_id])
    Rails.logger.info { "Updated #{users.pluck(:username)}" }
  end

  def achievement_contributor_level(user_achievements)
    return 4 if find_achievement(user_achievements, 62)
    return 3 if find_achievement(user_achievements, 61)
    return 2 if find_achievement(user_achievements, 60)
    return 1 if find_achievement(user_achievements, 59)

    0
  end

  def find_achievement(user_achievements, id)
    achievement_id = "gid://gitlab/Achievements::Achievement/#{id}"
    user_achievements.any? { |user_achievement| user_achievement.dig('achievement', 'id') == achievement_id }
  end

  def users_query(ids)
    inner_query = ids.map { |id| user_fragment(id) }.join("\n")

    <<~GRAPHQL
      query {
        #{inner_query}
      }
    GRAPHQL
  end

  def user_fragment(id)
    <<~GRAPHQL
      user#{id}:user(id: "gid://gitlab/User/#{id}") {
        id
        username
        state
        discord
        organization
        userAchievements {
          nodes {
            achievement {
              id
            }
          }
        }
      }
    GRAPHQL
  end
end
