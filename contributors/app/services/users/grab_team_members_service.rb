# frozen_string_literal: true

require 'httparty'

module Users
  class GrabTeamMembersService
    ROULETTE_JSON = 'https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json'

    def execute
      ActiveRecord::Base.transaction do
        TeamMember.delete_all
        TeamMember.insert_all(team_members)
      end
    end

    def team_members
      roulette_data.map do |team_member|
        {
          username: team_member['username'],
          mr_coach: team_member['merge_request_coach'],
          available: team_member['available'],
          departments: team_member['departments'],
          projects: team_member['projects']
        }
      end
    end

    def roulette_data
      json = HTTParty.get(ROULETTE_JSON, format: :plain)
      JSON.parse(json)
    end
  end
end
