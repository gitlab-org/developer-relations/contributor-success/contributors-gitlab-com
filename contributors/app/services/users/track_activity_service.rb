# frozen_string_literal: true

module Users
  class TrackActivityService
    def self.track_first_login!(id)
      user = User.find_by(id:)
      return if user.nil? # User is not loaded into platform yet

      profile = UserProfile.find_or_create_by!(user:)
      return if profile.platform_first_login_at.present?

      profile.update_column(:platform_first_login_at, Time.zone.now)
    end

    def initialize(session, cookies)
      @session = session
      @cookies = cookies
    end

    def execute
      cookies[:contributor_platform_user] = signed_in?
      return if !signed_in? || active_less_than_hour_ago?

      track_activity_in_profile!
      session[:last_activity_at] = Time.zone.now.to_i
    end

    private

    attr_reader :session, :cookies

    def active_less_than_hour_ago?
      !session[:last_activity_at].nil? &&
        session[:last_activity_at] > 1.hour.ago.to_i
    end

    def signed_in?
      session[:user_id].present?
    end

    def track_activity_in_profile!
      user = User.find_by(id: session[:user_id])
      return if user.nil? # User is not loaded into platform yet

      profile = UserProfile.find_or_create_by!(user:)
      profile.update_column(:platform_last_activity_at, Time.zone.now)
      profile.update_column(:platform_first_login_at, Time.zone.now) if profile.platform_first_login_at.nil?
    end
  end
end
