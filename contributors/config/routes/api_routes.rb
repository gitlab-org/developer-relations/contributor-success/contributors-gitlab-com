# frozen_string_literal: true

namespace :api do
  namespace :v1 do
    # We can't use out of the box rails routing here as names with special characters (./) break
    # See https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/merge_requests/420
    get 'organizations', to: 'organizations#show', constraints: ->(request) { request.params[:id].present? }
    resources :organizations, only: :index

    resources :manage_merge_request, only: :index do
      collection do
        post 'request_review'
      end
    end

    get 'leaderboard', to: 'leaderboard#index'

    resources :users, only: %i[edit update] do
      member do
        post 'add_points'
      end
    end
    # We can't use out of the box rails routing here as names with special characters (./) break
    # See https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/merge_requests/420
    get 'users/:id', to: 'users#show', constraints: { id: /.+/ }

    resources :rewards, only: :index do
      collection do
        post 'issue'
        post 'bulk_issue'
      end
    end

    resources :events, only: :create
  end
end
