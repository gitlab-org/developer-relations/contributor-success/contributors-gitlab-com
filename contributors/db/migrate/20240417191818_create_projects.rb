# frozen_string_literal: true

class CreateProjects < ActiveRecord::Migration[7.1]
  def change
    create_table :projects, id: false do |t|
      t.integer :id, null: false # rubocop:disable Rails/DangerousColumnNames
      t.string :group_path
      t.string :name

      t.timestamps
    end

    reversible do |migration|
      migration.up { execute 'ALTER TABLE projects ADD PRIMARY KEY (id);' }
    end
  end
end
