# frozen_string_literal: true

class CreateIssues < ActiveRecord::Migration[7.1]
  def change
    create_table :issues, id: false do |t|
      t.integer :id, null: false # rubocop:disable Rails/DangerousColumnNames
      t.references :user, null: false, foreign_key: true
      t.references :project, foreign_key: true
      t.string :title, null: false
      t.integer :state_id, limit: 2, null: false
      t.timestamp :opened_date, null: false
      t.timestamp :closed_date
      t.string :web_url, null: false
      t.integer :upvotes, limit: 2, null: false

      t.timestamps
    end

    reversible do |migration|
      migration.up { execute 'ALTER TABLE issues ADD PRIMARY KEY (id);' }
    end
  end
end
