# frozen_string_literal: true

class CreateNotes < ActiveRecord::Migration[7.1]
  def change
    create_table :notes, id: false do |t|
      t.integer :id, null: false # rubocop:disable Rails/DangerousColumnNames
      t.references :user, null: false, foreign_key: true
      t.references :merge_request, foreign_key: true
      t.references :issue, foreign_key: true
      t.timestamp :added_date, null: false

      t.timestamps
    end

    reversible do |migration|
      migration.up { execute 'ALTER TABLE notes ADD PRIMARY KEY (id);' }
    end
  end
end
