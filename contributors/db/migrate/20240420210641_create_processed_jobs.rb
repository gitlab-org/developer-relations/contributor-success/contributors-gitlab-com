# frozen_string_literal: true

class CreateProcessedJobs < ActiveRecord::Migration[7.1]
  def change
    create_table :processed_jobs do |t|
      t.string :group, null: false
      t.string :issuable_type, null: false
      t.integer :processed_records, null: false
      t.integer :elapsed, null: false
      t.date :activity_date, null: false

      t.timestamps
    end

    reversible do |migration|
      migration.up do
        add_index :processed_jobs, %i[group issuable_type id], order: { id: :desc }
      end
    end
  end
end
