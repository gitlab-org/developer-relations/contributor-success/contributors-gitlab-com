# frozen_string_literal: true

class CreateUserProfiles < ActiveRecord::Migration[7.2]
  def change
    create_table :user_profiles, id: false do |t|
      t.references :user, null: false, primary_key: true
      t.boolean :community_member_override

      t.timestamps
    end
  end
end
