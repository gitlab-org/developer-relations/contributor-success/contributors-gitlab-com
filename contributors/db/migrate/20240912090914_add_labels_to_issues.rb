# frozen_string_literal: true

class AddLabelsToIssues < ActiveRecord::Migration[7.2]
  def change
    add_column :issues, :labels, :jsonb
  end
end
