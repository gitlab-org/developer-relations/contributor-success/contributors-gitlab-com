# frozen_string_literal: true

class UpdateUserForeignKeyBehaviour < ActiveRecord::Migration[7.2]
  def change
    remove_foreign_key :notes, :users
    add_foreign_key :notes, :users, on_delete: :cascade

    remove_foreign_key :issues, :users
    add_foreign_key :issues, :users, on_delete: :cascade

    remove_foreign_key :merge_requests, :users
    add_foreign_key :merge_requests, :users, on_delete: :cascade

    remove_foreign_key :commits, :users
    add_foreign_key :commits, :users, on_delete: :cascade
  end
end
