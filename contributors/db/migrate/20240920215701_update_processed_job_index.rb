# frozen_string_literal: true

class UpdateProcessedJobIndex < ActiveRecord::Migration[7.2]
  def change
    remove_index :processed_jobs, %w[group issuable_type id], order: { id: :desc }

    add_index :processed_jobs, %w[group issuable_type activity_date], order: { activity_date: :desc }
  end
end
