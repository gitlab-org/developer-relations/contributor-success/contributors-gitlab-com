# frozen_string_literal: true

class CreateBonusPoints < ActiveRecord::Migration[7.2]
  def change
    create_table :bonus_points do |t|
      t.integer :user_id, null: false
      t.integer :awarded_by_user_id, null: false
      t.integer :points, null: false

      t.timestamps

      t.string :reason, null: false
    end

    add_index :bonus_points, :user_id
    add_index :bonus_points, :awarded_by_user_id
  end
end
