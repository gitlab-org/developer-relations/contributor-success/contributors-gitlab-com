# frozen_string_literal: true

class AddActivityTypeToBonusPoints < ActiveRecord::Migration[7.2]
  def change
    add_column :bonus_points, :activity_type, :smallint, default: 0, null: false

    add_index :bonus_points, :activity_type
  end
end
