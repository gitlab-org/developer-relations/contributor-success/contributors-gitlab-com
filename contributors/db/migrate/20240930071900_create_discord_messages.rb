# frozen_string_literal: true

class CreateDiscordMessages < ActiveRecord::Migration[7.2]
  def change
    create_table :discord_messages, id: false do |t|
      t.integer :user_id, null: false
      t.boolean :reply, null: false, default: false
      t.timestamp :added_date, null: false
      t.timestamps

      t.string :id, null: false # rubocop:disable Rails/DangerousColumnNames
      t.string :channel_id, null: false
    end

    reversible do |migration|
      migration.up do
        execute 'ALTER TABLE discord_messages ADD PRIMARY KEY (id);'

        add_index :discord_messages, :user_id
        add_index :discord_messages, :added_date
      end
    end
  end
end
