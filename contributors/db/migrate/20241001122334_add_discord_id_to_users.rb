# frozen_string_literal: true

class AddDiscordIdToUsers < ActiveRecord::Migration[7.2]
  def change
    add_column :users, :discord_id, :string

    add_index :users, :discord_id, unique: true
  end
end
