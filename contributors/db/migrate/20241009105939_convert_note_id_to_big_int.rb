# frozen_string_literal: true

class ConvertNoteIdToBigInt < ActiveRecord::Migration[7.2]
  def up
    change_column :notes, :id, :bigint
  end

  def down
    change_column :notes, :id, :integer
  end
end
