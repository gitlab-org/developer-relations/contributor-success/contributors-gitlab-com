# frozen_string_literal: true

class AddReplyToForumPosts < ActiveRecord::Migration[7.2]
  def change
    add_column :forum_posts, :reply, :boolean, default: false, null: false
  end
end
