# frozen_string_literal: true

class AddOrganizationToUsers < ActiveRecord::Migration[7.2]
  def change
    add_column :users, :organization, :string
  end
end
