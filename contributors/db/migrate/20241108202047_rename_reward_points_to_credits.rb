# frozen_string_literal: true

class RenameRewardPointsToCredits < ActiveRecord::Migration[7.2]
  def change
    rename_column :rewards, :points, :credits
  end
end
