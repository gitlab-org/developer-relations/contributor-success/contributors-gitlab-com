# frozen_string_literal: true

class AddActivityTimestampsToUserProfiles < ActiveRecord::Migration[8.0]
  def change
    change_table :user_profiles, bulk: true do |t|
      t.timestamp :platform_first_login_at
      t.timestamp :platform_last_activity_at
    end
  end
end
