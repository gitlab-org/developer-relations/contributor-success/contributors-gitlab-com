# frozen_string_literal: true

class UpdateViewActivitySummariesToVersion2 < ActiveRecord::Migration[8.0]
  def change
    update_view :view_activity_summaries, version: 2, revert_to_version: 1, materialized: true
  end
end
