# frozen_string_literal: true

class CreateNewMaterializedView < ActiveRecord::Migration[8.0]
  def up
    create_view :view_recent_activity_summaries, materialized: true

    add_index :view_recent_activity_summaries, %i[community_member activity_date]
    add_index :view_recent_activity_summaries, %i[activity_date user_id], unique: true
  end

  def down
    drop_view :view_recent_activity_summaries, materialized: true
  end
end
