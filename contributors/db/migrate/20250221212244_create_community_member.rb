# frozen_string_literal: true

class CreateCommunityMember < ActiveRecord::Migration[8.0]
  def up
    create_table :community_members, id: false do |t|
      t.bigint :user_id, null: false

      t.timestamps
    end

    execute 'ALTER TABLE community_members ADD PRIMARY KEY (user_id)'
  end

  def down
    drop_table :community_members
  end
end
