# frozen_string_literal: true

class CreateTeamMember < ActiveRecord::Migration[8.0]
  def change
    create_table :team_members, id: false do |t|
      t.string :username, null: false, primary_key: true
      t.boolean :mr_coach, null: false, default: false, index: true
      t.boolean :available, null: false, default: true
      t.jsonb :departments, null: false, default: []
      t.jsonb :projects, null: false, default: {}

      t.timestamps
    end
  end
end
