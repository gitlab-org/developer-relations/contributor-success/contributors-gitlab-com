WITH
    merge_requests_agg AS (
        SELECT
            user_id,
            DATE (opened_date) AS activity_date,
            COUNT(1) AS opened_mrs
        FROM
            merge_requests
        GROUP BY
            user_id,
            activity_date
    ),
    merged_merge_requests_agg AS (
        SELECT
            user_id,
            DATE (merged_date) AS activity_date,
            COUNT(1) AS merged_mrs,
            COUNT(
                    CASE
                        WHEN labels @> '["linked-issue"]' THEN 1
                        END
            ) AS merged_mrs_with_linked_issues
        FROM
            merge_requests
        WHERE
            state_id = 3
        GROUP BY
            user_id,
            activity_date
    ),
    issues_agg AS (
        SELECT
            user_id,
            DATE (opened_date) AS activity_date,
            COUNT(1) AS opened_issues
        FROM
            issues
        GROUP BY
            user_id,
            activity_date
    ),
    commits_agg AS (
        SELECT
            commits.user_id AS user_id,
            DATE (merged_date) AS activity_date,
            COUNT(DISTINCT merge_request_id) AS merged_commits
        FROM
            merge_requests
                JOIN commits ON commits.merge_request_id = merge_requests.id
        WHERE
            state_id = 3
        GROUP BY
            commits.user_id,
            activity_date
    ),
    notes_agg AS (
        SELECT
            user_id,
            DATE (added_date) AS activity_date,
            COUNT(1) AS added_notes
        FROM
            notes
        GROUP BY
            user_id,
            activity_date
    ),
    bonus_points_agg AS (
        SELECT
            user_id,
            DATE (created_at) AS activity_date,
            SUM(points) AS bonus_points
        FROM
            bonus_points
        GROUP BY
            user_id,
            activity_date
    ),
    discord_messages_agg AS (
        SELECT
            user_id,
            DATE (added_date) AS activity_date,
            SUM(CASE WHEN reply = false THEN 1 ELSE 0 END) as discord_messages,
            SUM(CASE WHEN reply = true THEN 1 ELSE 0 END) as discord_replies
        FROM
            discord_messages
        GROUP BY
            user_id,
            activity_date
    ),
    forum_posts_agg AS (
        SELECT
            user_id,
            DATE (added_date) AS activity_date,
            SUM(CASE WHEN reply = false THEN 1 ELSE 0 END) as forum_posts,
            SUM(CASE WHEN reply = true THEN 1 ELSE 0 END) as forum_replies
        FROM
            forum_posts
        GROUP BY
            user_id,
            activity_date
    ),
    activity_dates AS (
        SELECT
            user_id,
            activity_date
        FROM
            merge_requests_agg
        UNION
        SELECT
            user_id,
            activity_date
        FROM
            merged_merge_requests_agg
        UNION
        SELECT
            user_id,
            activity_date
        FROM
            issues_agg
        UNION
        SELECT
            user_id,
            activity_date
        FROM
            commits_agg
        UNION
        SELECT
            user_id,
            activity_date
        FROM
            notes_agg
        UNION
        SELECT
            user_id,
            activity_date
        FROM
            bonus_points_agg
        UNION
        SELECT
            user_id,
            activity_date
        FROM
            discord_messages_agg
        UNION
        SELECT
            user_id,
            activity_date
        FROM
            forum_posts_agg
    ),
    counts AS (
        SELECT
            u.id AS user_id,
            u.username,
            COALESCE(p.community_member_override, u.community_member) AS community_member,
            u.contributor_level,
            d.activity_date,
            COALESCE(mr.opened_mrs, 0)::integer AS opened_mrs,
            COALESCE(mmr.merged_mrs, 0)::integer AS merged_mrs,
            COALESCE(mmr.merged_mrs_with_linked_issues, 0)::integer AS merged_mrs_with_linked_issues,
            COALESCE(i.opened_issues, 0)::integer AS opened_issues,
            COALESCE(c.merged_commits, 0)::integer AS merged_commits,
            COALESCE(n.added_notes, 0)::integer AS added_notes,
            COALESCE(b.bonus_points, 0)::integer AS bonus_points,
            COALESCE(dm.discord_messages, 0)::integer AS discord_messages,
            COALESCE(dm.discord_replies, 0)::integer AS discord_replies,
            COALESCE(fp.forum_posts, 0)::integer AS forum_posts,
            COALESCE(fp.forum_replies, 0)::integer AS forum_replies
        FROM
            users u
                JOIN activity_dates d ON u.id = d.user_id
                LEFT JOIN user_profiles p ON p.user_id = u.id
                LEFT JOIN merge_requests_agg mr ON u.id = mr.user_id
                AND d.activity_date = mr.activity_date
                LEFT JOIN merged_merge_requests_agg mmr ON u.id = mmr.user_id
                AND d.activity_date = mmr.activity_date
                LEFT JOIN issues_agg i ON u.id = i.user_id
                AND d.activity_date = i.activity_date
                LEFT JOIN commits_agg c ON u.id = c.user_id
                AND d.activity_date = c.activity_date
                LEFT JOIN notes_agg n ON u.id = n.user_id
                AND d.activity_date = n.activity_date
                LEFT JOIN bonus_points_agg b ON u.id = b.user_id
                AND d.activity_date = b.activity_date
                LEFT JOIN discord_messages_agg dm ON u.id = dm.user_id
                AND d.activity_date = dm.activity_date
                LEFT JOIN forum_posts_agg fp ON u.id = fp.user_id
                AND d.activity_date = fp.activity_date
    ),
    points AS (
        SELECT
            COALESCE(MAX(CASE WHEN var = 'created_issue_points' THEN REPLACE(value, '---', '')::integer END), 5) AS created_issue_points,
            COALESCE(MAX(CASE WHEN var = 'created_mr_author_points' THEN REPLACE(value, '---', '')::integer END), 20) AS created_mr_author_points,
            COALESCE(MAX(CASE WHEN var = 'merged_mr_author_points' THEN REPLACE(value, '---', '')::integer END), 60) AS merged_mr_author_points,
            COALESCE(MAX(CASE WHEN var = 'merged_mr_committer_points' THEN REPLACE(value, '---', '')::integer END), 20) AS merged_mr_committer_points,
            COALESCE(MAX(CASE WHEN var = 'merged_mr_with_issue_points' THEN REPLACE(value, '---', '')::integer END), 30) AS merged_mr_with_issue_points,
            COALESCE(MAX(CASE WHEN var = 'note_points' THEN REPLACE(value, '---', '')::integer END), 1) AS note_points,
            COALESCE(MAX(CASE WHEN var = 'discord_message_points' THEN REPLACE(value, '---', '')::integer END), 1) AS discord_message_points,
            COALESCE(MAX(CASE WHEN var = 'discord_reply_points' THEN REPLACE(value, '---', '')::integer END), 2) AS discord_reply_points,
            COALESCE(MAX(CASE WHEN var = 'forum_post_points' THEN REPLACE(value, '---', '')::integer END), 1) AS forum_post_points,
            COALESCE(MAX(CASE WHEN var = 'forum_reply_points' THEN REPLACE(value, '---', '')::integer END), 2) AS forum_reply_points
        FROM
            settings
        WHERE
            var like '%_points'
    )
SELECT
    *,
    (
        (opened_issues * created_issue_points) +
        (opened_mrs * created_mr_author_points) +
        (merged_mrs * merged_mr_author_points) +
        (merged_mrs_with_linked_issues * merged_mr_with_issue_points) +
        (merged_commits * merged_mr_committer_points) +
        (added_notes * note_points) +
        bonus_points +
        (discord_messages * discord_message_points) +
        (discord_replies * discord_reply_points) +
        (forum_posts * forum_post_points) +
        (forum_replies * forum_reply_points)
        )::integer AS points
FROM
    counts
        CROSS JOIN points;
