# frozen_string_literal: true

namespace :credits do
  desc 'Check for new contributions and award credits'
  task award_for_first_contribution: :environment do
    Credits::FirstContributionService.new.execute
  end

  desc 'Check for contributors who have levelled up and award credits'
  task award_for_level_up: :environment do
    Credits::LevelUpService.new.execute
  end
end
