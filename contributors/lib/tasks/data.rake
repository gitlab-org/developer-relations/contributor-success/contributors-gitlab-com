# frozen_string_literal: true

namespace :data do
  desc 'Show data counts'
  task counts: :environment do
    puts "Commits: #{Commit.count}"
    puts "Issues: #{Issue.count}"
    puts "Merge requests: #{MergeRequest.count}"
    puts "Notes: #{Note.count}"
    puts "Processed jobs: #{ProcessedJob.count}"
    puts "Projects: #{Project.count}"
    puts "Users: #{User.count}"
  end

  desc 'Grab team members'
  task grab_team_members: :environment do
    start = Time.current
    Users::GrabTeamMembersService.new.execute
    puts "Grabbed #{TeamMember.count} team members in #{Time.current - start} seconds"
  end

  desc 'Grab data'
  task :grab, [:group] => :environment do |_t, args|
    service = GrabDataService.new
    if args.group.nil?
      service.execute
    else
      service.process_group(args.group)
    end
  end

  desc 'Grab @gitlab-community/community-members'
  task grab_community_members: :environment do
    start = Time.current
    Users::GrabCommunityMembersService.new.execute
    puts "Grabbed #{CommunityMember.count} community members in #{Time.current - start} seconds"
  end

  desc 'Refresh ActivitySummary'
  task refresh: :environment do
    start = Time.current
    Views::ActivitySummary.refresh
    puts "Refreshed view in #{Time.current - start} seconds"
  end

  desc 'Grab Discord data'
  task :discord, [:weeks] => :environment do |_t, args|
    weeks = args.weeks.nil? ? 13 : args.weeks.to_i
    start = Time.current
    DiscordMessage::CHANNELS.each_key do |channel_id|
      DiscordService.new.fetch_all_messages(channel_id, weeks)
    end

    puts "Grabbed Discord data in #{Time.current - start} seconds"
  end

  desc 'Grab forum data'
  task :forum, %i[start_date end_date] => :environment do |_t, args|
    start_date = args.start_date || Date.yesterday.iso8601
    end_date = args.end_date || Date.tomorrow.iso8601

    puts "Fetching forum posts from #{start_date} to #{end_date}"
    service = DiscourseService.new
    service.fetch_all_posts(start_date, end_date)
  end

  desc 'Update points'
  task update_points: :environment do
    start = Time.current
    PointsService.new(from_date: nil, to_date: nil, community_only: false).fetch_data(save: true)
    puts "Updated points in #{Time.current - start} seconds"
  end

  desc 'Update users'
  task update_users: :environment do
    start = Time.current
    UserUpdateService.new.execute
    puts "Updated users in #{Time.current - start} seconds"
  end

  desc 'Clear data'
  task clear: :environment do
    puts 'Clearing data...'
    start = Time.current

    DiscordMessage.delete_all
    ForumPost.delete_all
    Reward.delete_all
    Note.delete_all
    Commit.delete_all
    Issue.delete_all
    MergeRequest.delete_all
    ProcessedJob.delete_all
    Project.delete_all
    UserProfile.delete_all
    User.delete_all

    Views::ActivitySummary.refresh(concurrently: false)
    puts "Cleared data in #{Time.current - start} seconds"
  end

  desc 'Seed data'
  task :seed, [:quantity] => :environment do |_t, args|
    puts 'Please be patient, this may take a while...'
    start = Time.current

    quantity = args.quantity.nil? ? 50 : args.quantity.to_i

    puts "Seeding #{quantity} users/merge_requests/issues"
    FactoryBot.create_list(:user, quantity)
    user_ids = User.pluck(:id)
    quantity.times do
      6.times { FactoryBot.create(:merge_request, :merged, user_id: user_ids.sample) }
      2.times { FactoryBot.create(:merge_request, :closed, user_id: user_ids.sample) }
      2.times { FactoryBot.create(:merge_request, :opened, user_id: user_ids.sample) }
      4.times { FactoryBot.create(:issue, :closed, user_id: user_ids.sample) }
      6.times { FactoryBot.create(:issue, :opened, user_id: user_ids.sample) }
    end

    puts "Seeding #{quantity} notes/commits"
    issue_ids = Issue.pluck(:id)
    merge_request_ids = MergeRequest.pluck(:id)
    quantity.times do
      50.times { FactoryBot.create(:note, merge_request_id: nil, issue_id: issue_ids.sample, user_id: user_ids.sample) }
      50.times do
        FactoryBot.create(:note, issue_id: nil, merge_request_id: merge_request_ids.sample, user_id: user_ids.sample)
      end
      10.times { FactoryBot.create(:commit, merge_request_id: merge_request_ids.sample, user_id: user_ids.sample) }
    end

    puts "Seeding #{quantity} rewards, Discord messages, and forum posts"
    quantity.times do
      FactoryBot.create(:reward, awarded_by_user_id: user_ids.sample, user_id: user_ids.sample)
      FactoryBot.create(:discord_message, user_id: user_ids.sample)
      FactoryBot.create(:forum_post, user_id: user_ids.sample)
    end

    Views::ActivitySummary.refresh(concurrently: false)
    Views::RecentActivitySummary.refresh(concurrently: false)

    users = Views::ActivitySummary.user_counts
    records = users.map do |user|
      {
        id: user.user_id,
        username: user.username,
        contributor_level: PointsService.contributor_level(user.score)
      }
    end
    User.upsert_all(
      records,
      update_only: :contributor_level
    )

    Views::ActivitySummary.refresh
    Views::RecentActivitySummary.refresh

    puts "Seeded data in #{Time.current - start} seconds"
  end
end
