# Previous GitLab Heroes

The GitLab Heroes are members of the wider GitLab community who made outstanding
contributions to GitLab and our community around the globe.

As we transition to our new unified GitLab Contributors Program that recognizes
both code and non-code contributions, we have retired the separate GitLab Heroes
program.

We continue to honor past GitLab Heroes for their valuable contributions and
support of the GitLab mission.

| Photo | Name | GitLab Handle | Twitter |
| ----- | ---- | ------------- | ------- |
| ![Abubakar Sadiq Hassan](images/heroes/abubakarhassan.png) | [Abubakar Sadiq Hassan](/users/itssadon) | [itssadon](https://gitlab.com/itssadon) | [@itssadon](https://x.com/itssadon) |
| ![Akanksha Bhasin](images/heroes/Akankshabhasin.png) | [Akanksha Bhasin](/users/Akankshabhasin) | [Akankshabhasin](https://gitlab.com/Akankshabhasin) | [@Akii_20](https://x.com/Akii_20) |
| ![Artur Frysiak](images/heroes/arturfrysiak.jpg) | [Artur Frysiak](/users/wiget) | [wiget](https://gitlab.com/wiget) | |
| ![Benjamin Leggett](images/heroes/bleggett.png) | [Benjamin Leggett](/users/bleggett) | [bleggett](https://gitlab.com/bleggett) | [@phanboy_iv](https://x.com/phanboy_iv) |
| ![Bethan Vincent](images/heroes/bethanvincent.png) | [Bethan Vincent](/users/bethan_vincent) | [bethan_vincent](https://gitlab.com/bethan_vincent) | [@BethanVincent](https://x.com/BethanVincent) |
| ![Cory Zibell](images/heroes/coryzibell.jpg) | [Cory Zibell](/users/coryzibell) | [coryzibell](https://gitlab.com/coryzibell) | [@coryzibell](https://x.com/coryzibell) |
| ![Drew Malone](images/heroes/drewmalone.png) | [Drew Malone](/users/drawsmcgraw) | [drawsmcgraw](https://gitlab.com/drawsmcgraw) | [@thatdrewmalone](https://x.com/thatdrewmalone) |
| ![Elad Leev](images/heroes/elad_leev.png) | [Elad Leev](/users/eladleev) | [eladleev](https://gitlab.com/eladleev) | [@eladleev](https://x.com/eladleev) |
| ![Frank Ford](images/heroes/fford.jpg) | [Frank Ford](/users/fford) | [fford](https://gitlab.com/fford) | [@ArchonVT](https://x.com/ArchonVT) |
| ![Gaga Pan](images/heroes/gagapan.jpg) | [Gaga Pan](/users/gaga5lala1013) | [gaga5lala1013](https://gitlab.com/gaga5lala1013) | |
| ![Gilang Gumilar](images/heroes/gilanggumilar.png) | [Gilang Gumilar](/users/gilangmlr) | [gilangmlr](https://gitlab.com/gilangmlr) | [@gilang_gumilar](https://x.com/gilang_gumilar) |
| ![Huss EL-Sheikh](images/heroes/husselsheikh.jpg) | [Huss EL-Sheikh](/users/HUSSTECH) | [HUSSTECH](https://gitlab.com/HUSSTECH) | [@HUSSTECH](https://x.com/HUSSTECH) |
| ![Julien Millau](images/heroes/julienmillau.png) | [Julien Millau](/users/devnied) | [devnied](https://gitlab.com/devnied) | [@devnied](https://x.com/devnied) |
| ![Karan Verma](images/heroes/karanverma.jpg) | [Karan Verma](/users/karanverma) | [karanverma](https://gitlab.com/karanverma) | [@karanvrm19](https://x.com/karanvrm19) |
| ![Katrin Leinweber](images/heroes/katrinleinweber.png) | [Katrin Leinweber](/users/katrinleinweber) | [katrinleinweber](https://gitlab.com/katrinleinweber) | |
| ![Kenneth Ijama](images/heroes/kennethijama.jpg) | [Kenneth Ijama](/users/webbravo) | [webbravo](https://gitlab.com/webbravo) | [@teamwebbravo](https://x.com/teamwebbravo) |
| ![Kyle Persohn](images/heroes/kylepersohn.jpg) | [Kyle Persohn](/users/kpersohn) | [kpersohn](https://gitlab.com/kpersohn) | [@kylepersohn](https://x.com/kylepersohn) |
| ![Mario Kleinsasser](images/heroes/mariokleinsasser.jpg) | [Mario Kleinsasser](/users/m4r10k) | [m4r10k](https://gitlab.com/m4r10k) | [@m4r10k](https://x.com/m4r10k) |
| ![Mark Downey](images/heroes/markdowney.png) | [Mark Downey](/users/markdowney) | [markdowney](https://gitlab.com/markdowney) | [@markdaviddowney](https://x.com/markdaviddowney) |
| ![Mateusz Nowotyński](images/heroes/mateusznowotynski.jpg) | [Mateusz Nowotyński](/users/maxmati) | [maxmati](https://gitlab.com/maxmati) | |
| ![Matteo Codogno](images/heroes/matteocodogno.jpg) | [Matteo Codogno](/users/papasmurf17) | [papasmurf17](https://gitlab.com/papasmurf17) | [@teo1690](https://x.com/teo1690) |
| ![Micheal Iyanda](images/heroes/mike.jpg) | [Micheal Iyanda](/users/miyanda2) | [miyanda2](https://gitlab.com/miyanda2) | [@miyanda000](https://x.com/miyanda000) |
| ![Mike Nick](images/heroes/mikenick.jpg) | [Mike Nick](/users/michaelnick) | [michaelnick](https://gitlab.com/michaelnick) | |
| ![Rajendra Kadam](images/heroes/rajendrakadam.jpg) | [Rajendra Kadam](/users/raju249) | [raju249](https://gitlab.com/raju249) | [@rajendra_ak](https://x.com/rajendra_ak) |
| ![Roland Heusser](images/heroes/rolandheusser.png) | [Roland Heusser](/users/skofgar) | [skofgar](https://gitlab.com/skofgar) | [@skofgar](https://x.com/skofgar) |
| ![Simone D'Avico](images/heroes/simonedavico.jpg) | [Simone D'Avico](/users/simonedavico) | [simonedavico](https://gitlab.com/simonedavico) | [@simonedavico](https://x.com/simonedavico) |
| ![Solomon Rubin](images/heroes/solomonrubin.jpg) | [Solomon Rubin](/users/serubin) | [serubin](https://gitlab.com/serubin) | [@serubin_](https://x.com/serubin_) |
| ![Tom West](images/heroes/tomawest.jpg) | [Tom West](/users/tomawest) | [tomawest](https://gitlab.com/tomawest) | [@TWest_Dev](https://x.com/TWest_Dev) |
| ![Yannick Siewe](images/heroes/YannickSiewe.png) | [Yannick Siewe](/users/yannicksiewe) | [yannicksiewe](https://gitlab.com/yannicksiewe) | [@YSiewe](https://x.com/YSiewe) |
| ![Zach Dunn](images/heroes/zachdunn.jpg) | [Zach Dunn](/users/zadunn) | [zadunn](https://gitlab.com/zadunn) | [@SillySophist](https://x.com/SillySophist) |
| ![David Elizondo](images/heroes/davidelizondo.png) | [David Elizondo](/users/daelmo) | [daelmo](https://gitlab.com/daelmo) | [@daelmo](https://x.com/daelmo) |
| ![Muhammad Hassan](images/heroes/Hassan.png) | [Muhammad Hassan](/users/Mhassanbughio) | [Mhassanbughio](https://gitlab.com/Mhassanbughio) | [@Muhamma36404716](https://x.com/Muhamma36404716) |
| ![Asharib Ahmed](images/heroes/asharib.png) | [Asharib Ahmed](/users/asharibahmed143) | [asharibahmed143](https://gitlab.com/asharibahmed143) | [@asharib90](https://x.com/asharib90) |
| ![Ben Bodenmiller](images/heroes/benbodenmiller.jpg) | [Ben Bodenmiller](/users/bbodenmiller) | [bbodenmiller](https://gitlab.com/bbodenmiller) | |
| ![Jacopo Beschi](images/heroes/jacopobeschi.jpg) | [Jacopo Beschi](/users/jacopo-beschi) | [jacopo-beschi](https://gitlab.com/jacopo-beschi) | [@jacopobeschi](https://x.com/jacopobeschi) |
| | [Takuya Noguchi](/users/tnir) | [tnir](https://gitlab.com/tnir) | [@tn961ir](https://x.com/tn961ir) |
| ![Kevin Davin](images/heroes/kevindavin.jpg) | [Kevin Davin](/users/davinkevin) | [davinkevin](https://gitlab.com/davinkevin) | [@davinkevin](https://x.com/davinkevin) |
| ![Taisuke 'Jeff' Inoue](images/heroes/taisukeinoue.png) | [Taisuke 'Jeff' Inoue](/users/jeffi7) | [jeffi7](https://gitlab.com/jeffi7) | [@jeffi7](https://x.com/jeffi7) |
| ![Logan Weber](images/heroes/loganweber.png) | [Logan Weber](/users/neonox31) | [neonox31](https://gitlab.com/neonox31) | [@neonox31](https://x.com/neonox31) |
| ![Thomas Teoh](images/heroes/thomasteoh.jpg) | [Thomas Teoh](/users/tteoh) | [tteoh](https://gitlab.com/tteoh) | [@tteoh](https://x.com/tteoh) |
| ![Adrian Moisey](images/heroes/adrianmoisey.jpg) | [Adrian Moisey](/users/adrianmoisey) | [adrianmoisey](https://gitlab.com/adrianmoisey) | [@adrianmoisey](https://x.com/adrianmoisey) |
| ![Rachid Zarouali](images/heroes/rachidzarouali.png) | [Rachid Zarouali](/users/rzarouali) | [rzarouali](https://gitlab.com/rzarouali) | [@xinity](https://x.com/xinity) |
| ![Yogi](images/heroes/yogi.png) | [Yogi](/users/yo) | [yo](https://gitlab.com/yo) | [@big1nt](https://x.com/big1nt) |
| ![Jean-Yves Gastaud](images/heroes/jygastaud.jpg) | [Jean-Yves Gastaud](/users/jygastaud1) | [jygastaud1](https://gitlab.com/jygastaud1) | [@jygastaud](https://x.com/jygastaud) |
| ![Afzal Ansari](images/heroes/afzalansari.jpeg) | [Afzal Ansari](/users/afzal442) | [afzal442](https://gitlab.com/afzal442) | [@afzalshams01](https://x.com/afzalshams01) |
| ![Philipp Westphalen](images/heroes/philippwestphalen.png) | [Philipp Westphalen](/users/Phil404) | [Phil404](https://gitlab.com/Phil404) | [@Koala_Phil](https://x.com/Koala_Phil) |
| ![Mario García](images/heroes/mariogarcia.jpg) | [Mario García](/users/mattdark) | [mattdark](https://gitlab.com/mattdark) | [@mariogmd](https://x.com/mariogmd) |
| ![Sean Corkum](images/heroes/seancorkum.jpg) | [Sean Corkum](/users/s_c) | [s_c](https://gitlab.com/s_c) | |
| ![Darwin Sanoy](images/heroes/darwinsanoy.jpg) | [Darwin Sanoy](/users/DarwinJS) | [DarwinJS](https://gitlab.com/DarwinJS) | [@DarwinTheorizes](https://x.com/DarwinTheorizes) |
| ![Manuel Grabowski](images/heroes/manu_faktur.jpg) | [Manuel Grabowski](/users/manu_faktur) | [manu_faktur](https://gitlab.com/manu_faktur) | [@manu_faktur](https://x.com/manu_faktur) |
| ![Onose Oghenevwogaga](images/heroes/onose_oghenevwogaga.jpg) | [Onose Oghenevwogaga](/users/ogasky) | [ogasky](https://gitlab.com/ogasky) | [@OGA4SKY](https://x.com/OGA4SKY) |
| ![Nico Meisenzahl](images/heroes/nicomeisenzahl.png) | [Nico Meisenzahl](/users/nico-meisenzahl) | [nico-meisenzahl](https://gitlab.com/nico-meisenzahl) | [@nmeisenzahl](https://x.com/nmeisenzahl) |
| ![Riccardo Padovani](images/heroes/rpadovani.jpg) | [Riccardo Padovani](/users/rpadovani) | [rpadovani](https://gitlab.com/rpadovani) | [@rpadovani93](https://x.com/rpadovani93) |
| ![James Tharpe](images/heroes/jamestharpe.jpg) | [James Tharpe](/users/jamestharpe) | [jamestharpe](https://gitlab.com/jamestharpe) | [@jamestharpe](https://x.com/jamestharpe) |
| ![Alex Viscreanu](images/heroes/alexviscreanu.png) | [Alex Viscreanu](/users/alexviscreanu) | [alexviscreanu](https://gitlab.com/alexviscreanu) | [@aexvir](https://x.com/aexvir) |
| ![Max Wittig](images/heroes/maxwittig.jpg) | [Max Wittig](/users/max-wittig) | [max-wittig](https://gitlab.com/max-wittig) | [@maxwittig_](https://x.com/maxwittig_) |
| ![Will Hall](images/heroes/will-hall.jpg) | [Will Hall](/users/willhallonline) | [willhallonline](https://gitlab.com/willhallonline) | [@hn_will](https://x.com/hn_will) |
| ![Rouineb Hama](images/heroes/hamzarouineb.jpg) | [Rouineb Hama](/users/devlifealways) | [devlifealways](https://gitlab.com/devlifealways) | [@ROUINEB](https://x.com/ROUINEB) |
| ![J. Manrique López de la Fuente](images/heroes/jsmanrique.jpg) | [J. Manrique López de la Fuente](/users/jsmanrique) | [jsmanrique](https://gitlab.com/jsmanrique) | [@jsmanrique](https://x.com/jsmanrique) |
| ![Nicolas Fränkel](images/heroes/frankel.jpg) | [Nicolas Fränkel](/users/nfrankel) | [nfrankel](https://gitlab.com/nfrankel) | [@nicolas_frankel](https://x.com/nicolas_frankel) |
| ![Samuel Alfageme](images/heroes/samuelalfageme.jpg) | [Samuel Alfageme](/users/alfageme) | [alfageme](https://gitlab.com/alfageme) | [@alfageme](https://x.com/alfageme) |
| ![Christophe Chaudier](images/heroes/christophechaudier.jpg) | [Christophe Chaudier](/users/cchaudier) | [cchaudier](https://gitlab.com/cchaudier) | [@c_chaudier](https://x.com/c_chaudier) |
| ![Roger Meier](images/heroes/bufferoverflow.png) | [Roger Meier](/users/bufferoverflow) | [bufferoverflow](https://gitlab.com/bufferoverflow) | [@rbufferoverflow](https://x.com/rbufferoverflow) |
| ![Fred Zind](images/heroes/fredzind.jpg) | [Fred Zind](/users/free_zed) | [free_zed](https://gitlab.com/free_zed) | |
| ![Matt Smith](images/heroes/mattsmith.jpg) | [Matt Smith](/users/harmelodic) | [harmelodic](https://gitlab.com/harmelodic) | [@harmelodic](https://x.com/harmelodic) |
| ![Fabio Huser](images/heroes/fabiohuser.jpg) | [Fabio Huser](/users/fh1ch) | [fh1ch](https://gitlab.com/fh1ch) | [@fh1ch](https://x.com/fh1ch) |
| ![Jason Mongaras](images/heroes/jasonmongaras.jpg) | [Jason Mongaras](/users/jmongaras) | [jmongaras](https://gitlab.com/jmongaras) | [@jason_mongaras](https://x.com/jason_mongaras) |
| ![Marvin Karegyeya](images/heroes/marvinkaregyeya.png) | [Marvin Karegyeya](/users/nuwe1) | [nuwe1](https://gitlab.com/nuwe1) | [@NuweMarvin](https://x.com/NuweMarvin) |
| ![Aaron Brown](images/heroes/aaronbrown.png) | [Aaron Brown](/users/aayore) | [aayore](https://gitlab.com/aayore) | [@aayore](https://x.com/aayore) |
| ![Abel Adugam Ayuba](images/heroes/abelayuba.png) | [Abel Adugam Ayuba](/users/adugamayuba) | [adugamayuba](https://gitlab.com/adugamayuba) | [@adugamayuba](https://x.com/adugamayuba) |
| ![Anthony Sayre](images/heroes/anthonysayre.jpg) | [Anthony Sayre](/users/asayre) | [asayre](https://gitlab.com/asayre) | [@sayreanthony](https://x.com/sayreanthony) |
| ![Giovanni Bucci](images/heroes/giovannibucci.jpg) | [Giovanni Bucci](/users/puskin) | [puskin](https://gitlab.com/puskin) | |
| ![Piotr Kurpik](images/heroes/piotrkurpik.jpg) | [Piotr Kurpik](/users/kurpik.piotr) | [kurpik.piotr](https://gitlab.com/kurpik.piotr) | |
| ![Vladimir Roudakov](images/heroes/profile-vladimirroudakov-400.jpg) | [Vladimir Roudakov](/users/VladimirAus) | [VladimirAus](https://gitlab.com/VladimirAus) | [@VladimirAus](https://x.com/VladimirAus) |
| ![Alejandro Mercado](images/heroes/mercadoalex.png) | [Alejandro Mercado](/users/mercadoalex) | [mercadoalex](https://gitlab.com/mercadoalex) | [@alexmarket](https://x.com/alexmarket) |
| ![Carlos Eduardo Arango](images/heroes/arangogutierrez.jpeg) | [Carlos Eduardo Arango](/users/arangogutierrez) | [arangogutierrez](https://gitlab.com/arangogutierrez) | [@CarlosEArango](https://x.com/CarlosEArango) |
| ![Cheng Wei Chen](images/heroes/chengweichen.jpg) | [Cheng Wei Chen](/users/chengwei.chen) | [chengwei.chen](https://gitlab.com/chengwei.chen) | [@Cheng_Wei_Chen](https://x.com/Cheng_Wei_Chen) |
| ![Joost Evertse](images/heroes/joostevertse.png) | [Joost Evertse](/users/joustie) | [joustie](https://gitlab.com/joustie) | [@joustie](https://x.com/joustie) |
| ![David Nicholaeff](images/heroes/davidnicholaeff.png) | [David Nicholaeff](/users/dnic) | [dnic](https://gitlab.com/dnic) | |
| ![Diego Louzán](images/heroes/dlouzan.jpg) | [Diego Louzán](/users/dlouzan) | [dlouzan](https://gitlab.com/dlouzan) | |
| ![Utkarsh Gupta](images/heroes/utkarshgupta.jpg) | [Utkarsh Gupta](/users/utkarsh2102) | [utkarsh2102](https://gitlab.com/utkarsh2102) | [@utkarsh2102](https://x.com/utkarsh2102) |
| ![Ejiroghene Adeniyi](images/heroes/eadeniyi.jpg) | [Ejiroghene Adeniyi](/users/ejis) | [ejis](https://gitlab.com/ejis) | [@AdeniyiEjiro](https://x.com/AdeniyiEjiro) |
| | [Peter Dave Hello](/users/PeterDaveHello) | [PeterDaveHello](https://gitlab.com/PeterDaveHello) | [@PeterDaveHello](https://x.com/PeterDaveHello) |
| ![Jean-Philippe Baconnais](images/heroes/jeanphi-baconnais.jpg) | [Jean-Philippe Baconnais](/users/jeanphi-baconnais) | [jeanphi-baconnais](https://gitlab.com/jeanphi-baconnais) | [@JPhi_Baconnais](https://x.com/JPhi_Baconnais) |
| ![Niclas Mietz](images/heroes/niclasmietz.png) | [Niclas Mietz](/users/solidnerd) | [solidnerd](https://gitlab.com/solidnerd) | [@solidnerd](https://x.com/solidnerd) |
| ![Irtiza Ali](images/heroes/irtiza-ali.png) | [Irtiza Ali](/users/aliartiza75) | [aliartiza75](https://gitlab.com/aliartiza75) | [@IrtizaAli75](https://x.com/IrtizaAli75) |
| ![Alejandro Medina](images/heroes/alejandromedina.jpg) | [Alejandro Medina](/users/Mellab) | [Mellab](https://gitlab.com/Mellab) | [@mentalabduction](https://x.com/mentalabduction) |
| ![Clement Sam](images/heroes/clementsam.png) | [Clement Sam](/users/profclems) | [profclems](https://gitlab.com/profclems) | [@clems_dev](https://x.com/clems_dev) |
| ![Chieh-Min Wang](images/heroes/chiehminwang.jpg) | [Chieh-Min Wang](/users/chiehmin18) | [chiehmin18](https://gitlab.com/chiehmin18) | [@fatminmin](https://x.com/fatminmin) |
| ![Jean-Baptiste Martin](images/heroes/jeanbaptistemartin.jpg) | [Jean-Baptiste Martin](/users/spanska) | [spanska](https://gitlab.com/spanska) | [@mr_martingale](https://x.com/mr_martingale) |
| ![Mouson Chen](images/heroes/mousonchen.jpg) | [Mouson Chen](/users/mouson) | [mouson](https://gitlab.com/mouson) | [@mouson](https://x.com/mouson) |
| ![Raimund Hook](images/heroes/raimundhook.jpg) | [Raimund Hook](/users/stingrayza) | [stingrayza](https://gitlab.com/stingrayza) | [@stingrayza](https://x.com/stingrayza) |
| ![Shubham Kumar](images/heroes/shubhamkumar.png) | [Shubham Kumar](/users/imskr) | [imskr](https://gitlab.com/imskr) | [@thetweetofskr](https://x.com/thetweetofskr) |
| ![Andrew Smith](images/heroes/andrewsmith.jpg) | [Andrew Smith](/users/espadav8) | [espadav8](https://gitlab.com/espadav8) | [@espadav8](https://x.com/espadav8) |
| ![Abhijeet Chatterjee](images/heroes/abhijeetchatterjee.jpg) | [Abhijeet Chatterjee](/users/abhijeet007rocks8) | [abhijeet007rocks8](https://gitlab.com/abhijeet007rocks8) | [@abhi_jeet_00](https://x.com/abhi_jeet_00) |
| ![Rishabh Gupta](images/heroes/rishabhgupta.jpg) | [Rishabh Gupta](/users/imrishabh18) | [imrishabh18](https://gitlab.com/imrishabh18) | [@im_rishabh18](https://x.com/im_rishabh18) |
| ![Shivansh Srivastava](images/heroes/shivanshsrivastava.png) | [Shivansh Srivastava](/users/shivanshsrivastava2000) | [shivanshsrivastava2000](https://gitlab.com/shivanshsrivastava2000) | [@Shivansh_2407](https://x.com/Shivansh_2407) |
| ![Niklas van Schrick](images/heroes/taucher2003.png) | [Niklas van Schrick](/users/Taucher2003) | [Taucher2003](https://gitlab.com/Taucher2003) | |
| ![Mehul Sharma](images/heroes/mehulsharma.png) | [Mehul Sharma](/users/mehulsharma) | [mehulsharma](https://gitlab.com/mehulsharma) | [@mehul_sharma27](https://x.com/mehul_sharma27) |
| ![Philip Welz](images/heroes/philipwelz.jpg) | [Philip Welz](/users/phil.xx) | [phil.xx](https://gitlab.com/phil.xx) | [@philip_welz](https://x.com/philip_welz) |
| ![Piotr Stankowski](images/heroes/piotrstankowski.jpg) | [Piotr Stankowski](/users/trakos) | [trakos](https://gitlab.com/trakos) | [@trakos](https://x.com/trakos) |
| ![Luiz Felipe Bernardo](images/heroes/luizfelipebernardo.jpeg) | [Luiz Felipe Bernardo](/users/br.bernardo) | [br.bernardo](https://gitlab.com/br.bernardo) | [@brbernardoo](https://x.com/brbernardoo) |
| ![Siddharth Asthana](images/heroes/siddharthasthana.jpg) | [Siddharth Asthana](/users/edith007) | [edith007](https://gitlab.com/edith007) | [@Asthana31](https://x.com/Asthana31) |
| ![Valentin Despa](images/heroes/valentindespa.png) | [Valentin Despa](/users/vdespa) | [vdespa](https://gitlab.com/vdespa) | [@vdespa](https://x.com/vdespa) |
| ![Mohammed Daoudi](images/heroes/mohammeddaoudi.png) | [Mohammed Daoudi](/users/Iduoad) | [Iduoad](https://gitlab.com/Iduoad) | [@Miduoad](https://x.com/Miduoad) |
| ![Adil Shehzad](images/heroes/adilshehzad.png) | [Adil Shehzad](/users/adilshehzad) | [adilshehzad](https://gitlab.com/adilshehzad) | [@adilshehzad69](https://x.com/adilshehzad69) |
| ![Marco Zille](images/heroes/marcozille.jpg) | [Marco Zille](/users/zillemarco) | [zillemarco](https://gitlab.com/zillemarco) | [@marco_zille](https://x.com/marco_zille) |
| ![Anshul Riyal](images/heroes/anshulriyal.jpg) | [Anshul Riyal](/users/anshulriyal) | [anshulriyal](https://gitlab.com/anshulriyal) | [@AnshulRiyal](https://x.com/AnshulRiyal) |
| ![Steven Pritchard](images/heroes/stevenpritchard.jpg) | [Steven Pritchard](/users/silug) | [silug](https://gitlab.com/silug) | [@silug](https://x.com/silug) |
| ![Shreedhar Bhat](images/heroes/shreedhar.jpg) | [Shreedhar Bhat](/users/shridharbhat1998) | [shridharbhat1998](https://gitlab.com/shridharbhat1998) | |
| ![Rossana Suarez](images/heroes/roxsross.png) | [Rossana Suarez](/users/roxsross) | [roxsross](https://gitlab.com/roxsross) | [@roxsross](https://x.com/roxsross) |
| ![Jonas Wälter](images/heroes/jonaswaelter.jpg) | [Jonas Wälter](/users/wwwjon) | [wwwjon](https://gitlab.com/wwwjon) | |
| ![Karthikayan](images/heroes/parkourkarthik.jpg) | [Karthikayan](/users/parkourkarthik) | [parkourkarthik](https://gitlab.com/parkourkarthik) | [@parkourkarthik](https://x.com/parkourkarthik) |
| ![Lucas Zampieri](images/heroes/lzampier.jpg) | [Lucas Zampieri](/users/lzampier) | [lzampier](https://gitlab.com/lzampier) | |
| ![Guillaume Chauvel](images/heroes/guillaumechauvel.jpg) | [Guillaume Chauvel](/users/guillaume.chauvel) | [guillaume.chauvel](https://gitlab.com/guillaume.chauvel) | |
| ![Matthieu Vincent](images/heroes/yodamad.jpg) | [Matthieu Vincent](/users/yodamad) | [yodamad](https://gitlab.com/yodamad) | [@yodamad03](https://x.com/yodamad03) |
| ![Stéphane ROBERT](images/heroes/srobert.jpg) | [Stéphane ROBERT](/users/Bob74) | [Bob74](https://gitlab.com/Bob74) | [@RobertStphane19](https://x.com/RobertStphane19) |
| ![Jeremy Neff](images/heroes/n3ff.png) | [Jeremy Neff](/users/n3ff) | [n3ff](https://gitlab.com/n3ff) | [@logginglager](https://x.com/logginglager) |
| ![Missy Davies](images/heroes/missydavies.jpg) | [Missy Davies](/users/missy-davies) | [missy-davies](https://gitlab.com/missy-davies) | [@missy_davies_](https://x.com/missy_davies_) |
| ![Rene Hernandez Remedios](images/heroes/renehernandez.jpg) | [Rene Hernandez Remedios](/users/renehernandez) | [renehernandez](https://gitlab.com/renehernandez) | [@renehernandezio](https://x.com/renehernandezio) |
| ![Di Gao](images/heroes/derekgao.jpg) | [Di Gao](/users/DerekGao) | [DerekGao](https://gitlab.com/DerekGao) | [@Derekgao8](https://x.com/Derekgao8) |
| ![Shahriyar Al Mustakim Mitul](images/heroes/shahriyaralmustakimmitul.png) | [Shahriyar Al Mustakim Mitul](/users/mitul3737) | [mitul3737](https://gitlab.com/mitul3737) | [@mitul_shahriyar](https://x.com/mitul_shahriyar) |
| |  [Clarissa R Mendes](/users/claromes) | [claromes](https://gitlab.com/claromes) | [@claromes](https://x.com/claromes) |
| ![Syed Zubeen](images/heroes/syedzubeen.jpg) | [Syed Zubeen](/users/syedzubeen) | [syedzubeen](https://gitlab.com/syedzubeen) | [@zubeensyed](https://x.com/zubeensyed) |
| ![Brett Weir](images/heroes/brettweir.jpg) | [Brett Weir](/users/brettweir) | [brettweir](https://gitlab.com/brettweir) | |
| ![Keith McDuffee](images/heroes/keithmcduffee.png) | [Keith McDuffee](/users/kmcduffee-stackref) | [kmcduffee-stackref](https://gitlab.com/kmcduffee-stackref) | [@KeithMcDuffee](https://x.com/KeithMcDuffee) |
| ![Romain Boulanger](images/heroes/romainboulanger.png) | [Romain Boulanger](/users/axinorm) | [axinorm](https://gitlab.com/axinorm) | |
| ![Justin Zeng](images/heroes/justinzeng.png) | [Justin Zeng](/users/jzeng88) | [jzeng88](https://gitlab.com/jzeng88) | [@jzeng88](https://x.com/jzeng88) |
| ![Gaurav Marwal](images/heroes/gauravmarwal.png) | [Gaurav Marwal](/users/gauravmarwal) | [gauravmarwal](https://gitlab.com/gauravmarwal) | [@marinekxng](https://x.com/marinekxng) |
| ![Martin Schurz](images/heroes/martinschurz.png) | [Martin Schurz](/users/schurzi) | [schurzi](https://gitlab.com/schurzi) | [@schurzi85](https://x.com/schurzi85) |
