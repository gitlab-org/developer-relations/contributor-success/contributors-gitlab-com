# frozen_string_literal: true

RSpec.describe ApplicationController do
  describe '.ensure_logged_in' do
    controller do
      before_action :ensure_logged_in

      def index
        render plain: 'success'
      end
    end

    context 'when the user is logged in' do
      it 'allows access to the action' do
        session[:username] = 'test_user'

        get :index

        expect(response).to have_http_status(:ok)
        expect(response.body).to eq('success')
      end
    end

    context 'when the user is not logged in' do
      it 'redirects to the login path' do
        get :index

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(login_path(return: request.fullpath))
      end
    end
  end

  describe '.gitlab_access_token' do
    let(:current_time) { Time.zone.now.to_i }
    let(:refresh_response) do
      {
        gitlab_access_token: 'new_token',
        gitlab_expires_at: current_time + 3_600,
        gitlab_refresh_token: 'new_refresh_token'
      }
    end

    before do
      session[:gitlab_refresh_token] = 'refresh_token'
      allow(Time.zone).to receive(:now).and_return(Time.zone.at(current_time))
      allow(GitlabOauthService).to receive(:refresh)
        .with('refresh_token')
        .and_return(refresh_response)
    end

    context 'when the access token is not expired' do
      before do
        session[:gitlab_access_token] = 'old_token'
        session[:gitlab_expires_at] = current_time + 1
      end

      it 'returns the access token from the session' do
        expect(controller.send(:gitlab_access_token)).to eq('old_token')
      end

      context 'when force_refres = true' do
        it 'refreshes the access token' do
          expect(controller.send(:gitlab_access_token, force_refresh: true)).to eq('new_token')
          expect(session[:gitlab_expires_at]).to eq(current_time + 3_600)
          expect(session[:gitlab_refresh_token]).to eq('new_refresh_token')
        end
      end
    end

    context 'when the access token is expired' do
      it 'refreshes the access token' do
        session[:gitlab_access_token] = 'expired_token'
        session[:gitlab_expires_at] = current_time - 1

        expect(controller.send(:gitlab_access_token)).to eq('new_token')
        expect(session[:gitlab_expires_at]).to eq(current_time + 3_600)
        expect(session[:gitlab_refresh_token]).to eq('new_refresh_token')
      end
    end
  end

  describe '.logged_in?' do
    context 'when the user is logged in' do
      it 'returns true' do
        session[:username] = 'test_user'

        expect(controller.send(:logged_in?)).to be true
      end
    end

    context 'when the user is not logged in' do
      it 'returns false' do
        expect(controller.send(:logged_in?)).to be false
      end
    end
  end
end
