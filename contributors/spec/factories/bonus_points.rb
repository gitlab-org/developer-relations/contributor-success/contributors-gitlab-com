# frozen_string_literal: true

FactoryBot.define do
  factory :bonus_points do
    awarded_by_user factory: :user
    points { rand(1..1_000) }
    reason { 'Ran an event promoting GitLab' }
    user
  end
end
