# frozen_string_literal: true

FactoryBot.define do
  factory :discord_message do
    sequence(:id) # rubocop:disable FactoryBot/IdSequence
    channel_id { DiscordMessage::CHANNELS.keys.sample }
    added_date { rand(1..100).days.ago }
    reply { [true, false].sample }
    user
  end
end
