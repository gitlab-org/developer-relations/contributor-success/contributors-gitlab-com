# frozen_string_literal: true

FactoryBot.define do
  factory :issue do
    sequence(:id) # rubocop:disable FactoryBot/IdSequence
    opened_date { rand(1..100).days.ago }
    # project
    state_id { 1 }
    sequence(:title) { |n| "Issue #{n}" }
    upvotes { 0 }
    user
    web_url { "https://gitlab.example.com/group/project/issues/#{id}" }
    labels { FactoryBotHelpers::Labels.random_labels }
  end
end
