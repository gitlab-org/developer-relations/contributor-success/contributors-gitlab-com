# frozen_string_literal: true

FactoryBot.define do
  factory :note do
    sequence(:id) # rubocop:disable FactoryBot/IdSequence
    added_date { rand(1..100).days.ago }
    user

    after(:build) do |note, evaluator|
      next if evaluator.issue_id.present? || evaluator.merge_request_id.present?

      if evaluator.id.even?
        note.issue = build(:issue)
      else
        note.merge_request = build(:merge_request)
      end
    end
  end
end
