# frozen_string_literal: true

FactoryBot.define do
  factory :processed_job do
    sequence(:group) { |n| "group-#{n}" }
    issuable_type { %w[issue merge_request].sample }
    processed_records { rand(1..1_000) }
    elapsed { rand(1..1_000) }
    activity_date { rand(1..100).days.ago }
  end
end
