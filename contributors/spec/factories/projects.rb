# frozen_string_literal: true

FactoryBot.define do
  factory :project do
    sequence(:id) # rubocop:disable FactoryBot/IdSequence
    sequence(:group_path) { |n| "group/project-#{n}" }
    sequence(:name) { |n| "Project #{n}" }
  end
end
