# frozen_string_literal: true

FactoryBot.define do
  factory :reward do
    awarded_by_user factory: :user
    reason { rand(0..3) }
    credits { rand(1..200) }
    issue_iid { rand(1..1_000) }
    gift_code { ('A'..'Z').to_a.sample(20).join }
    user
  end
end
