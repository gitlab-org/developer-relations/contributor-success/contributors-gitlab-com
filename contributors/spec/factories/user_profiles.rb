# frozen_string_literal: true

FactoryBot.define do
  factory :user_profile do
    user
    community_member_override { [true, false, nil].sample }
  end
end
