# frozen_string_literal: true

RSpec.describe GraphqlHelper do
  describe('#id_from_graphql_id') do
    it 'returns the id from the graphql_id' do
      graphql_id = 'gid://gitlab/User/1'
      expect(helper.id_from_graphql_id(graphql_id)).to eq(1)
    end
  end
end
