# frozen_string_literal: true

RSpec.describe ActivitySummaryRefreshJob do
  describe '#perform' do
    subject(:perform) { described_class.perform_now }

    it 'calls Views::ActivitySummary.refresh' do
      allow(Views::ActivitySummary).to receive(:refresh)
      allow(Views::RecentActivitySummary).to receive(:refresh)

      perform

      expect(Views::ActivitySummary).to have_received(:refresh)
      expect(Views::RecentActivitySummary).to have_received(:refresh)
    end
  end
end
