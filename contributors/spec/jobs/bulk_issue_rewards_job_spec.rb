# frozen_string_literal: true

RSpec.describe BulkIssueRewardsJob do
  let(:token) { 'test_token' }
  let(:usernames) { %w[user1 user2 user3] }
  let(:credits) { 100 }
  let(:reason) { 'Great contribution' }
  let(:note) { 'Keep up the good work' }
  let(:awarded_by_user_id) { 999 }
  let(:rewards_service) { instance_double(RewardsService) }

  before do
    allow(RewardsService).to receive(:new).with(token).and_return(rewards_service)
    allow(rewards_service).to receive(:issue)

    (1..3).each do |id|
      create(:user, id:, username: "user#{id}")
    end
  end

  describe '#perform' do
    subject(:perform) { described_class.perform_now(token, usernames, credits, reason, note, awarded_by_user_id) }

    it 'creates a RewardsService with the provided token' do
      perform

      expect(RewardsService).to have_received(:new).with(token)
    end

    it 'issues rewards to each user in the usernames array' do
      perform

      (1..3).each do |id|
        expect(rewards_service).to have_received(:issue)
          .with(
            user_id: id,
            credits:,
            reason:,
            note:,
            awarded_by_user_id:,
            username: "user#{id}"
          )
      end
    end

    context 'when usernames include the @ prefix' do
      let(:usernames) { %w[@user1 @user2 @user3] }

      it 'strips the prefix' do
        perform

        (1..3).each do |id|
          expect(rewards_service).to have_received(:issue)
            .with(
              user_id: id,
              credits:,
              reason:,
              note:,
              awarded_by_user_id:,
              username: "user#{id}"
            )
        end
      end
    end

    context 'when username case is different' do
      let(:usernames) { %w[USER2] }

      it 'matches the user regardless of case' do
        perform

        expect(rewards_service).to have_received(:issue)
          .with(
            user_id: 2,
            credits:,
            reason:,
            note:,
            awarded_by_user_id:,
            username: 'USER2'
          )
      end
    end

    context 'when a username does not exist' do
      let(:usernames) { %w[user1 nonexistent_user] }

      it 'does not raise an error' do
        expect { perform }.not_to raise_error(NoMethodError)
      end

      it 'logs error' do
        allow(Rails.logger).to receive(:error)

        perform

        expect(Rails.logger).to have_received(:error) do |&block|
          expect(block.call).to eq('Unable to find user: nonexistent_user to issue reward')
        end
      end
    end

    context 'when the usernames array is empty' do
      let(:usernames) { [] }

      it 'completes without issuing any rewards' do
        perform

        expect(rewards_service).not_to have_received(:issue)
      end
    end
  end
end
