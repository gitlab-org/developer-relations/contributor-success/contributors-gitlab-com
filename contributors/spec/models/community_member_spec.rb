# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  user_id
].freeze

RSpec.describe CommunityMember, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to belong_to(:user).required }
  end

  describe 'user association' do
    let!(:community_member) { create(:community_member) }

    it 'persists after user deletion' do
      User.destroy_by(id: community_member.user_id)

      expect { community_member.reload }.not_to raise_error
      expect(community_member.reload.user_id).not_to be_nil
    end
  end
end
