# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  added_date
  channel_id
  id
  user_id
].freeze

RSpec.describe DiscordMessage, required_fields: REQUIRED_FIELDS do
  it { is_expected.to belong_to(:user).required }

  describe 'validations' do
    it { is_expected.to allow_value([true, false]).for(:reply) }
    it { is_expected.not_to allow_value(nil).for(:reply) }
  end

  describe 'scopes' do
    describe 'added_between' do
      let!(:user) { create(:user) }
      let!(:one) { create(:discord_message, user:, added_date: 15.days.ago) }
      let!(:two) { create(:discord_message, user:, added_date: 10.days.ago) }
      let!(:three) { create(:discord_message, user:, added_date: 5.days.ago) }

      context 'with from_date' do
        it 'returns forum posts after the given dates' do
          expect(described_class.added_between(12.days.ago, nil)).to contain_exactly(two, three)
        end
      end

      context 'with to_date' do
        it 'returns forum posts before the given dates' do
          expect(described_class.added_between(nil, 7.days.ago)).to contain_exactly(one, two)
        end
      end

      context 'with to_date and from_date' do
        it 'returns forum posts between the given dates' do
          expect(described_class.added_between(12.days.ago, 7.days.ago)).to contain_exactly(two)
        end
      end
    end

    describe 'user_counts' do
      let(:users) { create_list(:user, 2) }

      before do
        create(:discord_message, user: users[0])
        create_list(:discord_message, 2, user: users[1])
      end

      it 'returns message counts grouped by user' do
        expect(described_class.user_counts).to eq({ users[0].id => 1, users[1].id => 2 })
      end
    end
  end

  describe '::CHANNELS' do
    it 'contains 4 channels' do
      expect(described_class::CHANNELS.keys.size).to eq(4)
    end
  end

  describe '#channel' do
    it 'returns the channel name' do
      message = build(:discord_message, channel_id: '997442331202564176')

      expect(message.channel).to eq('#contribute')
    end
  end

  describe '#web_url' do
    it 'returns the web url' do
      message = build(:discord_message, channel_id: '997442331202564176', id: 123)

      expect(message.web_url).to eq('https://discord.com/channels/778180511088640070/997442331202564176/123')
    end
  end
end
