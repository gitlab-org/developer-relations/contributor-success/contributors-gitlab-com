# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  id
  user_id
  added_date
].freeze

RSpec.describe Note, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to belong_to(:user).required }
    it { is_expected.to belong_to(:issue).optional }
    it { is_expected.to belong_to(:merge_request).optional }
  end

  describe 'validations' do
    describe 'noteable_present' do
      let(:issue) { build(:issue) }
      let(:merge_request) { build(:merge_request) }

      context 'when issue and merge request are nil' do
        subject(:instance) do
          build(:note).tap do |note|
            note.issue = nil
            note.merge_request = nil
          end
        end

        it { is_expected.not_to be_valid }
      end

      context 'when only merge_request is present' do
        subject(:instance) { build(:note, issue: nil, merge_request:) }

        it { is_expected.to be_valid }
      end

      context 'when only issue is present' do
        subject(:instance) { build(:note, issue:, merge_request: nil) }

        it { is_expected.to be_valid }
      end

      context 'when merge_request and issue are present' do
        subject(:instance) { build(:note, issue:, merge_request:) }

        it { is_expected.not_to be_valid }
      end
    end
  end

  describe '.noteable' do
    let(:issue) { build(:issue) }
    let(:merge_request) { build(:merge_request) }

    context 'with an issue' do
      let(:note) { create(:note, issue:, merge_request: nil) }

      it 'returns the issue' do
        expect(note.noteable).to eq(issue)
      end
    end

    context 'with a merge request' do
      let(:note) { create(:note, issue: nil, merge_request:) }

      it 'returns the merge request' do
        expect(note.noteable).to eq(merge_request)
      end
    end
  end

  describe '.web_url' do
    let(:issue) { build(:issue, web_url: 'issue_url') }
    let(:merge_request) { build(:merge_request, web_url: 'mr_url') }

    context 'with an issue' do
      let(:note) { create(:note, issue:, merge_request: nil, id: 123) }

      it 'returns the issue' do
        expect(note.web_url).to eq('issue_url#note_123')
      end
    end

    context 'with a merge request' do
      let(:note) { create(:note, issue: nil, merge_request:, id: 456) }

      it 'returns the merge request' do
        expect(note.web_url).to eq('mr_url#note_456')
      end
    end
  end

  describe 'scopes' do
    describe 'added_between' do
      let!(:user) { create(:user) }
      let!(:note_one) { create(:note, user:, added_date: 15.days.ago) }
      let!(:note_two) { create(:note, user:, added_date: 10.days.ago) }
      let!(:note_three) { create(:note, user:, added_date: 5.days.ago) }

      context 'with from_date' do
        it 'returns notes opened after the given dates' do
          expect(described_class.added_between(12.days.ago, nil)).to contain_exactly(note_two, note_three)
        end
      end

      context 'with to_date' do
        it 'returns notes opened before the given dates' do
          expect(described_class.added_between(nil, 7.days.ago)).to contain_exactly(note_one, note_two)
        end
      end

      context 'with to_date and from_date' do
        it 'returns notes opened between the given dates' do
          expect(described_class.added_between(12.days.ago, 7.days.ago)).to contain_exactly(note_two)
        end
      end
    end

    describe 'user_counts' do
      let(:users) { create_list(:user, 2) }

      before do
        create(:note, user: users[0])
        create_list(:note, 2, user: users[1])
      end

      it 'returns note counts grouped by user' do
        expect(described_class.user_counts).to eq({ users[0].id => 1, users[1].id => 2 })
      end
    end
  end
end
