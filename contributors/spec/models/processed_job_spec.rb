# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  group
  issuable_type
  processed_records
  elapsed
  activity_date
].freeze

RSpec.describe ProcessedJob, required_fields: REQUIRED_FIELDS
