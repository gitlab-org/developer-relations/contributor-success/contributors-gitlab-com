# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  username
].freeze

RSpec.describe TeamMember, required_fields: REQUIRED_FIELDS do
  describe 'validations' do
    it { is_expected.to allow_value([true, false]).for(:mr_coach) }
    it { is_expected.to allow_value([true, false]).for(:available) }
  end

  describe 'scopes' do
    let!(:available_mr_coach) { create(:team_member, mr_coach: true, available: true) }
    let!(:busy_mr_coach) { create(:team_member, mr_coach: true, available: false) }
    let!(:available_non_mr_coach) { create(:team_member, mr_coach: false, available: true) }

    before do
      create(:team_member, mr_coach: false, available: false)
    end

    describe 'mr_coach' do
      it 'returns only mr coaches' do
        expect(described_class.mr_coach).to contain_exactly(available_mr_coach, busy_mr_coach)
      end
    end

    describe 'available' do
      it 'returns only available team members' do
        expect(described_class.available).to contain_exactly(available_mr_coach, available_non_mr_coach)
      end
    end
  end

  describe '.random_available_coach' do
    let!(:available_mr_coach) { create(:team_member, mr_coach: true, available: true).username }
    let!(:available_mr_coach2) { create(:team_member, mr_coach: true, available: true).username }

    before do
      create(:team_member, mr_coach: true, available: false)
      create(:team_member, mr_coach: false, available: true)
      create(:team_member, mr_coach: false, available: false)
    end

    it 'returns a random available mr coach' do
      expect([available_mr_coach, available_mr_coach2]).to include(described_class.random_available_coach)
    end
  end
end
