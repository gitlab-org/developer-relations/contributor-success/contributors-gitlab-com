# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  id
  username
].freeze

RSpec.describe User, required_fields: REQUIRED_FIELDS do
  it { is_expected.to have_many(:issues).dependent(:delete_all) }
  it { is_expected.to have_many(:merge_requests).dependent(:delete_all) }
  it { is_expected.to have_many(:notes).dependent(:delete_all) }
  it { is_expected.to have_many(:bonus_points).class_name('BonusPoints').dependent(nil) }
  it { is_expected.to have_many(:awarded_bonus_points).class_name('BonusPoints').dependent(nil) }
  it { is_expected.to have_many(:rewards).dependent(nil) }
  it { is_expected.to have_many(:awarded_rewards).class_name('Reward').dependent(nil) }

  it { is_expected.to have_one(:user_profile).dependent(nil) }

  describe 'validations' do
    it { is_expected.to allow_value([true, false]).for(:community_member) }
    it { is_expected.not_to allow_value(nil).for(:community_member) }
  end

  describe 'scopes' do
    let(:community_users) { create_list(:user, 2, community_member: true) }
    let(:other_users) { create_list(:user, 2, community_member: false) }
    let(:overridden_community_user) do
      create(:user, community_member: false, user_profile: build(:user_profile, community_member_override: true))
    end
    let(:overridden_other_user) do
      create(:user, community_member: true, user_profile: build(:user_profile, community_member_override: false))
    end

    describe 'wider_community' do
      it 'only returns wider community members' do
        expect(described_class.wider_community).to contain_exactly(*community_users, overridden_community_user)
      end
    end

    describe 'other_members' do
      it 'only returns other members' do
        expect(described_class.other_members).to contain_exactly(*other_users, overridden_other_user)
      end
    end
  end

  describe '#web_url' do
    it 'returns the user web_url' do
      user = create(:user, username: 'asdf')

      expect(user.web_url).to eq('https://gitlab.com/asdf')
    end
  end
end
