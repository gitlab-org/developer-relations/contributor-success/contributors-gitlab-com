# frozen_string_literal: true

require_relative 'activity_summary_shared_examples'

RSpec.describe Views::ActivitySummary do
  it_behaves_like 'activity summary view'

  it 'returns activity older than 90 days' do
    create(:issue, opened_date: 100.days.ago)

    described_class.refresh(concurrently: false)

    expect(described_class.count).to eq(1)
  end
end
