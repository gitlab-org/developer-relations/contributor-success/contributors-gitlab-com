# frozen_string_literal: true

RSpec.describe OrganizationQuery do
  subject(:query) { described_class.new('Foo', **timeframe) }

  let(:timeframe) { { from_date: nil, to_date: nil } }
  let(:bob) { create(:user, username: 'bob', organization: 'Foo', community_member: true) }
  let(:alice) { create(:user, username: 'alice', organization: 'Foo', community_member: true) }
  let(:expected_bob) do
    {
      user_id: bob.id,
      username: bob.username,
      merged_merge_requests: 0,
      merged_with_issues: 0,
      opened_merge_requests: 0,
      opened_issues: 1,
      merged_commits: 0,
      added_notes: 0,
      contributor_level: 0,
      score: 5,
      bonus_points: 0
    }.stringify_keys
  end
  let(:expected_alice) do
    {
      user_id: alice.id,
      username: alice.username,
      merged_merge_requests: 0,
      merged_with_issues: 0,
      opened_merge_requests: 0,
      opened_issues: 1,
      merged_commits: 0,
      added_notes: 0,
      contributor_level: 0,
      score: 5,
      bonus_points: 0
    }.stringify_keys
  end

  before do
    johny = create(:user, organization: 'Bar', community_member: true)
    create(:issue, user: bob, opened_date: 1.week.ago)
    create(:issue, user: alice, opened_date: 1.day.ago)
    create(:issue, user: johny, opened_date: 1.day.ago)

    Views::ActivitySummary.refresh(concurrently: false)
    Views::RecentActivitySummary.refresh(concurrently: false)
  end

  it 'returns expected organization with contributors info' do
    result = query.execute
    expect(result[:name]).to eq('Foo')
    expect(result[:contributors].as_json).to contain_exactly(expected_bob, expected_alice)
  end

  it "doesn't return gitlab core members" do
    bob.update!(community_member: false)
    Views::ActivitySummary.refresh

    result = query.execute
    expect(result[:name]).to eq('Foo')
    expect(result[:contributors].as_json).to contain_exactly(expected_alice)
  end

  context 'with timeframe' do
    context 'with from_date only' do
      let(:timeframe) { { from_date: 2.days.ago.to_date, to_date: nil } }

      it 'returns active contributors for selected period' do
        result = query.execute
        expect(result[:name]).to eq('Foo')
        expect(result[:contributors].as_json).to contain_exactly(expected_alice)
      end
    end

    context 'with to_date only' do
      let(:timeframe) { { from_date: nil, to_date: 3.days.ago.to_date } }

      it 'returns active contributors for selected period' do
        result = query.execute
        expect(result[:name]).to eq('Foo')
        expect(result[:contributors].as_json).to contain_exactly(expected_bob)
      end
    end

    context 'with both from_date and to_date' do
      let(:timeframe) { { from_date: 2.weeks.ago.to_date, to_date: 3.days.ago.to_date } }

      it 'returns active contributors for selected period' do
        result = query.execute
        expect(result[:name]).to eq('Foo')
        expect(result[:contributors].as_json).to contain_exactly(expected_bob)
      end
    end
  end
end
