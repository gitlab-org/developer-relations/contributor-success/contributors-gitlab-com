# frozen_string_literal: true

RSpec.describe OrganizationsQuery do
  subject(:query) { described_class.new(filter_options) }

  let(:filter_options) { {} }
  let(:expected_organizations) do
    [
      { name: 'Foo', contributors_count: 2, score: 10 },
      { name: 'Bar', contributors_count: 1, score: 1 }
    ].map(&:stringify_keys)
  end
  let(:expected_total_count) { 2 }

  before do
    bob = create(:user, organization: 'Foo', community_member: true)
    alice = create(:user, organization: 'Foo', community_member: true)
    johny = create(:user, organization: 'Bar', community_member: true)
    create(:issue, user: bob, opened_date: 1.week.ago)
    create(:issue, user: alice, opened_date: 1.day.ago)
    create(:note, user: johny, added_date: 1.day.ago)
  end

  shared_examples 'works correctly' do
    it 'returns expected organizations in correct order' do
      Views::ActivitySummary.refresh(concurrently: false)

      records = query.execute
      expect(records.total_count).to eq(expected_total_count)
      expect(records.as_json).to eq(expected_organizations)
    end
  end

  it_behaves_like 'works correctly'

  context 'with organization that includes GitLab dirrect members' do
    before do
      gitlab_user = create(:user, community_member: false)
      create(:issue, user: gitlab_user, opened_date: 1.day.ago)
    end

    it_behaves_like 'works correctly'
  end

  context 'with blank organization' do
    before do
      no_name = create(:user, organization: '', community_member: true)
      create(:issue, user: no_name, opened_date: 1.day.ago)
    end

    it_behaves_like 'works correctly'
  end

  context 'when filters exists' do
    context 'with timeframe' do
      let(:filter_options) { { from_date: 2.days.ago.to_date } }
      let(:expected_organizations) do
        [
          { name: 'Foo', contributors_count: 1, score: 5 },
          { name: 'Bar', contributors_count: 1, score: 1 }
        ].map(&:stringify_keys)
      end

      it_behaves_like 'works correctly'
    end

    context 'with search' do
      let(:filter_options) { { search: 'f' } }
      let(:expected_organizations) do
        [
          { name: 'Foo', contributors_count: 2, score: 10 }
        ].map(&:stringify_keys)
      end
      let(:expected_total_count) { 1 }

      it_behaves_like 'works correctly'
    end
  end

  context 'with non-default page_size and page' do
    let(:filter_options) { { page: 2, page_size: 1 } }
    let(:expected_organizations) { [{ name: 'Bar', contributors_count: 1, score: 1 }].map(&:stringify_keys) }

    it_behaves_like 'works correctly'
  end
end
