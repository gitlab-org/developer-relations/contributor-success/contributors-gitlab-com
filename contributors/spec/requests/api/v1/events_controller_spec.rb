# frozen_string_literal: true

describe Api::V1::EventsController, vcr: { cassette_name: 'Events_CreateService/execute' } do
  describe 'POST #create' do
    subject(:create_event) { post api_v1_events_path, params: event_params }

    let(:user) { create(:user, id: 1_000_000) }
    let(:event_params) do
      {
        name: 'Community event #999',
        date: Date.new(2024, 12, 24).to_s,
        link: 'example.com',
        role: 'organizer',
        virtual: true,
        size: 'small',
        note: 'few test words about the event'
      }
    end

    context 'when logged in' do
      before do
        sign_in(user.id)
      end

      it 'responds with event issue data' do
        create_event

        expect(response).to have_http_status(:success)
        expect(json_body.keys).to include('web_url')
      end
    end

    it_behaves_like 'an endpoint requiring login'
  end
end
