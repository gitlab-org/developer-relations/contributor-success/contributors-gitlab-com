# frozen_string_literal: true

RSpec.describe Api::V1::ManageMergeRequestController do
  describe 'GET #index' do
    subject(:get_index) { get api_v1_manage_merge_request_index_path }

    before do
      allow(TeamMember).to receive(:random_available_coach).and_return('coach')
    end

    it_behaves_like 'an endpoint requiring login'

    context 'when logged in' do
      let(:current_user) { create(:user) }

      before do
        sign_in(current_user.id, current_user.username)
      end

      it 'returns expected data' do
        get_index

        expect(json_body).to include(
          'authorized' => false,
          'randomAvailableCoach' => 'coach'
        )
      end

      context 'when team member' do
        it 'returns expected data' do
          create(:team_member, username: current_user.username)

          get_index

          expect(json_body).to include(
            'authorized' => true,
            'randomAvailableCoach' => 'coach'
          )
        end
      end

      context 'when approved community member' do
        it 'returns expected data' do
          create(:community_member, user_id: current_user.id)

          get_index

          expect(json_body).to include(
            'authorized' => true,
            'randomAvailableCoach' => 'coach'
          )
        end
      end
    end
  end

  describe 'POST request_review' do
    subject(:request_review) do
      params = { project_id: 1, merge_request_iid: 2, include_duo:, reviewers: }

      post request_review_api_v1_manage_merge_request_index_path, params:
    end

    let(:include_duo) { false }
    let(:reviewers) { ['coach'] }
    let(:gitlab_rest_service) { instance_double(GitlabRestService) }
    let(:start_thread_response) { instance_double(HTTParty::Response, code: 201) }

    before do
      stub_env('GITLAB_BOT_API_TOKEN', 'token')

      allow(gitlab_rest_service).to receive(:start_thread).and_return(start_thread_response)
      allow(GitlabRestService).to receive(:new).and_return(gitlab_rest_service)
    end

    it_behaves_like 'an endpoint requiring login'

    context 'when logged in' do
      before do
        sign_in

        request_review
      end

      it 'creates a new thread in the merge request' do
        expect(gitlab_rest_service).to have_received(:start_thread) do |project_id, mr_iid, type, _|
          expect(project_id).to eq('1')
          expect(mr_iid).to eq('2')
          expect(type).to eq('merge_requests')
        end
      end

      it 'mentions the requester and adds the ~"workflow::ready for review" label' do
        expect(gitlab_rest_service).to have_received(:start_thread) do |_, _, _, message|
          expected_messages = [
            'Hey @lee, thanks for requesting a review!',
            '/label ~"workflow::ready for review"'
          ]

          expected_messages.each do |expected_message|
            expect(message).to include(expected_message)
          end

          expect(message).not_to include(
            'Feel free to address any feedback by @GitLabDuo, or wait for another reviewer to validate it first!'
          )
        end
      end

      context 'when include_duo is true' do
        let(:include_duo) { true }

        it 'mentions and requests a rview from @GitLabDuo' do
          expect(gitlab_rest_service).to have_received(:start_thread) do |_, _, _, message|
            expected_messages = [
              'Feel free to address any feedback by @GitLabDuo, or wait for another reviewer to validate it first!',
              'Hey @coach @GitLabDuo :wave:',
              '/request_review @coach @GitLabDuo'
            ]

            expected_messages.each do |expected_message|
              expect(message).to include(expected_message)
            end
          end
        end
      end

      context 'when include_duo is false' do
        it 'does not mention or request a review from GitLabDuo' do
          sign_in

          request_review

          expect(gitlab_rest_service).to have_received(:start_thread) do |_, _, _, message|
            expected_messages = [
              'Hey @coach :wave:',
              '/request_review @coach'
            ]

            expected_messages.each do |expected_message|
              expect(message).to include(expected_message)
            end
          end
        end
      end

      context 'when multiple reviewers are specified' do
        let(:reviewers) { %w[coach coach2] }

        it 'mentions all reviewers' do
          sign_in

          request_review

          expect(gitlab_rest_service).to have_received(:start_thread) do |_, _, _, message|
            expected_messages = [
              'Hey @coach @coach2 :wave:',
              '/request_review @coach @coach2'
            ]

            expected_messages.each do |expected_message|
              expect(message).to include(expected_message)
            end
          end
        end
      end

      context 'when reviewer name contains special characters' do
        let(:reviewers) { ['@coach', "\n/close"] }

        it 'strips the special characters' do
          sign_in

          request_review

          expect(gitlab_rest_service).to have_received(:start_thread) do |_, _, _, message|
            expected_messages = [
              'Hey @coach @close :wave:',
              '/request_review @coach @close'
            ]

            expected_messages.each do |expected_message|
              expect(message).to include(expected_message)
            end
          end
        end
      end

      context 'when something goes wrong' do
        let(:start_thread_response) { instance_double(HTTParty::Response, code: 500) }

        it 'returns internal_server_error' do
          expect(response).to have_http_status(:internal_server_error)
        end
      end
    end
  end
end
