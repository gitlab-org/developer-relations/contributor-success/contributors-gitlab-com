# frozen_string_literal: true

RSpec.describe Api::V1::OrganizationsController do
  let(:bob) { create(:user, username: 'bob', organization: 'Foo', community_member: true) }
  let(:alice) { create(:user, username: 'alice', organization: 'Foo', community_member: true) }

  before do
    create(
      :merge_request,
      user: bob, opened_date: 1.day.ago, merged_date: 1.day.ago, state_id: :merged
    )
    create(:issue, user: bob, opened_date: 1.week.ago)
    create(:issue, user: alice, opened_date: 1.day.ago)

    john = create(:user, organization: 'Bar', community_member: true)
    create(:issue, user: john, opened_date: 1.day.ago)

    Views::ActivitySummary.refresh(concurrently: false)
    Views::RecentActivitySummary.refresh(concurrently: false)
  end

  describe 'GET #index' do
    it 'returns expected organizations' do
      get api_v1_organizations_path

      expect(json_body.count).to eq(2)
      expect(json_body['records']).to eq(
        [
          {
            'name' => 'Foo',
            'contributors_count' => 2,
            'score' => 90
          },
          {
            'name' => 'Bar',
            'contributors_count' => 1,
            'score' => 5
          }
        ]
      )
    end

    it_behaves_like 'a paginated endpoint', key_attr_json: 'name' do
      let(:endpoint) { api_v1_organizations_path }
      let(:first_record) { { 'name' => alice.organization } }
      let(:second_record) { { 'name' => 'Bar' } }
    end

    context 'with filters' do
      let(:from_date) { 2.days.ago.to_date.to_s }
      let(:to_date) { Time.zone.now.to_date }
      let(:search) { 'f' }

      it 'returns expected organizations' do
        get api_v1_organizations_path, params: { from_date:, to_date:, search: }

        expect(json_body['records']).to eq(
          [
            {
              'name' => 'Foo',
              'contributors_count' => 2,
              'score' => 85
            }
          ]
        )
      end
    end
  end

  describe 'GET #show' do
    let(:expected_bob) do
      {
        'user_id' => bob.id,
        'username' => bob.username,
        'merged_merge_requests' => 1,
        'merged_with_issues' => 0,
        'opened_merge_requests' => 1,
        'opened_issues' => 1,
        'merged_commits' => 0,
        'added_notes' => 0,
        'contributor_level' => 0,
        'score' => 85,
        'bonus_points' => 0
      }
    end

    let(:expected_alice) do
      {
        'user_id' => alice.id,
        'username' => alice.username,
        'merged_merge_requests' => 0,
        'merged_with_issues' => 0,
        'opened_merge_requests' => 0,
        'opened_issues' => 1,
        'merged_commits' => 0,
        'added_notes' => 0,
        'contributor_level' => 0,
        'score' => 5,
        'bonus_points' => 0
      }
    end

    it 'returns expected organization data' do
      get api_v1_organizations_path, params: { id: 'Foo' }

      expect(json_body['name']).to eq('Foo')
      expect(json_body['contributors'].size).to eq(2)
      expect(json_body['contributors'][0]).to eq(expected_bob)
      expect(json_body['contributors'][1]).to eq(expected_alice)
    end

    context 'with special charactars in the name' do
      let(:organization) { './@' }
      let(:bob) { create(:user, username: 'bob', organization:, community_member: true) }

      it 'returns expected organization' do
        get api_v1_organizations_path, params: { id: organization }

        expect(json_body['name']).to eq(organization)
      end
    end
  end
end
