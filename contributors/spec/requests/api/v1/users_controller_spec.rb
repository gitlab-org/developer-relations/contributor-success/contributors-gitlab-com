# frozen_string_literal: true

RSpec.describe Api::V1::UsersController do
  let(:user) { create(:user, username: 'lee', contributor_level: 1, points: 100) }
  let(:id) { user.id }
  let(:from_date) { Date.parse('2024-01-01') }
  let(:to_date) { Date.parse('2024-01-31') }
  let(:before_date) { Date.parse('2023-01-01') }
  let(:during_date) { Date.parse('2024-01-10') }
  let(:after_date) { Date.parse('2024-03-01') }

  describe 'GET #show' do
    before do
      [before_date, during_date, during_date, after_date].each do |date|
        labels = %w[a b c]
        labels << 'linked-issue' if date == during_date
        merge_request = create(:merge_request, user:, merged_date: date, opened_date: date, labels:)
        create(:commit, user:, merge_request:)
        create(:issue, user:, opened_date: date)
        create(:note, user:, added_date: date)
        create(:bonus_points, user:, created_at: date)
        create(:discord_message, user:, added_date: date)
        create(:forum_post, user:, added_date: date)
        create(:reward, user:)
      end
    end

    it 'returns the records occurring during the date range' do
      get api_v1_user_path(id:), params: { from_date:, to_date: }

      expect(response).to have_http_status(:ok)

      expect(json_body).to match(
        hash_including(
          'username' => 'lee',
          'merged_merge_requests' => have_attributes(count: 2),
          'opened_merge_requests' => have_attributes(count: 2),
          'merged_commits' => have_attributes(count: 2),
          'opened_issues' => have_attributes(count: 2),
          'added_notes' => have_attributes(count: 2),
          'bonus_points' => have_attributes(count: 2),
          'discord_messages' => have_attributes(count: 2),
          'forum_posts' => have_attributes(count: 2),
          'rewards' => nil,
          'contributor_level' => 1,
          'points' => 100
        )
      )
    end

    it 'when logged in as admin' do
      sign_in(admin: true)

      get api_v1_user_path(id:)

      expect(json_body).to match(
        hash_including(
          'username' => 'lee',
          'rewards' => have_attributes(count: 4)
        )
      )
    end

    it 'when logged in as same user' do
      sign_in(id)

      get api_v1_user_path(id:)

      expect(json_body).to match(
        hash_including(
          'username' => 'lee',
          'rewards' => have_attributes(count: 4)
        )
      )
    end

    context 'with a broader date range' do
      let(:from_date) { Date.parse('2021-01-01') }
      let(:to_date) { Date.parse('2025-01-01') }

      it 'returns the records occurring during the date range' do
        get api_v1_user_path(id:), params: { from_date:, to_date: }

        expect(response).to have_http_status(:ok)

        expect(json_body).to match(
          hash_including(
            'username' => 'lee',
            'merged_merge_requests' => have_attributes(count: 4),
            'opened_merge_requests' => have_attributes(count: 4),
            'merged_commits' => have_attributes(count: 4),
            'opened_issues' => have_attributes(count: 4),
            'added_notes' => have_attributes(count: 4),
            'bonus_points' => have_attributes(count: 4),
            'discord_messages' => have_attributes(count: 4),
            'forum_posts' => have_attributes(count: 4),
            'rewards' => nil
          )
        )
      end

      it 'returns the correct has_linked_issue values' do
        get api_v1_user_path(id:), params: { from_date:, to_date: }

        expect(json_body['merged_merge_requests']).to contain_exactly(
          hash_including('has_linked_issue' => false),
          hash_including('has_linked_issue' => true),
          hash_including('has_linked_issue' => true),
          hash_including('has_linked_issue' => false)
        )

        expect(json_body['opened_merge_requests']).to contain_exactly(
          hash_including('has_linked_issue' => false),
          hash_including('has_linked_issue' => true),
          hash_including('has_linked_issue' => true),
          hash_including('has_linked_issue' => false)
        )
      end
    end

    context 'when an invalid id is provided' do
      it 'returns not found status' do
        get api_v1_user_path(id: 'invalid')

        expect(response).to have_http_status(:not_found)
      end
    end

    context 'when passed a username' do
      it 'returns the correct user' do
        get api_v1_user_path(id: user.username)

        expect(response).to have_http_status(:ok)
        expect(json_body['id']).to eq(user.id)
      end

      context 'with special characters' do
        let(:user) { create(:user, username: 'lee.tickett') }

        it 'returns the correct user' do
          get api_v1_user_path(id: user.username)

          expect(response).to have_http_status(:ok)
          expect(json_body['id']).to eq(user.id)
        end
      end
    end

    context 'when username case is different' do
      it 'returns the expected user' do
        get api_v1_user_path(id: 'LEE')

        expect(json_body).to match(hash_including('username' => 'lee'))
      end
    end
  end

  describe 'GET #edit' do
    subject(:user_edit) { get edit_api_v1_user_path(id:) }

    it_behaves_like 'an endpoint requiring admin'

    it 'returns the expected data' do
      sign_in(admin: true)

      user_edit

      expect(response).to have_http_status(:ok)

      expect(json_body).to match hash_including({
        'id' => id,
        'username' => user.username,
        'community_member' => user.community_member,
        'community_member_override' => nil
      })
    end
  end

  describe 'PUT #update' do
    subject(:user_update) { put api_v1_user_path(id:), params: { community_member_override: } }

    let!(:user) { create(:user, username: 'lee', community_member:) }
    let(:community_member) { false }
    let(:community_member_override) { !community_member }
    let(:id) { user.id }

    it_behaves_like 'an endpoint requiring admin'

    context 'when setting community_member_override true' do
      it 'overrides the flag' do
        expect(user.community_member).to eq(community_member)
        expect(user.user_profile).to be_nil

        sign_in(admin: true)

        user_update

        expect(response).to have_http_status(:ok)
        expect(json_body['message']).to eq('User updated successfully')

        user.reload
        expect(user.user_profile.community_member_override).to eq(community_member_override)
        expect(user.community_member).to eq(community_member_override)
      end
    end

    context 'when passing an invalid id' do
      let(:id) { 9_999 }

      it 'returns an unprocessable_content response' do
        sign_in(admin: true)

        user_update

        expect(response).to have_http_status(:unprocessable_content)
      end
    end

    context 'when setting community_member_override false' do
      let(:community_member) { true }

      it 'overrides the flag' do
        expect(user.community_member).to eq(community_member)
        expect(user.user_profile).to be_nil

        sign_in(admin: true)

        user_update

        expect(response).to have_http_status(:ok)
        expect(json_body['message']).to eq('User updated successfully')

        user.reload
        expect(user.user_profile.community_member_override).to eq(community_member_override)
        expect(user.community_member).to eq(community_member_override)
      end
    end

    it 'queues an refresh of ActivitySummary' do
      sign_in(admin: true)
      ActiveJob::Base.queue_adapter = :test

      expect { user_update }.to have_enqueued_job(ActivitySummaryRefreshJob)
    end
  end

  describe 'POST #add_points' do
    subject(:add_points) { post add_points_api_v1_user_path(id:), params: { points:, reason:, activity_type: } }

    let!(:awarder) { create(:user, id: 1) }

    let(:points) { '100' }
    let(:reason) { 'Reason' }
    let(:activity_type) { 'social' }

    it_behaves_like 'an endpoint requiring admin', :created

    it 'creates the bonus points with expected attributes' do
      sign_in(admin: true)

      add_points

      expect(BonusPoints.last.attributes.transform_keys(&:to_sym)).to match hash_including(
        user_id: user.id,
        points: points.to_i,
        reason:,
        activity_type:,
        awarded_by_user_id: awarder.id
      )
    end
  end
end
