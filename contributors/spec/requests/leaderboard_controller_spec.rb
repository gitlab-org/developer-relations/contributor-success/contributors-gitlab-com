# frozen_string_literal: true

RSpec.describe LeaderboardController do
  describe 'GET #me' do
    let(:alice) { create(:user, username: 'Alice', community_member: true) }
    let(:bobby) { create(:user, username: 'Bobby', community_member: true) }

    before do
      create(
        :merge_request,
        user: alice,
        opened_date: 1.day.ago
      )
      create(
        :issue,
        user: bobby,
        opened_date: 1.day.ago
      )
      Views::RecentActivitySummary.refresh(concurrently: false)
    end

    it 'redirects to expected leaderboard page' do
      sign_in(bobby.id)

      get leaderboard_me_path(page_size: 1)

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to('/leaderboard?page=2&page_size=1')
    end

    context 'when not logged in' do
      it 'redirects to login page with return param' do
        get leaderboard_me_path(page_size: 1)

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to('/login?return=%2Fleaderboard%2Fme%3Fpage_size%3D1')
      end
    end

    context 'when logged in user does not appear on the leaderboard' do
      it 'redirects to generic leaderboard page' do
        newbie = create(:user)
        sign_in(newbie.id, newbie.username)

        get leaderboard_me_path(page_size: 1)

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to('/leaderboard')
      end
    end
  end
end
