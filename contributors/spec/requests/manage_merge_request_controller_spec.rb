# frozen_string_literal: true

RSpec.describe ManageMergeRequestController do
  let(:gitlab_rest_service) { instance_double(GitlabRestService) }
  let(:params) { { project_id: 100, merge_request_iid: 999 } }

  before do
    allow(GitlabRestService).to receive(:new).and_return(gitlab_rest_service)
  end

  shared_examples 'manage_merge_reequest request' do
    context 'when not logged in' do
      it 'redirects to login' do
        get(path, params:)

        expect(response).to redirect_to(login_path(return: request.fullpath))
      end
    end

    context 'when logged in' do
      let(:rest_response) { instance_double(HTTParty::Response, code: 201) }

      before do
        allow(gitlab_rest_service).to receive(:bot_request).and_return(rest_response)
        sign_in
      end

      context 'when a parameter is missing' do
        it 'returns bad request response' do
          params.delete(:project_id)

          get(path, params:)

          expect(response).to have_http_status(:bad_request)
        end
      end

      context 'when success' do
        let(:web_url) { 'https://gitlab.example/project/merge_request' }

        before do
          allow(gitlab_rest_service).to receive(:get_merge_request_web_url).and_return(web_url)
        end

        it 'calls gitlab_rest_service.get_merge_request_web_url' do
          get(path, params:)

          expect(gitlab_rest_service).to have_received(:get_merge_request_web_url).with('100', '999')
        end

        it 'redirects to merge request web_url' do
          get(path, params:)

          expect(response).to redirect_to(web_url)
        end
      end

      context 'when failures' do
        let(:to_json) { '{"message":"404 Project Not Found"}' }
        let(:rest_response) { instance_double(HTTParty::Response, code: 404, to_json:) }

        it 'returns error json' do
          get(path, params:)

          expect(response.body).to eq(to_json)
        end
      end
    end
  end

  describe 'GET #request_help' do
    let(:path) { request_help_path }

    it_behaves_like 'manage_merge_reequest request'
  end

  describe 'GET #request_review' do
    let(:path) { request_review_path }

    context 'when not logged in' do
      it 'redirects to login' do
        get(path, params:)

        expect(response).to redirect_to(login_path(return: request.fullpath))
      end
    end

    context 'when logged in' do
      it 'redirects to new path' do
        sign_in

        get(path, params:)

        expect(response).to redirect_to(
          "/manage-merge-request?projectId=#{params[:project_id]}&mergeRequestIid=#{params[:merge_request_iid]}"
        )
      end
    end
  end
end
