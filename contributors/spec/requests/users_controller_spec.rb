# frozen_string_literal: true

RSpec.describe UsersController do
  describe 'GET #login' do
    it 'redirects to GitLab OAuth URL' do
      stub_env('APP_ID', 'app_id')

      regex_pattern = %r{
        https://gitlab\.com/oauth/authorize
        \?client_id=app_id
        &redirect_uri=http%3A%2F%2Flocalhost%3A3030%2Foauth_callback
        &response_type=code
        &scope=api
        &state=[a-f0-9]{32}
      }x

      get login_path

      expect(response).to redirect_to(regex_pattern)
    end
  end

  describe 'GET #logout' do
    it 'redirects to root' do
      get logout_path

      expect(response).to redirect_to(root_path)
    end

    it 'calls reset_session' do
      expect_any_instance_of(ActionDispatch::Flash::RequestMethods).to receive(:reset_session) # rubocop:disable RSpec/AnyInstance

      get logout_path
    end

    it 'clears session[:user_id], session[:username], and session[:admin]' do
      sign_in(admin: true)

      expect(session.keys).to contain_exactly(
        'session_id',
        'user_id',
        'username',
        'admin',
        'gitlab_access_token',
        'gitlab_refresh_token',
        'gitlab_expires_at'
      )

      get logout_path

      expect(session.keys).to contain_exactly('session_id')
    end
  end

  describe 'GET #oauth_callback' do
    let(:contributor_success_users_ids) { [] }
    let(:user) { create(:user, :with_profile) }

    before do
      allow(GitlabOauthService).to receive(:authorize).with('abc123')
        .and_return({
          user_id: user.id,
          username: 'lee',
          gitlab_access_token: 'access_token',
          gitlab_expires_at: 1_715_031_945,
          gitlab_refresh_token: 'refresh_token'
        })
      allow(FetchContributorSuccessUsersService).to receive(:execute)
        .and_return(contributor_success_users_ids)

      get oauth_callback_path, params: { code: 'abc123' }
    end

    it 'redirects to root' do
      expect(response).to redirect_to(root_path)
    end

    it 'sets session attrs and platform_first_login_at for user' do
      expect(session[:user_id]).to eq(user.id)
      expect(session[:username]).to eq('lee')
      expect(user.user_profile.reload.platform_first_login_at)
        .to be_within(2.seconds).of(Time.zone.now)
    end

    context 'when regular user' do
      it 'sets session[:admin] false' do
        expect(session[:admin]).to be(false)
      end
    end

    context 'when admin' do
      let(:contributor_success_users_ids) { [user.id] }

      it 'sets session[:admin] true' do
        expect(session[:admin]).to be(true)
      end
    end
  end
end
