# frozen_string_literal: true

RSpec.describe BrilliantService do
  let(:token) { 'brilliant-api-token' }
  let(:service) { described_class.new }

  before do
    stub_env('BRILLIANT_API_TOKEN', token)
    allow(SecureRandom).to receive(:uuid).and_return('9029b216-9296-4ad7-a777-405b4a864370')
  end

  describe '#initialize' do
    it 'sets up headers with the correct token' do
      expect(service.instance_variable_get(:@headers)).to include(
        'Authorization' => "Basic #{token}",
        'Content-Type' => 'application/json'
      )
    end
  end

  describe '#create_storefront_gift_card', :vcr do
    let(:user_id) { 1_933_526 }
    let(:credits) { 2 }

    it 'sends the expected HTTP POST with the correct payload' do
      response = service.create_storefront_gift_card(user_id, credits)

      expect(response).to eq('GNALUZQQMTHQPWFQ')
    end

    context 'with a failed request', :vcr do
      it 'logs the error response' do
        allow(Rails.logger).to receive(:error)

        service.create_storefront_gift_card(user_id, credits)

        expect(Rails.logger).to have_received(:error) do |&block|
          expect(block.call).to eq("HTTP Basic: Access denied.\n")
        end
      end
    end
  end
end
