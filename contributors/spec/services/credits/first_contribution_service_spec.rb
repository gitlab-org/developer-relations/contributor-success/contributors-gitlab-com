# frozen_string_literal: true

RSpec.describe Credits::FirstContributionService do
  subject(:service) { described_class.new }

  let(:eligible_user) { create(:user, community_member: true) }
  let(:awarded_by_user_id) { create(:user).id }
  let!(:rewards_service) do
    rewards_service = instance_double(RewardsService)

    allow(RewardsService).to receive(:new).and_return(rewards_service)
    allow(rewards_service).to receive(:issue)

    rewards_service
  end

  before do
    # eligible
    create(:merge_request, :merged, merged_date: 5.days.ago, user_id: eligible_user.id)

    # ineligible - already has a reward
    ineligible_harry = create(:user)
    create(:reward, user_id: ineligible_harry.id)
    create(:merge_request, :merged, merged_date: 5.days.ago, user_id: ineligible_harry.id)

    # ineligible - MR before cutoff
    create(:merge_request, :merged, merged_date: 6.days.ago, user_id: create(:user, community_member: true).id)

    # ineligible - have at least 1 MR merged before cutoff
    ineligible_hermione = create(:user, community_member: true)
    create(:merge_request, :merged, merged_date: 4.days.ago, user_id: ineligible_hermione.id)
    create(:merge_request, :merged, merged_date: 10.days.ago, user_id: ineligible_hermione.id)

    # ineligible - not a community_member
    create(:merge_request, :merged, merged_date: 5.days.ago, user_id: create(:user, community_member: false).id)

    stub_env('GITLAB_USER_ID', awarded_by_user_id)
    stub_env('GITLAB_ACCESS_TOKEN', 'mock_token')
  end

  describe '#execute' do
    before do
      Views::RecentActivitySummary.refresh(concurrently: false)
    end

    it 'issues reward to eligible user with expected params' do
      allow(service).to receive(:fetch_eligible_users).and_return([eligible_user])

      service.execute

      expect(rewards_service).to have_received(:issue).with(
        user_id: eligible_user.id,
        username: eligible_user.username,
        credits: 5,
        reason: Reward.reasons[:first_contribution],
        note: 'Congratulations on having your first MR merged!',
        awarded_by_user_id: awarded_by_user_id
      )
    end
  end

  describe '#award_credits' do
    it 'awards credits to eligible users' do
      service.award_credits([eligible_user])

      expect(rewards_service).to have_received(:issue).with(
        user_id: eligible_user.id,
        username: eligible_user.username,
        credits: 5,
        reason: Reward.reasons[:first_contribution],
        note: 'Congratulations on having your first MR merged!',
        awarded_by_user_id: awarded_by_user_id
      )
    end

    context 'when overriding FIRST_CONTRIBUTION_CREDITS' do
      before do
        stub_env('FIRST_CONTRIBUTION_CREDITS', 10)
      end

      it 'awards the correct number of credits' do
        service.award_credits([eligible_user])

        expect(rewards_service).to have_received(:issue).with(
          user_id: eligible_user.id,
          username: eligible_user.username,
          credits: 10,
          reason: Reward.reasons[:first_contribution],
          note: 'Congratulations on having your first MR merged!',
          awarded_by_user_id: awarded_by_user_id
        )
      end
    end
  end

  describe '#fetch_eligible_users' do
    it 'returns eligible users' do
      Views::RecentActivitySummary.refresh(concurrently: false)

      eligible_users = service.fetch_eligible_users
      expect(eligible_users.first).to eq(eligible_user)
      expect(eligible_users.count).to eq(1)
    end
  end
end
