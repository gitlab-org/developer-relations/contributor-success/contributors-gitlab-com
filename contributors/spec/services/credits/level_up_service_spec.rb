# frozen_string_literal: true

RSpec.describe Credits::LevelUpService do
  subject(:service) { described_class.new }

  let(:current_achievements) do
    [
      {
        'id' => 'gid://gitlab/Achievements::UserAchievement/1',
        'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/1' }
      }
    ]
  end
  let(:eligable_users) do
    {
      level1: create(:user, community_member: true, contributor_level: 0, points: 100),
      level2: create(:user, community_member: true, contributor_level: 1, points: 1_000)
    }
  end
  let(:ineligable_users) do
    {
      team_member: create(:user, community_member: false, contributor_level: 0, points: 100),
      already_leveled_up: create(:user, community_member: true, contributor_level: 1, points: 100),
      no_recent_activity: create(:user, community_member: true, contributor_level: 1, points: 1_000)
    }
  end
  let(:awarded_by_user_id) { create(:user).id }
  let!(:rewards_service) do
    rewards_service = instance_double(RewardsService)

    allow(RewardsService).to receive(:new).and_return(rewards_service)
    allow(rewards_service).to receive(:issue)

    rewards_service
  end

  before do
    users = [
      eligable_users[:level1],
      eligable_users[:level2],
      ineligable_users[:team_member],
      ineligable_users[:already_leveled_up]
    ]
    users.each { |user| create(:issue, user:, opened_date: 5.days.ago) }

    create(:issue, user: ineligable_users[:no_recent_activity], opened_date: 6.days.ago)

    stub_env('GITLAB_USER_ID', awarded_by_user_id)
    stub_env('GITLAB_API_TOKEN', 'mock_token')

    Views::RecentActivitySummary.refresh(concurrently: false)

    allow(Gitlab::AchievementsService).to receive(:award)
    allow(Gitlab::AchievementsService).to receive(:current).and_return(current_achievements)
    allow(Gitlab::AchievementsService).to receive(:revoke)
    allow(Rails.logger).to receive(:info)
  end

  describe '#execute' do
    it 'issues reward to eligible user with expected params' do
      service.execute

      expect(rewards_service).to have_received(:issue).exactly(2).times
      expect(rewards_service).to have_received(:issue).with(
        user_id: eligable_users[:level1].id,
        username: eligable_users[:level1].username,
        credits: 5,
        reason: described_class::LEVEL_UP_REWARD_REASON,
        note: 'Congratulations on becoming a level 1 contributor!',
        awarded_by_user_id: awarded_by_user_id
      )
      expect(rewards_service).to have_received(:issue).with(
        user_id: eligable_users[:level2].id,
        username: eligable_users[:level2].username,
        credits: 50,
        reason: described_class::LEVEL_UP_REWARD_REASON,
        note: 'Congratulations on becoming a level 2 contributor!',
        awarded_by_user_id: awarded_by_user_id
      )
    end

    it 'updates the eligable users contributor_level' do
      expect { service.execute }.to change { eligable_users[:level1].reload.contributor_level }.from(0).to(1)
        .and change { eligable_users[:level2].reload.contributor_level }.from(1).to(2)
    end

    it 'issues achievements to the eligable users' do
      service.execute

      expect(Gitlab::AchievementsService).to have_received(:award).exactly(2).times
      expect(Gitlab::AchievementsService).to have_received(:award).with(
        eligable_users[:level1].id, 'gid://gitlab/Achievements::Achievement/59'
      )
      expect(Gitlab::AchievementsService).to have_received(:award).with(
        eligable_users[:level2].id, 'gid://gitlab/Achievements::Achievement/60'
      )
    end

    context 'with existing contributor achievement' do
      let(:current_achievements) do
        [
          {
            'id' => 'gid://gitlab/Achievements::UserAchievement/1',
            'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/1' }
          },
          {
            'id' => 'gid://gitlab/Achievements::UserAchievement/10',
            'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/59' }
          }
        ]
      end

      before do
        service.execute
      end

      it 'revokes the previous achievement' do
        expect(Gitlab::AchievementsService).to have_received(:revoke).once
        expect(Gitlab::AchievementsService).to have_received(:revoke).with(
          'gid://gitlab/Achievements::UserAchievement/10'
        )
      end

      it 'does not reissue if already received' do
        expect(Gitlab::AchievementsService).to have_received(:award).once
        expect(Gitlab::AchievementsService).to have_received(:award).with(
          eligable_users[:level2].id, 'gid://gitlab/Achievements::Achievement/60'
        )
      end
    end
  end
end
