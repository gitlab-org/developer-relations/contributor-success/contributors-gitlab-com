# frozen_string_literal: true

RSpec.describe DiscordService do
  let(:token) { 'fake_token' }
  let(:discord_service) { described_class.new }
  let(:channel_id) { '789' }

  before do
    stub_env('DISCORD_API_TOKEN', token)
  end

  describe '#initialize' do
    it 'sets up headers with the correct token' do
      expect(discord_service.instance_variable_get(:@headers)).to include(
        'Authorization' => "Bot #{token}",
        'Content-Type' => 'application/json'
      )
    end

    it 'initializes discord_users from the database' do
      user1 = create(:user, discord_id: '123')
      user2 = create(:user, discord_id: '456')
      create(:user)

      expect(discord_service.instance_variable_get(:@discord_users)).to eq({
        '123' => user1.id,
        '456' => user2.id
      })
    end
  end

  describe '#fetch_all_messages' do
    let(:weeks) { 2 }

    before do
      call_count = 0
      allow(discord_service).to receive(:fetch_messages) do
        call_count += 1

        discord_service.instance_variable_set(:@added_date, call_count.weeks.ago.to_s)
        []
      end
      allow(DiscordMessage).to receive(:upsert_all)
    end

    it 'fetches messages for the specified number of weeks' do
      discord_service.fetch_all_messages(channel_id, weeks)

      expect(discord_service).to have_received(:fetch_messages).with(channel_id).exactly(3).times
    end

    it 'inserts records into DiscordMessage' do
      discord_service.fetch_all_messages(channel_id, weeks)

      expect(DiscordMessage).to have_received(:upsert_all).exactly(3).times
    end
  end

  describe '#fetch_messages' do
    let(:response) { instance_double(HTTParty::Response, code: 200) }

    before do
      allow(HTTParty).to receive(:get).and_return(response)

      parsed_response = [
        { 'id' => '1', 'channel_id' => channel_id, 'author' => { 'id' => '1' }, 'timestamp' => '2023-01-01T00:00:00Z' },
        { 'id' => '2', 'channel_id' => channel_id, 'author' => { 'id' => '4' }, 'timestamp' => '2023-01-02T00:00:00Z' }
      ]
      allow(response).to receive(:parsed_response).and_return(parsed_response)
    end

    it 'makes an API request with correct parameters' do
      discord_service.fetch_messages(channel_id)

      expect(HTTParty).to have_received(:get).with(
        "#{DiscordService::BASE_URL}/channels/#{channel_id}/messages",
        hash_including(headers: discord_service.instance_variable_get(:@headers), query: { limit: 10 })
      )
    end

    it 'updates end_cursor and added_date' do
      discord_service.fetch_messages(channel_id)

      expect(discord_service.instance_variable_get(:@end_cursor)).to eq('2')
      expect(discord_service.instance_variable_get(:@added_date)).to eq('2023-01-02T00:00:00Z')
    end

    it 'fetches thread messages when present' do
      allow(discord_service).to receive(:fetch_thread_messages).and_return([])

      discord_service.fetch_messages(channel_id)

      expect(discord_service).to have_received(:fetch_thread_messages)
    end

    context 'when something goes wrong' do
      let(:error_response) { '{ "errors": [{ "message": "Error" }] }' }

      before do
        allow(discord_service).to receive(:make_api_request).and_return(error_response)
      end

      it 'logs the resposne' do
        allow(Rails.logger).to receive(:info)

        discord_service.fetch_messages(channel_id)

        expect(Rails.logger).to have_received(:info) do |&block|
          expect(block.call).to eq(error_response)
        end
      end
    end
  end

  describe '#discord_user_in_database' do
    let(:message) { { 'author' => { 'id' => '123' } } }

    it 'returns true when the discord user is in the database' do
      discord_service.instance_variable_set(:@discord_users, { '123' => 1 })

      expect(discord_service.discord_user_in_database(message)).to be(true)
    end

    it 'returns false when the discord user is not in the database' do
      discord_service.instance_variable_set(:@discord_users, { '456' => 2 })

      expect(discord_service.discord_user_in_database(message)).to be(false)
    end
  end

  describe '#make_api_request' do
    let(:response) { instance_double(HTTParty::Response, code: 200) }

    before do
      allow(HTTParty).to receive(:get).and_return(response)
    end

    it 'calls the expected URL and returns the expected response' do
      result = discord_service.make_api_request(channel_id, {})

      expect(HTTParty).to have_received(:get).with('https://discord.com/api/v10/channels/789/messages', {})
      expect(result).to eq(response)
    end

    context 'when throttled' do
      let(:throttled_response) { instance_double(HTTParty::Response, code: 429, headers: { 'retry-after' => '5' }) }

      it 'sleeps and retries the request' do
        allow(HTTParty).to receive(:get).and_return(throttled_response, response)
        allow(discord_service).to receive(:sleep)
        allow(discord_service).to receive(:make_api_request).and_call_original

        discord_service.make_api_request(channel_id, {})

        expect(discord_service).to have_received(:sleep).with(5)
        expect(discord_service).to have_received(:make_api_request).twice
      end
    end
  end
end
