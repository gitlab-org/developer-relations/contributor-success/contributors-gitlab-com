# frozen_string_literal: true

RSpec.describe GitlabOauthService do
  before do
    stub_env('APP_ID', 'app_id')
    stub_env('APP_SECRET', 'APP_SECRET')
  end

  describe '#authorize', :vcr do
    let(:user_response) { { 'id' => 1, 'username' => 'leetickett-gitlab' } }

    before do
      allow(described_class).to receive(:user).with('access_token').and_return(user_response)
    end

    it 'makes the expected OAuth API call' do
      expect { described_class.authorize('oauth_code') }.not_to raise_error
    end

    it 'calls #user with the token' do
      described_class.authorize('oauth_code')

      expect(described_class).to have_received(:user).with('access_token')
    end

    it 'returns the expected hash' do
      response = described_class.authorize('oauth_code')

      expect(response).to match a_hash_including({
        user_id: 1,
        username: 'leetickett-gitlab',
        gitlab_access_token: 'access_token',
        gitlab_expires_at: 1_715_031_945,
        gitlab_refresh_token: 'refresh_token'
      })
    end
  end

  describe '#refresh', :vcr do
    it 'makes the expected OAuth API call and returns the expected hash' do
      response = described_class.refresh('refresh_token')

      expect(response).to match a_hash_including({
        gitlab_access_token: 'new_token',
        gitlab_expires_at: 1_719_613_188,
        gitlab_refresh_token: 'new_refresh_token'
      })
    end
  end

  describe '#oauth_authorize_url' do
    it 'returns the GitLab OAuth URL' do
      regex_pattern = %r{
        https://gitlab\.com/oauth/authorize
        \?client_id=app_id
        &redirect_uri=http%3A%2F%2Flocalhost%3A3030%2Foauth_callback
        &response_type=code
        &scope=api
        &state=[a-f0-9]{32}
      }x

      response = described_class.oauth_authorize_url

      expect(response).to match(regex_pattern)
    end
  end

  describe '#user', :vcr do
    it 'makes the expected REST API call and passes the JSON response' do
      response = described_class.user('abcde-abcdefghijklmnopqrst')

      expect(response).to include({ 'id' => 12_687_636, 'username' => 'leetickett-gitlab' })
    end
  end
end
