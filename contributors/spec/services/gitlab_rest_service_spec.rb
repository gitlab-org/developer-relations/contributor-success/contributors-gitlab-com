# frozen_string_literal: true

RSpec.describe GitlabRestService do
  subject(:service) { described_class.new(token) }

  let(:token) { 'token' }

  before do
    stub_env('APP_ID', 'app_id')
    allow(Rails.logger).to receive(:warn)
  end

  describe '#create_event_issue', vcr: { cassette_name: 'Events_CreateService/execute' } do
    subject(:create_event_issue) do
      service.create_event_issue(1_000_000, 'Test title', 'Test description')
    end

    let(:response) { instance_double(HTTParty::Response, success?: true, parsed_response: { test: true }) }

    before do
      allow(HTTParty).to receive(:post).and_return(response)
    end

    it 'sends expected request to GitLab' do
      expect(create_event_issue).to eq({ test: true })
      expect(HTTParty).to have_received(:post).with(
        'https://gitlab.com/api/v4/projects/65972950/issues',
        {
          body: '{"assignee_id":1000000,"title":"Test title","description":"Test description"}',
          headers: {
            'Authorization' => 'Bearer token',
            'Content-Type' => 'application/json',
            'User-Agent' => 'contributorsDot'
          }
        }
      )
    end

    context 'when development environment' do
      before do
        allow(Rails.env).to receive(:development?).and_return(true)
      end

      it "doesn't send request" do
        allow(HTTParty).to receive(:post)

        expect(create_event_issue).to eq({})

        expect(HTTParty).not_to have_received(:post)
      end
    end

    context 'when request failed' do
      let(:response) { instance_double(HTTParty::Response, success?: false, code: 500, body: 'test fail') }

      it 'raises and logs error' do
        allow(Rails.logger).to receive(:error)

        expect { create_event_issue }.to raise_error(GitlabRestService::GitlabResponseFailure)

        expect(Rails.logger).to have_received(:error).with('test fail')
      end
    end
  end

  describe '#create_reward_issue' do
    subject(:create_reward_issue) do
      service.create_reward_issue(1_000_000, 1, 'Test title', 'Test description')
    end

    let(:response) { instance_double(HTTParty::Response, success?: true, parsed_response: { 'iid' => 999 }) }

    before do
      allow(HTTParty).to receive(:post).and_return(response)
    end

    it 'sends expected request to GitLab' do
      expect(create_reward_issue).to eq(999)
      expect(HTTParty).to have_received(:post).with(
        'https://gitlab.com/api/v4/projects/48412664/issues',
        {
          body: '{"assignee_id":1000000,"title":"Test title","description":"Test description"}',
          headers: {
            'Authorization' => 'Bearer token',
            'Content-Type' => 'application/json',
            'User-Agent' => 'contributorsDot'
          }
        }
      )
    end

    context 'when development environment' do
      before do
        allow(Rails.env).to receive(:development?).and_return(true)
      end

      it "doesn't send request" do
        allow(HTTParty).to receive(:post)

        expect(create_reward_issue).to eq(0)

        expect(HTTParty).not_to have_received(:post)
      end
    end

    context 'when request failed' do
      let(:response) { instance_double(HTTParty::Response, success?: false, code: 500, body: 'test fail') }

      it 'raises and logs error' do
        allow(Rails.logger).to receive(:error)

        expect { create_reward_issue }.to raise_error(GitlabRestService::GitlabResponseFailure)

        expect(Rails.logger).to have_received(:error).with('test fail')
      end
    end
  end

  describe '#start_thread' do
    it 'makes the expected HTTParty request' do
      allow(HTTParty).to receive(:post).and_return(instance_double(HTTParty::Response, success?: true))

      service.start_thread(1, 2, 'merge_requests', 'Three')

      expect(HTTParty).to have_received(:post).with(
        'https://gitlab.com/api/v4/projects/1/merge_requests/2/discussions',
        body: '{"body":"Three\n\nPosted via [contributors.gitlab.com](https://contributors.gitlab.com)."}',
        headers: {
          'Authorization' => 'Bearer token',
          'Content-Type' => 'application/json',
          'User-Agent' => 'contributorsDot'
        }
      )
      expect(Rails.logger).not_to have_received(:warn)
    end
  end

  describe '#bot_request' do
    it 'makes the expected HTTParty request' do
      allow(HTTParty).to receive(:post).and_return(instance_double(HTTParty::Response, success?: true))

      service.bot_request('help', 10, 100)

      expect(HTTParty).to have_received(:post).with(
        'https://gitlab.com/api/v4/projects/10/merge_requests/100/notes',
        body: '{"body":"@gitlab-bot help\n\nPosted via [contributors.gitlab.com](https://contributors.gitlab.com)."}',
        headers: {
          'Authorization' => 'Bearer token',
          'Content-Type' => 'application/json',
          'User-Agent' => 'contributorsDot'
        }
      )
      expect(Rails.logger).not_to have_received(:warn)
    end

    context 'with a unsuccessful response' do
      it 'logs the error' do
        post_response = instance_double(
          HTTParty::Response,
          success?: false,
          code: 404,
          body: '{"message":"404 Project Not Found"}'
        )
        allow(HTTParty).to receive(:post).and_return(post_response)

        service.bot_request('help', 10, 100)

        expect(Rails.logger).to have_received(:warn)
      end
    end
  end

  describe '#get_merge_request_web_url', :vcr do
    let(:token) { '' }

    it 'makes the expected HTTParty request' do
      allow(HTTParty).to receive(:get).and_call_original

      service.get_merge_request_web_url('56614952', '58')

      expect(HTTParty).to have_received(:get).with(
        'https://gitlab.com/api/v4/projects/56614952/merge_requests/58',
        headers: {
          'Authorization' => 'Bearer ',
          'Content-Type' => 'application/json',
          'User-Agent' => 'contributorsDot'
        }
      )
      expect(Rails.logger).not_to have_received(:warn)
    end

    it 'returns the web_url' do
      response = service.get_merge_request_web_url('56614952', '58')

      expect(response).to eq('https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/merge_requests/58')
    end
  end
end
