# frozen_string_literal: true

RSpec.describe GrabDataService do
  subject(:instance) { described_class.new }

  let(:activity_date) { Date.new(2024, 3, 2) }
  let(:after) { nil }
  let(:group) { 'gitlab-com' }
  let(:issuable_type) { 'issues' }
  let(:team_members) { ['leetickett-gitlab'] }
  let(:team_member_service) { instance_double(TeamMemberService, team_members:) }

  let(:user_node) do
    {
      'id' => 'gid://gitlab/User/3',
      'username' => 'three',
      'state' => 'active'
    }
  end
  let(:note_node) do
    {
      'id' => 'gid://gitlab/Note/19',
      'createdAt' => '2024-01-01T04:01:00Z',
      'author' => user_node
    }
  end
  let(:issue_node) do
    {
      'id' => 'gid://gitlab/Issue/7',
      'title' => 'Seven',
      'state' => 'opened',
      'createdAt' => '2024-01-01T01:01:01Z',
      'webUrl' => 'https://gitlab.com/gitlab-com/Product/-/issues/2',
      'upvotes' => 0,
      'closedAt' => '2024-05-06T07:08:09Z',
      'author' =>
      {
        'id' => 'gid://gitlab/User/1',
        'username' => 'one',
        'state' => 'active'
      },
      'labels' => { 'nodes' => [] },
      'notes' => {
        'nodes' =>
        [
          {
            'id' => 'gid://gitlab/Note/19',
            'createdAt' => '2024-01-01T04:01:00Z',
            'author' => {
              'id' => 'gid://gitlab/User/3',
              'username' => 'three',
              'state' => 'active'
            }
          }
        ]
      }
    }
  end
  let(:merge_request_node) do
    {
      'id' => 'gid://gitlab/MergeRequest/7',
      'title' => 'Seven',
      'state' => 'opened',
      'createdAt' => '2024-01-01T01:01:01Z',
      'webUrl' => 'https://gitlab.com/gitlab-com/Product/-/merge_requests/2',
      'upvotes' => 0,
      'mergedAt' => '2024-05-06T07:08:09Z',
      'commits' => {
        'nodes' => []
      },
      'author' => {
        'id' => 'gid://gitlab/User/1',
        'username' => 'one',
        'state' => 'active'
      },
      'labels' => { 'nodes' => [] },
      'notes' => {
        'nodes' =>
        [
          note_node,
          {
            'id' => 'gid://gitlab/Note/20',
            'createdAt' => '2024-02-01T04:01:00Z',
            'author' => {
              'id' => 'gid://gitlab/User/4',
              'username' => 'four',
              'state' => 'active'
            }
          },
          {
            'id' => 'gid://gitlab/Note/21',
            'createdAt' => '2023-02-01T04:01:00Z',
            'author' => {
              'id' => 'gid://gitlab/User/5',
              'username' => 'five',
              'state' => 'active'
            }
          }
        ]
      }
    }
  end

  before do
    allow(TeamMemberService).to receive(:new).and_return(team_member_service)
    stub_const('GrabDataService::GROUPS', %w[gitlab-com gitlab-org])
  end

  describe '.execute', :vcr do
    it 'executes' do
      expect { instance.execute }.not_to raise_error
    end

    it 'calls process_group for each group' do
      allow(instance).to receive(:process_group)

      instance.execute

      expect(instance).to have_received(:process_group).with('gitlab-com')
      expect(instance).to have_received(:process_group).with('gitlab-org')
    end
  end

  describe '.process_group' do
    it 'calls process_issuable_type for issuable type' do
      allow(instance).to receive(:process_issuable_type)

      instance.process_group(group)

      expect(instance).to have_received(:process_issuable_type).with(group, 'issues')
      expect(instance).to have_received(:process_issuable_type).with(group, 'mergeRequests')
    end
  end

  describe '.process_issuable_type' do
    let(:process_page_result) { { processed_records: 0, has_next_page: false } }
    let(:activity_date) { described_class::EARLIEST_DATE }

    before do
      allow(instance).to receive(:next_date).and_return(activity_date)
      allow(instance).to receive(:process_page)
        .with(group, issuable_type, activity_date, nil)
        .and_return(process_page_result)
      allow(Rails.logger).to receive(:info)
      allow(ProcessedJob).to receive(:create!)
    end

    it 'calls next_date with group and issuable_type' do
      instance.process_issuable_type(group, issuable_type)

      expect(instance).to have_received(:next_date).with(group, issuable_type)
    end

    it 'calls process_page with group, issuable_type, activity_date and after' do
      instance.process_issuable_type(group, issuable_type)

      expect(instance).to have_received(:process_page).with(group, issuable_type, activity_date, after)
    end

    it 'calls Rails.logger.info with expected params' do
      instance.process_issuable_type(group, issuable_type)

      expect(Rails.logger).to have_received(:info) do |&block|
        expect(block.call).to match(%r{\[gitlab-com / issues / 2024-01-01\] Processed 0 in})
      end
    end

    it 'calls ProcessedJob.create! with expected params' do
      instance.process_issuable_type(group, issuable_type)

      expect(ProcessedJob).to have_received(:create!).with(
        group:,
        issuable_type:,
        activity_date:,
        processed_records: 0,
        elapsed: kind_of(Float)
      )
    end

    context 'when process_page returns has_next_page' do
      let(:process_page_result) { { processed_records: 10, has_next_page: true, after: 'recordId' } }

      before do
        allow(instance).to receive(:process_page)
          .with(group, issuable_type, activity_date, 'recordId')
          .and_return({ processed_records: 0, has_next_page: false })
      end

      it 'calls process_page a 2nd time' do
        instance.process_issuable_type(group, issuable_type)

        expect(instance).to have_received(:process_page).with(group, issuable_type, activity_date, after)
        expect(instance).to have_received(:process_page).with(group, issuable_type, activity_date, 'recordId')
      end

      it 'calls Rails.logger.info with expected params' do
        instance.process_issuable_type(group, issuable_type)

        expect(Rails.logger).to have_received(:info) do |&block|
          expect(block.call).to match(%r{\[gitlab-com / issues / 2024-01-01\] Processed 10 in})
        end
      end

      it 'calls ProcessedJob.create! with expected params' do
        instance.process_issuable_type(group, issuable_type)

        expect(ProcessedJob).to have_received(:create!).with(
          group:,
          issuable_type:,
          activity_date:,
          processed_records: 10,
          elapsed: kind_of(Float)
        )
      end
    end
  end

  describe '.next_date' do
    context 'when ProcessedJob is empty' do
      it 'returns the earliest date' do
        expect(instance.next_date(group, issuable_type)).to eq(described_class::EARLIEST_DATE)
      end
    end

    context 'when ProcessedJob is not empty' do
      before do
        create(:processed_job, group:, issuable_type:, activity_date:)
      end

      it 'returns the next date' do
        expect(instance.next_date(group, issuable_type)).to eq(activity_date + 1.day)
      end
    end
  end

  describe '.process_page' do
    let(:has_next_page) { false }
    let(:nodes) { [] }
    let(:data) do
      {
        'data' => {
          'group' => {
            'issues' => {
              'pageInfo' => {
                'endCursor' => after,
                'hasNextPage' => has_next_page
              },
              'nodes' => nodes
            }
          }
        }
      }
    end

    before do
      allow(instance).to receive(:fetch_page)
        .with(group, issuable_type, activity_date, after)
        .and_return(data)
    end

    it 'returns expected values' do
      result = instance.process_page(group, issuable_type, activity_date, after)

      expect(result).to eq(
        {
          processed_records: 0,
          after:,
          has_next_page:
        }
      )
    end

    context 'with data' do
      let(:after) { 'endCursor' }
      let(:has_next_page) { true }
      let(:nodes) { [1, 2, 3] }

      before do
        allow(instance).to receive(:process_nodes)
      end

      it 'returns expected values' do
        result = instance.process_page(group, issuable_type, activity_date, after)

        expect(result).to eq(
          {
            processed_records: 3,
            after:,
            has_next_page:
          }
        )
      end
    end
  end

  describe '.process_nodes' do
    context 'with no data' do
      let(:nodes) { [] }

      it 'inserts no data' do
        expect { instance.process_nodes(nodes, issuable_type) }.not_to change(User, :count)
        expect { instance.process_nodes(nodes, issuable_type) }.not_to change(Issue, :count)
        expect { instance.process_nodes(nodes, issuable_type) }.not_to change(MergeRequest, :count)
        expect { instance.process_nodes(nodes, issuable_type) }.not_to change(Note, :count)
      end
    end

    context 'with issue data' do
      let(:nodes) do
        [
          {
            'id' => 'gid://gitlab/Issue/7',
            'title' => 'Seven',
            'state' => 'opened',
            'createdAt' => '2024-01-01T04:01:32Z',
            'webUrl' => 'https://gitlab.com/gitlab-com/Product/-/issues/1',
            'upvotes' => 0,
            'closedAt' => nil,
            'author' =>
            {
              'id' => 'gid://gitlab/User/1',
              'username' => 'one',
              'state' => 'active'
            },
            'labels' => { 'nodes' => [] },
            'notes' => { 'nodes' => [] }
          },
          {
            'id' => 'gid://gitlab/Issue/8',
            'title' => 'Eight',
            'state' => 'opened',
            'createdAt' => '2024-01-01T04:01:31Z',
            'webUrl' => 'https://gitlab.com/gitlab-com/Product/-/issues/2',
            'upvotes' => 0,
            'closedAt' => nil,
            'author' =>
            {
              'id' => 'gid://gitlab/User/2',
              'username' => 'two',
              'state' => 'active'
            },
            'labels' => { 'nodes' => [] },
            'notes' => {
              'nodes' =>
              [
                {
                  'id' => 'gid://gitlab/Note/9',
                  'createdAt' => '2024-01-01T04:01:31Z',
                  'author' => {
                    'id' => 'gid://gitlab/User/3',
                    'username' => 'three',
                    'state' => 'active'
                  }
                },
                {
                  'id' => 'gid://gitlab/Note/10',
                  'createdAt' => '2024-01-01T04:01:31Z',
                  'author' => {
                    'id' => 'gid://gitlab/User/3',
                    'username' => 'three',
                    'state' => 'active'
                  }
                }
              ]
            }
          }
        ]
      end

      it 'inserts 3 users' do
        expect { instance.process_nodes(nodes, issuable_type) }.to change(User, :count).by(3)
      end

      it 'inserts 2 issues' do
        expect { instance.process_nodes(nodes, issuable_type) }.to change(Issue, :count).by(2)
      end

      it 'inserts no merge requests' do
        expect { instance.process_nodes(nodes, issuable_type) }.not_to change(MergeRequest, :count)
      end

      it 'inserts 2 notes' do
        expect { instance.process_nodes(nodes, issuable_type) }.to change(Note, :count).by(2)
      end
    end

    context 'with merge request data' do
      let(:issuable_type) { 'mergeRequests' }
      let(:nodes) do
        [
          {
            'id' => 'gid://gitlab/MergeRequest/6',
            'title' => 'Six',
            'state' => 'merged',
            'createdAt' => '2024-01-01T04:01:32Z',
            'webUrl' => 'https://gitlab.com/gitlab-com/Product/-/issues/1',
            'upvotes' => 0,
            'commits' => {
              'nodes' => [{
                'sha' => 'aaa111',
                'committedDate' => '2024-01-02T04:01:32Z',
                'author' => {
                  'id' => 'gid://gitlab/User/9',
                  'username' => 'nine',
                  'state' => 'active'
                }
              }]
            },
            'closedAt' => nil,
            'author' =>
            {
              'id' => 'gid://gitlab/User/1',
              'username' => 'one',
              'state' => 'active'
            },
            'labels' => { 'nodes' => [] },
            'notes' => { 'nodes' => [] }
          },
          {
            'id' => 'gid://gitlab/MergeRequest/7',
            'title' => 'Seven',
            'state' => 'merged',
            'createdAt' => '2024-01-01T04:01:32Z',
            'webUrl' => 'https://gitlab.com/gitlab-com/Product/-/issues/2',
            'upvotes' => 0,
            'commits' => {
              'nodes' => [
                {
                  'sha' => 'bbb222',
                  'committedDate' => '2024-01-03T04:01:32Z',
                  'author' => {
                    'id' => 'gid://gitlab/User/8',
                    'username' => 'eight',
                    'state' => 'active'
                  }
                },
                {
                  'sha' => 'aaa111',
                  'committedDate' => '2024-01-04T04:01:32Z',
                  'author' => {
                    'id' => 'gid://gitlab/User/9',
                    'username' => 'nine',
                    'state' => 'active'
                  }
                }
              ]
            },
            'closedAt' => nil,
            'author' =>
            {
              'id' => 'gid://gitlab/User/1',
              'username' => 'one',
              'state' => 'active'
            },
            'labels' => { 'nodes' => [] },
            'notes' => {
              'nodes' =>
              [
                {
                  'id' => 'gid://gitlab/Note/19',
                  'createdAt' => '2024-01-01T04:01:31Z',
                  'author' => {
                    'id' => 'gid://gitlab/User/3',
                    'username' => 'three',
                    'state' => 'active'
                  }
                }
              ]
            }
          },
          {
            'id' => 'gid://gitlab/MergeRequest/8',
            'title' => 'Eight',
            'state' => 'opened',
            'createdAt' => '2024-01-01T04:01:31Z',
            'webUrl' => 'https://gitlab.com/gitlab-com/Product/-/issues/3',
            'upvotes' => 0,
            'commits' => {
              'nodes' => [{
                'sha' => 'abc123',
                'committedDate' => '2024-01-05T04:01:32Z',
                'author' => {
                  'id' => 'gid://gitlab/User/2',
                  'username' => 'two',
                  'state' => 'active'
                }
              }]
            },
            'closedAt' => nil,
            'author' =>
            {
              'id' => 'gid://gitlab/User/2',
              'username' => 'two',
              'state' => 'active'
            },
            'labels' => { 'nodes' => [] },
            'notes' => {
              'nodes' =>
              [
                {
                  'id' => 'gid://gitlab/Note/9',
                  'createdAt' => '2024-01-01T04:01:31Z',
                  'author' => {
                    'id' => 'gid://gitlab/User/3',
                    'username' => 'three',
                    'state' => 'active'
                  }
                },
                {
                  'id' => 'gid://gitlab/Note/10',
                  'createdAt' => '2024-01-01T04:01:31Z',
                  'author' => {
                    'id' => 'gid://gitlab/User/4',
                    'username' => 'four',
                    'state' => 'active'
                  }
                }
              ]
            }
          }
        ]
      end

      it 'inserts 6 users' do
        expect { instance.process_nodes(nodes, issuable_type) }.to change(User, :count).by(6)
      end

      it 'inserts no issues' do
        expect { instance.process_nodes(nodes, issuable_type) }.not_to change(Issue, :count)
      end

      it 'inserts 3 merge requests' do
        expect { instance.process_nodes(nodes, issuable_type) }.to change(MergeRequest, :count).by(3)
      end

      it 'inserts 3 notes' do
        expect { instance.process_nodes(nodes, issuable_type) }.to change(Note, :count).by(3)
      end

      it 'inserts 2 commits' do
        expect { instance.process_nodes(nodes, issuable_type) }.to change(Commit, :count).by(2)
      end

      it 'clears out existing commits first' do
        create(:merge_request, id: 7)
        create(:merge_request, id: 8)
        old_commits = create_list(:commit, 2, merge_request_id: 7)
        old_commits += create_list(:commit, 2, merge_request_id: 8)

        instance.process_nodes(nodes, issuable_type)

        expect(Commit.where(sha: old_commits.map(&:sha))).to be_empty
      end
    end
  end

  describe '.process_node' do
    context 'with an issue' do
      let(:node) { issue_node }

      it 'returns the expected hash' do
        result = instance.process_node(node, issuable_type)

        expect(result).to eq(
          {
            users: {
              'one' => { community_member: true, organization: nil, id: 1, username: 'one' },
              'three' => { community_member: true, organization: nil, id: 3, username: 'three' }
            },
            issuable: {
              closed_date: DateTime.parse('2024-05-06T07:08:09Z'),
              id: 7,
              opened_date: DateTime.parse('2024-01-01T01:01:01Z'),
              project_id: nil,
              state_id: 'opened',
              title: 'Seven',
              upvotes: 0,
              user_id: 1,
              web_url: 'https://gitlab.com/gitlab-com/Product/-/issues/2',
              labels: []
            },
            notes: [{
              added_date: DateTime.parse('2024-01-01T04:01:00Z'),
              id: 19,
              issue_id: 7,
              merge_request_id: nil,
              user_id: 3
            }],
            commits: []
          }
        )
      end
    end

    context 'with a merge request' do
      let(:issuable_type) { 'mergeRequests' }
      let(:node) { merge_request_node }

      it 'returns the expected hash' do
        result = instance.process_node(node, issuable_type)

        expect(result).to eq(
          {
            users: {
              'one' => { community_member: true, id: 1, organization: nil, username: 'one' },
              'three' => { community_member: true, id: 3, organization: nil, username: 'three' },
              'four' => { community_member: true, id: 4, organization: nil, username: 'four' }
            },
            issuable: {
              merged_date: DateTime.parse('2024-05-06T07:08:09Z'),
              id: 7,
              opened_date: DateTime.parse('2024-01-01T01:01:01Z'),
              project_id: nil,
              state_id: 'opened',
              title: 'Seven',
              upvotes: 0,
              user_id: 1,
              web_url: 'https://gitlab.com/gitlab-com/Product/-/merge_requests/2',
              labels: []
            },
            notes: [
              {
                added_date: DateTime.parse('2024-01-01T04:01:00Z'),
                id: 19,
                issue_id: nil,
                merge_request_id: 7,
                user_id: 3
              },
              {
                added_date: DateTime.parse('2024-02-01T04:01:00Z'),
                id: 20,
                issue_id: nil,
                merge_request_id: 7,
                user_id: 4
              }
            ],
            commits: []
          }
        )
      end
    end
  end

  describe '.process_notes' do
    context 'with an issue' do
      let(:issuable_node) { issue_node }

      it 'returns the expected hash' do
        result = instance.process_notes(issuable_node, issuable_type)

        expect(result).to eq(
          {
            users: {
              'three' => { community_member: true, id: 3, organization: nil, username: 'three' }
            },
            notes: [
              {
                added_date: DateTime.parse('2024-01-01T04:01:00Z'),
                id: 19,
                issue_id: 7,
                merge_request_id: nil,
                user_id: 3
              }
            ]
          }
        )
      end
    end

    context 'with a merge request' do
      let(:issuable_type) { 'mergeRequests' }
      let(:issuable_node) { merge_request_node }

      it 'returns the expected hash' do
        result = instance.process_notes(issuable_node, issuable_type)

        expect(result).to eq(
          {
            users: {
              'three' => { community_member: true, id: 3, organization: nil, username: 'three' },
              'four' => { community_member: true, id: 4, organization: nil, username: 'four' }
            },
            notes: [
              {
                added_date: DateTime.parse('2024-01-01T04:01:00Z'),
                id: 19,
                issue_id: nil,
                merge_request_id: 7,
                user_id: 3
              },
              {
                added_date: DateTime.parse('2024-02-01T04:01:00Z'),
                id: 20,
                issue_id: nil,
                merge_request_id: 7,
                user_id: 4
              }
            ]
          }
        )
      end
    end
  end

  describe '.process_note' do
    let(:issuable_id) { 7 }

    context 'with an issue' do
      it 'returns the expected hash' do
        result = instance.process_note(note_node, issuable_type, issuable_id)

        expect(result).to eq(
          {
            author: {
              community_member: true,
              organization: nil,
              id: 3,
              username: 'three'
            },
            note: {
              id: 19,
              issue_id: issuable_id,
              merge_request_id: nil,
              user_id: 3,
              added_date: DateTime.parse('2024-01-01T04:01:00Z')
            }
          }
        )
      end
    end

    context 'with a merge request' do
      let(:issuable_type) { 'mergeRequests' }

      it 'returns the expected hash' do
        result = instance.process_note(note_node, issuable_type, issuable_id)

        expect(result).to eq(
          {
            author: {
              community_member: true,
              organization: nil,
              id: 3,
              username: 'three'
            },
            note: {
              id: 19,
              issue_id: nil,
              merge_request_id: issuable_id,
              user_id: 3,
              added_date: DateTime.parse('2024-01-01T04:01:00Z')
            }
          }
        )
      end
    end
  end

  describe '.process_user' do
    shared_examples 'a user with community_member: false' do
      it 'returns community_member: false' do
        result = instance.process_user(user_node)

        expect(result[:community_member]).to be_falsy
      end
    end

    it 'returns the expected hash' do
      result = instance.process_user(user_node)

      expect(result).to eq(
        {
          community_member: true,
          organization: nil,
          id: 3,
          username: 'three'
        }
      )
    end

    context 'when the user is blocked' do
      let(:user_node) do
        {
          'id' => 'gid://gitlab/User/3',
          'username' => 'three',
          'state' => 'blocked'
        }
      end

      it_behaves_like 'a user with community_member: false'
    end

    context 'when the user is a contractor' do
      let(:user_node) do
        {
          'id' => 'gid://gitlab/User/3',
          'username' => 'three-ext',
          'state' => 'active'
        }
      end

      it_behaves_like 'a user with community_member: false'
    end

    context 'when the user is a bot' do
      let(:user_node) do
        {
          'id' => 'gid://gitlab/User/3',
          'username' => 'gitlab-bot',
          'state' => 'active'
        }
      end

      it_behaves_like 'a user with community_member: false'
    end

    context 'when the user is a team member' do
      let(:user_node) do
        {
          'id' => 'gid://gitlab/User/3',
          'username' => 'leetickett-gitlab',
          'state' => 'active'
        }
      end

      it_behaves_like 'a user with community_member: false'
    end
  end

  describe '.additional_props' do
    context 'with an issue' do
      context 'with a closed date' do
        it 'returns the expected hash' do
          result = instance.additional_props(issue_node, issuable_type)

          expect(result).to eq({ closed_date: DateTime.parse('2024-05-06T07:08:09Z') })
        end
      end

      context 'with no closed date' do
        it 'returns the expected hash' do
          issue_node['closedAt'] = nil

          result = instance.additional_props(issue_node, issuable_type)

          expect(result).to eq({ closed_date: nil })
        end
      end
    end

    context 'with a merge request' do
      let(:issuable_type) { 'mergeRequests' }

      context 'with a closed date' do
        it 'returns the expected hash' do
          result = instance.additional_props(merge_request_node, issuable_type)

          expect(result).to eq({ merged_date: DateTime.parse('2024-05-06T07:08:09Z') })
        end
      end

      context 'with no closed date' do
        it 'returns the expected hash' do
          merge_request_node['mergedAt'] = nil

          result = instance.additional_props(merge_request_node, issuable_type)

          expect(result).to eq({ merged_date: nil })
        end
      end
    end
  end

  describe '.graphql_query' do
    subject(:result) { instance.graphql_query(group, issuable_type, activity_date, 'recordId') }

    it 'filters the correct group' do
      expect(result).to include('fullPath: "gitlab-com"')
    end

    it 'filters the correct created date' do
      expect(result).to include('createdAfter: "2024-01-01"')
    end

    it 'filters the correct activity date' do
      expect(result).to include('updatedAfter: "2024-03-02"')
      expect(result).to include('updatedBefore: "2024-03-03"')
    end

    it 'requests the correct issuable type' do
      expect(result).to include('issues(')
    end

    it 'requests the correct page' do
      expect(result).to include('after: "recordId"')
    end

    describe 'issuable_attribute' do
      context 'when issuable_type is issues' do
        it 'requests the correct issuable attribute' do
          expect(result).to include('closedAt')
        end
      end

      context 'when issuable_type is mergeRequests' do
        let(:issuable_type) { 'mergeRequests' }

        it 'requests the correct issuable attribute' do
          expect(result).to include('mergedAt')
        end
      end
    end
  end

  describe '.fetch_page' do
    let(:graphql_response) { JSON.parse('{ "data": { "group": { "mergeRequests": { "nodes": [] } } } }') }

    before do
      allow(Rails.logger).to receive(:error)
      allow(GitlabGraphqlService).to receive(:execute).and_return(graphql_response)
    end

    it 'only executes the query once (and logs no errors)' do
      instance.fetch_page('gitlab-org', 'mergeRequests', Date.new(2024, 9, 13), nil)

      expect(Rails.logger).not_to have_received(:error)
      expect(GitlabGraphqlService).to have_received(:execute).once
    end

    context 'when the query times out' do
      let(:graphql_response) { JSON.parse('{ "errors": [ { "message": "Timeout on BaseConnection.pageInfo", "locations": [ { "line": 71, "column": 7 } ], "path": [ "group", "mergeRequests", "pageInfo" ] } ], "data": { "group": { "mergeRequests": null } } }') } # rubocop:disable Layout/LineLength

      it 'retries the query 5 times and logs the error' do
        instance.fetch_page('gitlab-org', 'mergeRequests', Date.new(2024, 9, 13), nil)

        expect(Rails.logger).to have_received(:error).exactly(5).times do |&block|
          expect(block.call).to match(/nodes are nil- response was:/)
        end
        expect(GitlabGraphqlService).to have_received(:execute).exactly(5).times
      end

      context 'with a custom PAGE_SIZE' do
        it 'executes the query with the correct page sizes' do
          stub_env('PAGE_SIZE', '4')

          instance.fetch_page('gitlab-org', 'mergeRequests', Date.new(2024, 9, 13), nil)

          expect(GitlabGraphqlService).to have_received(:execute).with(/first: 4/)
          expect(GitlabGraphqlService).to have_received(:execute).with(/first: 2/)
          expect(GitlabGraphqlService).to have_received(:execute).with(/first: 1/)
          expect(Rails.logger).to have_received(:error).exactly(3).times do |&block|
            expect(block.call).to match(/nodes are nil- response was:/)
          end
        end
      end
    end
  end
end
