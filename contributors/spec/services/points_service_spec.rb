# frozen_string_literal: true

RSpec.describe PointsService do
  subject(:service) { described_class.new(from_date: nil, to_date: nil, community_only: false) }

  let!(:top_user) { create(:user, id: 1, community_member: true) }
  let!(:user) { create(:user, id: 2, community_member: true) }
  let!(:non_community_member) { create(:user, id: 3, community_member: false, contributor_level: 2) }

  before do
    merge_request =
      create(:merge_request, user: top_user, opened_date: 1.day.ago, merged_date: 1.day.ago, state_id: :merged)
    create(:commit, user: top_user, merge_request:)
    create(:commit, user: non_community_member, merge_request:)
    create(:issue, user:, opened_date: 1.day.ago)
    create(:note, user:, added_date: 1.day.ago, issue: Issue.all.sample)
    create(:note, user:, added_date: 1.day.ago, merge_request: MergeRequest.all.sample)

    Views::ActivitySummary.refresh(concurrently: false)
    Views::RecentActivitySummary.refresh(concurrently: false)
  end

  describe '#fetch_data' do
    it 'calculates points and saves them to the database' do
      expect(User.pluck(:points)).to all(eq(0))

      service.fetch_data(save: true)

      expect(User.pluck(:id, :points)).to contain_exactly([1, 100], [2, 7], [3, 20])
    end

    it 'does not update contributor_level' do
      expect(User.pluck(:points)).to all(eq(0))

      service.fetch_data(save: true)

      expect(User.pluck(:id, :contributor_level)).to contain_exactly([1, 0], [2, 0], [3, 2])
    end
  end

  describe '#update_users' do
    let!(:user_one) { create(:user, id: 100, username: 'a', community_member: true, points: 0, contributor_level: 0) }
    let!(:user_two) { create(:user, id: 200, username: 'b', community_member: false, points: 10, contributor_level: 1) }

    it 'updates points only' do
      data = [
        {
          user_id: user_one.id,
          community_member: false,
          username: 'c',
          score: 1_000,
          contributor_level: 1
        },
        {
          user_id: user_two.id,
          community_member: true,
          username: 'c',
          score: 2_000,
          contributor_level: 2
        }
      ]

      service.update_users(data)

      expect(user_one.reload).to have_attributes(
        username: 'a',
        community_member: true,
        points: 1_000,
        contributor_level: 0
      )
      expect(user_two.reload).to have_attributes(
        username: 'b',
        community_member: false,
        points: 2_000,
        contributor_level: 1
      )
    end
  end

  describe '.contributor_level' do
    context 'when 1 point below level 1' do
      it 'returns the correct level' do
        expect(described_class.contributor_level(24)).to eq(0)
      end
    end

    context 'when at the level 1 point threshold' do
      it 'returns the correct level' do
        expect(described_class.contributor_level(25)).to eq(1)
      end
    end

    context 'when 1 point below level 2' do
      it 'returns the correct level' do
        expect(described_class.contributor_level(499)).to eq(1)
      end
    end

    context 'when at the level 2 point threshold' do
      it 'returns the correct level' do
        expect(described_class.contributor_level(500)).to eq(2)
      end
    end

    context 'when 1 point below level 3' do
      it 'returns the correct level' do
        expect(described_class.contributor_level(2_499)).to eq(2)
      end
    end

    context 'when at the level 3 point threshold' do
      it 'returns the correct level' do
        expect(described_class.contributor_level(2_500)).to eq(3)
      end
    end

    context 'when 1 point below level 4' do
      it 'returns the correct level' do
        expect(described_class.contributor_level(7_499)).to eq(3)
      end
    end

    context 'when at the level 4 point threshold' do
      it 'returns the correct level' do
        expect(described_class.contributor_level(7_500)).to eq(4)
      end
    end

    context 'when crazily high' do
      it 'returns the correct level' do
        expect(described_class.contributor_level(7_500_000)).to eq(4)
      end
    end
  end
end
