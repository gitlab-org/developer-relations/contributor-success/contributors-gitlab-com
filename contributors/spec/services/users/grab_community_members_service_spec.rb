# frozen_string_literal: true

RSpec.describe Users::GrabCommunityMembersService do
  subject(:instance) { described_class.new }

  describe '#execute', vcr: { cassette_name: 'Users_GrabCommunityMembersService/execute' } do
    let!(:old_community_member) { create(:community_member) }

    it 'grabs the expected number of community members' do
      expect { instance.execute }.to change(CommunityMember, :count).from(1).to(1_296)
    end

    it 'grabs the expected community members' do
      instance.execute

      checksum = Digest::SHA256.hexdigest(
        CommunityMember.pluck(:user_id).sort.join
      )

      expect(checksum).to eq('bddac4cd24c35ef1aa85f36f440c20a8209556ebdfc19611a7a271183c333c28')
    end

    it 'removes the old community member' do
      instance.execute

      expect { old_community_member.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    context 'when something goes wrong' do
      it 'raises an error' do
        response = JSON.parse('{ "errors": [ { "message": "Timeout" } ] }')
        allow(GitlabGraphqlService).to receive(:execute).and_return(response)

        expect { instance.execute }.to raise_error(StandardError).with_message(response.to_s)
      end
    end
  end
end
