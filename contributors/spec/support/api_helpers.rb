# frozen_string_literal: true

module ApiHelpers
  def json_body
    JSON.parse(response.body, symoblize_names: true)
  end

  def sign_in(user_id = 1, username = 'lee', admin: false)
    allow(GitlabOauthService).to receive(:authorize).with('abc123')
      .and_return({
        user_id:,
        username:,
        gitlab_access_token: 'glpat-xxxxxxxxxxxxxxxxxxxx',
        gitlab_refresh_token: 'zzz111',
        gitlab_expires_at: Time.zone.now.to_i + 7_200
      })
    allow(FetchContributorSuccessUsersService).to receive(:execute)
      .and_return([admin ? user_id : nil])

    get oauth_callback_path, params: { code: 'abc123' }
  end
end
