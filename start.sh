#!/bin/sh

/cloud-sql-proxy $DB_INSTANCE_CONNECTION_NAME --auto-iam-authn &
rails db:migrate
rails server -b 0.0.0.0 -p 80
